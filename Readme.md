Feeback360

Summary
=======
Rest services on Spring

Prerequisites
=============
JDK 8
Maven 3
Postgres 9.5

Setup
=====

2. Create database user 'feedback360' with password 'feedback360'
2. Create database 'feedback360'
3. Edit application.properties to match Postgres credentials.
4. Uncomment the property `spring.jpa.hibernate.ddl-auto=create` to enable the creation of tables in the database. This option drops/creates tables every time you run the build, so comment it back once your database is populated.


Run
===

Execute the maven command:

mvn clean package spring-boot:run

After the execution the server will run on localhost:8080