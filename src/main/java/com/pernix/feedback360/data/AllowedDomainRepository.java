package com.pernix.feedback360.data;

import org.springframework.data.repository.CrudRepository;

import com.pernix.feedback360.entities.AllowedDomain;

public interface AllowedDomainRepository extends CrudRepository<AllowedDomain, Integer> {
}
