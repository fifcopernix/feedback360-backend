package com.pernix.feedback360.data;

import org.springframework.data.repository.CrudRepository;

import com.pernix.feedback360.entities.EvaluatorRole;

public interface EvaluatorRoleRepository extends CrudRepository<EvaluatorRole, Integer> {
    EvaluatorRole findByName(String name);
}
