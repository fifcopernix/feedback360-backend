package com.pernix.feedback360.data;

import org.springframework.data.repository.CrudRepository;

import com.pernix.feedback360.entities.Stage;

public interface StageRepository extends CrudRepository<Stage, Integer> {

}
