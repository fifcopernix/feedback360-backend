package com.pernix.feedback360.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.feedback360.data.QuestionRepository;
import com.pernix.feedback360.data.QuestionTypeRepository;
import com.pernix.feedback360.data.SubtopicRepository;
import com.pernix.feedback360.entities.Question;
import com.pernix.feedback360.entities.Subtopic;

@RestController
public class QuestionServices {

    private final QuestionRepository questionRepository;
    private final SubtopicRepository subtopicRepository;
    private final QuestionTypeRepository questionTypeRepository;

    @Autowired
    public QuestionServices(QuestionRepository questionRepository, SubtopicRepository subtopicRepository,
            QuestionTypeRepository questionTypeRepository) {
        this.questionRepository = questionRepository;
        this.subtopicRepository = subtopicRepository;
        this.questionTypeRepository = questionTypeRepository;
    }

    @RequestMapping(value = "/questions", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Question>> getAll() {
        Iterable<Question> questions = questionRepository.findAll();
        if (questions == null)
            return new ResponseEntity<Iterable<Question>>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<Iterable<Question>>(questions, HttpStatus.OK);
    }

    @RequestMapping(value = "/questions-by-subtopic", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Question>> getBySuptopic(@RequestParam("subtopicId") int id) {
        Subtopic subtopic = subtopicRepository.findOne(id);
        if (subtopic == null) {
            return new ResponseEntity<Iterable<Question>>(HttpStatus.NOT_FOUND);
        }
        Iterable<Question> questions = questionRepository.findAllBySubtopic(subtopic);
        if (questions == null) {
            return new ResponseEntity<Iterable<Question>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Question>>(questions, HttpStatus.OK);
    }

    @RequestMapping(value = "/questions/{id}", method = RequestMethod.GET)
    public ResponseEntity<Question> get(@PathVariable("id") int id) {
        Question question = questionRepository.findOne(id);
        if (question == null) {
            return new ResponseEntity<Question>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Question>(question, HttpStatus.OK);
    }

    @RequestMapping(value = "/questions", method = RequestMethod.POST)
    public ResponseEntity<Question> create(@RequestBody Question question) {
        if (questionRepository.exists(question.getId())) {
            return new ResponseEntity<Question>(HttpStatus.CONFLICT);
        }
        questionRepository.save(question);
        question.setSubtopic(subtopicRepository.findOne(question.getSubtopic().getId()));
        question.setType(questionTypeRepository.findOne(question.getType().getId()));
        return new ResponseEntity<Question>(question, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/questions", method = RequestMethod.PUT)
    public ResponseEntity<Question> update(@RequestBody Question question) {
        Question current = questionRepository.findOne(question.getId());
        if (current == null) {
            return new ResponseEntity<Question>(HttpStatus.NOT_FOUND);
        }
        question.setId(current.getId());
        questionRepository.save(question);
        question.setSubtopic(subtopicRepository.findOne(question.getSubtopic().getId()));
        question.setType(questionTypeRepository.findOne(question.getType().getId()));
        return new ResponseEntity<Question>(question, HttpStatus.OK);
    }

    @RequestMapping(value = "/questions/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") int id) {
        Question question = questionRepository.findOne(id);
        if (question == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        questionRepository.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
