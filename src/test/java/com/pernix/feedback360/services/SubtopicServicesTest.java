
package com.pernix.feedback360.services;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pernix.feedback360.Application;
import com.pernix.feedback360.entities.Subtopic;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class SubtopicServicesTest extends BaseTest {

    private static final String PATH = "/subtopics";
    private static final String PATH2 = "/subtopics-by-topic";
    private static final int NOT_FOUND_ID = 16357;

    private Subtopic subtopic;

    @Before
    public void prepareTestMethod() throws Exception {
        topic = createTopic();
        subtopic = createSubtopic();
    }

    @After
    public void cleanTestMethod() throws Exception {
        subtopicRepository.deleteAll();
    }

    @Test
    public void getAll() throws Exception {
        createSubtopic();

        ResultActions result = perform(get(PATH)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(3))).andExpect(jsonPath("$[1].id", is(subtopic.getId())))
                .andExpect(jsonPath("$[1].nameEs", is(subtopic.getNameEs())));
    }

    @Test
    public void getAllByTopic() throws Exception {
        String path = String.format("%s?topicId=%d", PATH2, topic.getId());
        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[1].id", is(subtopic.getId())))
                .andExpect(jsonPath("$[1].nameEs", is(subtopic.getNameEs())));
    }

    @Test
    public void getById() throws Exception {
        String path = String.format("%s/%d", PATH, subtopic.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(subtopic.getId()))).andExpect(jsonPath("nameEs", is(subtopic.getNameEs())));
    }

    @Test
    public void getByIdNotFound() throws Exception {
        String path = String.format("%s/%d", PATH, NOT_FOUND_ID);

        perform(get(path)).andExpect(status().isNotFound());
    }

    @Test
    public void create() throws Exception {
        Subtopic request = new Subtopic();
        request.setNameEn(SUBTOPIC_NAME_EN);
        request.setNameEs(SUBTOPIC_NAME_ES);
        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject topicObject = new JsonObject();
        topicObject.addProperty("id", topic.getId());
        jsonRequest.getAsJsonObject().add("topic", topicObject);

        ResultActions result = perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("nameEn", is(SUBTOPIC_NAME_EN))).andExpect(jsonPath("nameEs", is(SUBTOPIC_NAME_ES)));
    }

    @Test
    public void createConflict() throws Exception {
        Subtopic request = new Subtopic();
        request.setId(subtopic.getId());
        request.setNameEn(SUBTOPIC_NAME_EN);
        request.setNameEs(SUBTOPIC_NAME_ES);
        perform(post(PATH).content(new Gson().toJson(request))).andExpect(status().isConflict());
    }

    @Test
    public void update() throws Exception {

        final String updatedNameEN = "Updated name";
        final String updatedNameES = "Nombre actualizado";

        Subtopic request = subtopic;
        request.setTopic(null);
        request.setNameEn(updatedNameEN);
        request.setNameEs(updatedNameES);

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);
        JsonObject topicObject = new JsonObject();
        topicObject.addProperty("id", topic.getId());
        jsonRequest.getAsJsonObject().add("topic", topicObject);

        ResultActions result = perform(put(PATH).content(jsonRequest.toString())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(subtopic.getId()))).andExpect(jsonPath("nameEs", is(updatedNameES)))
                .andExpect(jsonPath("nameEn", is(updatedNameEN)));
    }

    @Test
    public void destroy() throws Exception {
        String path = String.format("%s/%d", PATH, subtopic.getId());

        performAuthorized(delete(path)).andExpect(status().isMethodNotAllowed());
    }

}
