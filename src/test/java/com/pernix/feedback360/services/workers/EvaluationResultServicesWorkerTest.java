package com.pernix.feedback360.services.workers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.pernix.feedback360.Application;
import com.pernix.feedback360.data.AnswerRepository;
import com.pernix.feedback360.data.EvaluationRepository;
import com.pernix.feedback360.data.EvaluatorRepository;
import com.pernix.feedback360.data.EvaluatorRoleRepository;
import com.pernix.feedback360.data.QuestionRepository;
import com.pernix.feedback360.data.QuestionTypeRepository;
import com.pernix.feedback360.data.RoleRepository;
import com.pernix.feedback360.data.StageRepository;
import com.pernix.feedback360.data.SubtopicRepository;
import com.pernix.feedback360.data.SurveyRepository;
import com.pernix.feedback360.data.TopicRepository;
import com.pernix.feedback360.data.UserRepository;
import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.Question;
import com.pernix.feedback360.entities.QuestionType;
import com.pernix.feedback360.entities.Stage;
import com.pernix.feedback360.entities.Subtopic;
import com.pernix.feedback360.entities.Survey;
import com.pernix.feedback360.entities.Topic;
import com.pernix.feedback360.entities.Users;
import com.pernix.feedback360.entities.util.EvaluationStatus;
import com.pernix.feedback360.entities.util.EvaluatorStatus;
import com.pernix.feedback360.services.pojos.EvaluationSubmit;
import com.pernix.feedback360.services.pojos.results.EvaluationResult;
import com.pernix.feedback360.services.pojos.results.OpenQuestionResult;
import com.pernix.feedback360.services.pojos.results.QuestionResult;
import com.pernix.feedback360.services.pojos.results.SubtopicComparatorResult;
import com.pernix.feedback360.services.pojos.results.SubtopicResult;
import com.pernix.feedback360.services.pojos.results.TopicComparatorResult;
import com.pernix.feedback360.services.pojos.results.TopicResult;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class EvaluationResultServicesWorkerTest {

    private static final String SURVEY_NAME_EN = "Survey name";
    private static final String SURVEY_NAME_ES = "Nombre del questionario";

    private static final String TOPIC_NAME_EN = "Topic name";
    private static final String TOPIC_NAME_ES = "Nombre del tema";

    private static final String SUBTOPIC_NAME_EN = "Subtopic name";
    private static final String SUBTOPIC_NAME_ES = "Nombre del subtema";

    private static final String QUESTION_NAME_EN = "Question name";
    private static final String QUESTION_NAME_ES = "Nombre de la pregunta";

    @Autowired
    private EvaluationResultServicesWorker evaluationResultServicesWorker;
    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private EvaluationRepository evaluationRepository;

    @Autowired
    private EvaluatorRepository evaluatorRepository;

    @Autowired
    private EvaluatorRoleRepository evaluatorRoleRepository;

    @Autowired
    private UserRepository usersRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private StageRepository stageRepository;

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private SubtopicRepository subtopicRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionTypeRepository questionTypeRepository;

    private Survey survey;
    private Topic topic1, topic2;
    private Subtopic subtopic1, subtopic2;
    private Question question1, question2, question3, question4, question5;
    private Evaluation evaluation,evaluationTwo, evaluationThree;
    private Users user1, user2, user3, user4;
    private Evaluator evaluator1, evaluator2, evaluator3, evaluator4;

    @Before
    public void prepareTestMethod() throws Exception {
        survey = createSurvey();
        createTopics();
        createSubtopics();
        createQuestions();
        createUsers();
        evaluation = createEvaluation();
        evaluationTwo = createEvaluation();
        evaluationThree = createEvaluation();
        createEvaluators();
        createAnswers();
    }

    @After
    public void cleanTestMethod() throws Exception {
        evaluationRepository.deleteAll();
        surveyRepository.deleteAll();
        usersRepository.deleteAll();
    }

    private Survey createSurvey() {
        Survey survey = new Survey();
        survey.setNameEn(SURVEY_NAME_EN);
        survey.setNameEs(SURVEY_NAME_ES);
        surveyRepository.save(survey);
        return survey;
    }

    private void createTopics() {
        topic1 = new Topic();
        topic1.setNameEn(TOPIC_NAME_EN);
        topic1.setNameEs(TOPIC_NAME_ES);
        topic1.setSurvey(survey);
        topicRepository.save(topic1);

        topic2 = new Topic();
        topic2.setNameEn(TOPIC_NAME_EN + " Open");
        topic2.setNameEs(TOPIC_NAME_ES + " Open");
        topic2.setSurvey(survey);
        topicRepository.save(topic2);
    }

    private void createSubtopics() {
        subtopic1 = new Subtopic();
        subtopic1.setNameEn(SUBTOPIC_NAME_EN);
        subtopic1.setNameEs(SUBTOPIC_NAME_ES);
        subtopic1.setTopic(topic1);
        subtopicRepository.save(subtopic1);

        subtopic2 = new Subtopic();
        subtopic2.setNameEn(SUBTOPIC_NAME_EN + " Open");
        subtopic2.setNameEs(SUBTOPIC_NAME_ES + " Open");
        subtopic2.setTopic(topic2);
        subtopicRepository.save(subtopic2);
    }

    private void createQuestions() {
        QuestionType closedQuestionType = questionTypeRepository.findByName("Closed");
        QuestionType openQuestionType = questionTypeRepository.findByName("Open");

        question1 = new Question();
        question1.setDescriptionEn(QUESTION_NAME_EN + "1");
        question1.setDescriptionEs(QUESTION_NAME_ES + "1");
        question1.setSubtopic(subtopic1);
        question1.setType(closedQuestionType);
        questionRepository.save(question1);

        question2 = new Question();
        question2.setDescriptionEn(QUESTION_NAME_EN + "2");
        question2.setDescriptionEs(QUESTION_NAME_ES + "2");
        question2.setSubtopic(subtopic1);
        question2.setType(closedQuestionType);
        questionRepository.save(question2);

        question3 = new Question();
        question3.setDescriptionEn(QUESTION_NAME_EN + "3");
        question3.setDescriptionEs(QUESTION_NAME_ES + "3");
        question3.setSubtopic(subtopic1);
        question3.setType(closedQuestionType);
        questionRepository.save(question3);

        question4 = new Question();
        question4.setDescriptionEn(QUESTION_NAME_EN + "4");
        question4.setDescriptionEs(QUESTION_NAME_ES + "4");
        question4.setSubtopic(subtopic2);
        question4.setType(openQuestionType);
        questionRepository.save(question4);

        question5 = new Question();
        question5.setDescriptionEn(QUESTION_NAME_EN + "5");
        question5.setDescriptionEs(QUESTION_NAME_ES + "5");
        question5.setSubtopic(subtopic2);
        question5.setType(openQuestionType);
        questionRepository.save(question5);
    }

    private void createUsers() {

        user1 = new Users();
        user1.setLogin("user1@example.com");
        user1.setName("user1");
        user1.setPassword("user1");
        user1.setRole(roleRepository.findOne(1));
        usersRepository.save(user1);

        user2 = new Users();
        user2.setLogin("user2@example.com");
        user2.setName("user2");
        user2.setPassword("user2");
        user2.setRole(roleRepository.findOne(1));
        usersRepository.save(user2);

        user3 = new Users();
        user3.setLogin("user3@example.com");
        user3.setName("user3");
        user3.setPassword("user3");
        user3.setRole(roleRepository.findOne(1));
        usersRepository.save(user3);

        user4 = new Users();
        user4.setLogin("user4@example.com");
        user4.setName("user4");
        user4.setPassword("user4");
        user4.setRole(roleRepository.findOne(1));
        usersRepository.save(user4);
    }

    private Evaluation createEvaluation() {

        Stage stage = new Stage();
        stage.setEndDate(new Date());
        stage.setStartDate(new Date());
        stage.setNameEs("Periodo 1");
        stage.setNameEn("Stage 1");
        stageRepository.save(stage);

        Evaluation evaluation = new Evaluation();
        evaluation.setStage(stage);
        evaluation.setSurvey(survey);
        evaluation.setUser(user1);
        evaluation.setStatus(EvaluationStatus.COMPLETED);
        evaluationRepository.save(evaluation);

        return evaluation;
    }

    private void createEvaluators() {
        evaluator1 = new Evaluator();
        evaluator1.setEvaluation(evaluation);
        evaluator1.setEvaluatorRole(evaluatorRoleRepository.findByName("Auto"));
        evaluator1.setUser(user1);
        evaluator1.setName(user1.getName());
        evaluator1.setStatus(EvaluatorStatus.STARTED);
        evaluator1.setUniqueLink("Not relevant");
        evaluatorRepository.save(evaluator1);

        evaluator2 = new Evaluator();
        evaluator2.setEvaluation(evaluation);
        evaluator2.setEvaluatorRole(evaluatorRoleRepository.findByName("Jefe"));
        evaluator2.setUser(user2);
        evaluator2.setName(user2.getName());
        evaluator2.setStatus(EvaluatorStatus.STARTED);
        evaluator2.setUniqueLink("Not relevant");
        evaluatorRepository.save(evaluator2);

        evaluator3 = new Evaluator();
        evaluator3.setEvaluation(evaluation);
        evaluator3.setEvaluatorRole(evaluatorRoleRepository.findByName("Par"));
        evaluator3.setUser(user3);
        evaluator3.setName(user3.getName());
        evaluator3.setStatus(EvaluatorStatus.STARTED);
        evaluator3.setUniqueLink("Not relevant");
        evaluatorRepository.save(evaluator3);

        evaluator4 = new Evaluator();
        evaluator4.setEvaluation(evaluation);
        evaluator4.setEvaluatorRole(evaluatorRoleRepository.findByName("Par"));
        evaluator4.setUser(user4);
        evaluator4.setName(user4.getName());
        evaluator4.setStatus(EvaluatorStatus.STARTED);
        evaluator4.setUniqueLink("Not relevant");
        evaluatorRepository.save(evaluator4);

        evaluation.setStatus(EvaluationStatus.INVITATIONS_SENT);
        evaluationRepository.save(evaluation);
    }

    private void createAnswers() {
        Answer answer = new Answer();
        answer.setEvaluation(evaluation);
        answer.setEvaluatorRole(evaluator1.getEvaluatorRole());
        answer.setQuestion(question1);
        answer.setResponse("10");
        answerRepository.save(answer);

        Answer answer2 = new Answer();
        answer2.setEvaluation(evaluation);
        answer2.setEvaluatorRole(evaluator1.getEvaluatorRole());
        answer2.setQuestion(question2);
        answer2.setResponse("10");
        answerRepository.save(answer2);

        Answer answer3 = new Answer();
        answer3.setEvaluation(evaluation);
        answer3.setEvaluatorRole(evaluator1.getEvaluatorRole());
        answer3.setQuestion(question3);
        answer3.setResponse("10");
        answerRepository.save(answer3);

        Answer answer4 = new Answer();
        answer4.setEvaluation(evaluation);
        answer4.setEvaluatorRole(evaluator1.getEvaluatorRole());
        answer4.setQuestion(question4);
        answer4.setResponse("Auto4");
        answerRepository.save(answer4);

        Answer answer5 = new Answer();
        answer5.setEvaluation(evaluation);
        answer5.setEvaluatorRole(evaluator1.getEvaluatorRole());
        answer5.setQuestion(question5);
        answer5.setResponse("Auto5");
        answerRepository.save(answer5);

        Answer answer6 = new Answer();
        answer6.setEvaluation(evaluation);
        answer6.setEvaluatorRole(evaluator2.getEvaluatorRole());
        answer6.setQuestion(question1);
        answer6.setResponse("7");
        answerRepository.save(answer6);

        Answer answer7 = new Answer();
        answer7.setEvaluation(evaluation);
        answer7.setEvaluatorRole(evaluator2.getEvaluatorRole());
        answer7.setQuestion(question2);
        answer7.setResponse("9");
        answerRepository.save(answer7);

        Answer answer8 = new Answer();
        answer8.setEvaluation(evaluation);
        answer8.setEvaluatorRole(evaluator2.getEvaluatorRole());
        answer8.setQuestion(question3);
        answer8.setResponse("8");
        answerRepository.save(answer8);

        Answer answer9 = new Answer();
        answer9.setEvaluation(evaluation);
        answer9.setEvaluatorRole(evaluator2.getEvaluatorRole());
        answer9.setQuestion(question4);
        answer9.setResponse("Jefe4");
        answerRepository.save(answer9);

        Answer answer10 = new Answer();
        answer10.setEvaluation(evaluation);
        answer10.setEvaluatorRole(evaluator2.getEvaluatorRole());
        answer10.setQuestion(question5);
        answer10.setResponse("Jefe5");
        answerRepository.save(answer10);

        Answer answer11 = new Answer();
        answer11.setEvaluation(evaluation);
        answer11.setEvaluatorRole(evaluator3.getEvaluatorRole());
        answer11.setQuestion(question1);
        answer11.setResponse("7");
        answerRepository.save(answer11);

        Answer answer12 = new Answer();
        answer12.setEvaluation(evaluation);
        answer12.setEvaluatorRole(evaluator3.getEvaluatorRole());
        answer12.setQuestion(question2);
        answer12.setResponse("7");
        answerRepository.save(answer12);

        Answer answer13 = new Answer();
        answer13.setEvaluation(evaluation);
        answer13.setEvaluatorRole(evaluator3.getEvaluatorRole());
        answer13.setQuestion(question3);
        answer13.setResponse("7");
        answerRepository.save(answer13);

        Answer answer14 = new Answer();
        answer14.setEvaluation(evaluation);
        answer14.setEvaluatorRole(evaluator3.getEvaluatorRole());
        answer14.setQuestion(question4);
        answer14.setResponse("Par4");
        answerRepository.save(answer14);

        Answer answer15 = new Answer();
        answer15.setEvaluation(evaluation);
        answer15.setEvaluatorRole(evaluator3.getEvaluatorRole());
        answer15.setQuestion(question5);
        answer15.setResponse("Par5");
        answerRepository.save(answer15);

        Answer answer16 = new Answer();
        answer16.setEvaluation(evaluation);
        answer16.setEvaluatorRole(evaluator4.getEvaluatorRole());
        answer16.setQuestion(question1);
        answer16.setResponse("10");
        answerRepository.save(answer16);

        Answer answer17 = new Answer();
        answer17.setEvaluation(evaluation);
        answer17.setEvaluatorRole(evaluator4.getEvaluatorRole());
        answer17.setQuestion(question2);
        answer17.setResponse("10");
        answerRepository.save(answer17);

        Answer answer18 = new Answer();
        answer18.setEvaluation(evaluation);
        answer18.setEvaluatorRole(evaluator4.getEvaluatorRole());
        answer18.setQuestion(question3);
        answer18.setResponse("10");
        answerRepository.save(answer18);

        Answer answer19 = new Answer();
        answer19.setEvaluation(evaluation);
        answer19.setEvaluatorRole(evaluator4.getEvaluatorRole());
        answer19.setQuestion(question4);
        answer19.setResponse("Par4");
        answerRepository.save(answer19);

        Answer answer20 = new Answer();
        answer20.setEvaluation(evaluation);
        answer20.setEvaluatorRole(evaluator4.getEvaluatorRole());
        answer20.setQuestion(question5);
        answer20.setResponse("Par5");
        answerRepository.save(answer20);

        evaluator1.setStatus(EvaluatorStatus.COMPLETED);
        evaluatorRepository.save(evaluator1);
        evaluator2.setStatus(EvaluatorStatus.COMPLETED);
        evaluatorRepository.save(evaluator2);
        evaluator3.setStatus(EvaluatorStatus.COMPLETED);
        evaluatorRepository.save(evaluator3);
        evaluator4.setStatus(EvaluatorStatus.COMPLETED);
        evaluatorRepository.save(evaluator4);

        evaluation.setStatus(EvaluationStatus.COMPLETED);
        evaluationRepository.save(evaluation);
    }
    @Ignore
    @Test
    public void createEvaluationResults() {
        EvaluationResult results = evaluationResultServicesWorker
                .createResults(evaluationRepository.findOne(evaluation.getId()),"es" );

        // Counters
        assertEquals(4, results.getCountOverallEvaluators());
        assertEquals(1, results.getCountSelfAssessmentEvaluators());
        assertEquals(1, results.getCountLeaderEvaluators());
        assertEquals(2, results.getCountPeerEvaluators());
        assertEquals(0, results.getCountCollaboratorEvaluators());

        // Best Questions
        ArrayList<QuestionResult> bestQuestions = results.getBestQuestions();
        assertNotNull(bestQuestions);
        assertEquals(3, bestQuestions.size());
        assertEquals(QUESTION_NAME_ES + "2", bestQuestions.get(0).getStatement());
        assertEquals(QUESTION_NAME_ES + "3", bestQuestions.get(1).getStatement());
        assertEquals(QUESTION_NAME_ES + "1", bestQuestions.get(2).getStatement());
        assertEquals(8.66, bestQuestions.get(0).getGrade(), 0.01);
        assertEquals(8.33, bestQuestions.get(1).getGrade(), 0.01);
        assertEquals(8, bestQuestions.get(2).getGrade(), 0.01);

        // Worst Questions
        ArrayList<QuestionResult> worstQuestions = results.getWorstQuestions();
        assertNotNull(worstQuestions);
        assertEquals(3, worstQuestions.size());
        assertEquals(QUESTION_NAME_ES + "1", worstQuestions.get(0).getStatement());
        assertEquals(QUESTION_NAME_ES + "3", worstQuestions.get(1).getStatement());
        assertEquals(QUESTION_NAME_ES + "2", worstQuestions.get(2).getStatement());
        assertEquals(8, worstQuestions.get(0).getGrade(), 0.01);
        assertEquals(8.33, worstQuestions.get(1).getGrade(), 0.01);
        assertEquals(8.66, worstQuestions.get(2).getGrade(), 0.01);

        // Open Questions
        ArrayList<OpenQuestionResult> openQuestions = results.getOpenQuestions();
        assertNotNull(openQuestions);
        assertEquals(2, openQuestions.size());
        OpenQuestionResult question1 = openQuestions.get(0);
        assertNotNull(question1);
        assertEquals(QUESTION_NAME_ES + "4", question1.getStatement());
        assertEquals("Auto4", question1.getSelfAssesmentResponse());
        assertEquals(1, question1.getLeaderResponses().size());
        assertEquals("Jefe4", question1.getLeaderResponses().get(0));
        assertEquals(2, question1.getPeerResponses().size());
        assertEquals("Par4", question1.getPeerResponses().get(0));
        assertEquals("Par4", question1.getPeerResponses().get(1));
        assertEquals(0, question1.getCollaboratorResponses().size());

        // Self assessment
        ArrayList<TopicResult> topics = results.getSelfAssessmentResults();
        assertNotNull(topics);
        assertEquals(1, topics.size());
        assertEquals(TOPIC_NAME_ES, topics.get(0).getName());
        assertEquals(10.0, topics.get(0).getGrade(), 0);
        ArrayList<SubtopicResult> subtopics = topics.get(0).getSubtopics();
        assertNotNull(subtopics);
        assertEquals(1, subtopics.size());
        assertEquals(SUBTOPIC_NAME_ES, subtopics.get(0).getName());
        assertEquals(10.0, subtopics.get(0).getGrade(), 0);

        // Leader
        topics = results.getLeaderEvaluatorsResults();
        assertNotNull(topics);
        assertEquals(1, topics.size());
        assertEquals(TOPIC_NAME_ES, topics.get(0).getName());
        assertEquals(8.0, topics.get(0).getGrade(), 0);
        subtopics = topics.get(0).getSubtopics();
        assertNotNull(subtopics);
        assertEquals(1, subtopics.size());
        assertEquals(SUBTOPIC_NAME_ES, subtopics.get(0).getName());
        assertEquals(8.0, subtopics.get(0).getGrade(), 0);

        // Peers
        topics = results.getPeerEvaluatorsResults();
        assertNotNull(topics);
        assertEquals(1, topics.size());
        assertEquals(TOPIC_NAME_ES, topics.get(0).getName());
        assertEquals(8.5, topics.get(0).getGrade(), 0);
        subtopics = topics.get(0).getSubtopics();
        assertNotNull(subtopics);
        assertEquals(1, subtopics.size());
        assertEquals(SUBTOPIC_NAME_ES, subtopics.get(0).getName());
        assertEquals(8.5, subtopics.get(0).getGrade(), 0);

        // Collaborators
        topics = results.getCollaboratorEvaluatorsResults();
        assertNotNull(topics);
        assertEquals(1, topics.size());
        assertEquals(TOPIC_NAME_ES, topics.get(0).getName());
        assertEquals(0.0, topics.get(0).getGrade(), 0);
        subtopics = topics.get(0).getSubtopics();
        assertNotNull(subtopics);
        assertEquals(1, subtopics.size());
        assertEquals(SUBTOPIC_NAME_ES, subtopics.get(0).getName());
        assertEquals(0.0, subtopics.get(0).getGrade(), 0);

        // Excludes Self Assessment Results
        topics = results.getEvaluatorsResults();
        assertNotNull(topics);
        assertEquals(1, topics.size());
        assertEquals(TOPIC_NAME_ES, topics.get(0).getName());
        assertEquals(8.33, topics.get(0).getGrade(), 0.01);
        subtopics = topics.get(0).getSubtopics();
        assertNotNull(subtopics);
        assertEquals(1, subtopics.size());
        assertEquals(SUBTOPIC_NAME_ES, subtopics.get(0).getName());
        assertEquals(8.33, subtopics.get(0).getGrade(), 0.01);

        // Overall Results
        ArrayList<TopicComparatorResult> topics1 = results.getOverallResults();
        assertNotNull(topics1);
        assertEquals(1, topics1.size());
        assertEquals(TOPIC_NAME_ES, topics1.get(0).getName());
        assertEquals(10.0, topics1.get(0).getSelfAssessmentGrade(), 0);
        assertEquals(8.33, topics1.get(0).getEvaluatorsAverageGrade(), 0.01);
        ArrayList<SubtopicComparatorResult> subtopics1 = topics1.get(0).getSubtopics();
        assertNotNull(subtopics1);
        assertEquals(1, subtopics1.size());
        assertEquals(SUBTOPIC_NAME_ES, subtopics1.get(0).getName());
        assertEquals(10.0, subtopics1.get(0).getSelfAssessmentGrade(), 0);
        assertEquals(8.33, subtopics1.get(0).getEvaluatorsAverageGrade(), 0.01);
    }

    @Ignore
    @Test
    public void generateCSV() throws Exception {
        evaluationTwo = createEvaluation();
        evaluationThree = createEvaluation();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date startDateFormat = sdf.parse("20120223");
        Date endDateFormat = sdf.parse("20180423");
        evaluation.getStage().setStartDate(startDateFormat);
        evaluation.getStage().setEndDate(endDateFormat);
        evaluationTwo.getStage().setStartDate(startDateFormat);
        evaluationTwo.getStage().setEndDate(endDateFormat);
        evaluationTwo.setStatus(EvaluationStatus.COMPLETED);
        evaluationThree.getStage().setStartDate(startDateFormat);
        evaluationThree.getStage().setEndDate(endDateFormat);
        evaluationThree.setStatus(EvaluationStatus.COMPLETED);
        evaluationResultServicesWorker.GetUserEvaluationsByDate(user1, startDateFormat, endDateFormat);
    }
}
