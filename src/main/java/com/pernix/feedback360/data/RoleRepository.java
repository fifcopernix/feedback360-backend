package com.pernix.feedback360.data;

import org.springframework.data.repository.CrudRepository;

import com.pernix.feedback360.entities.Role;

public interface RoleRepository extends CrudRepository<Role, Integer> {
}
