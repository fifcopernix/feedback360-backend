package com.pernix.feedback360.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.pernix.feedback360.entities.Users;

public interface UserRepository extends CrudRepository<Users, Integer> {

    Users findByLogin(String login);

    @Query("from Users where id=:id")
    Users findById(@Param("id") int id);

    @Query("from Users where office365id=:office365id and office365email=:office365email")
    Users findByOffice365IDAndOffice365Email(@Param("office365id") String office365id, @Param("office365email") String office365email);
}
