package com.pernix.feedback360.services.workers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.pernix.feedback360.Application;
import com.pernix.feedback360.data.EvaluationRepository;
import com.pernix.feedback360.data.QuestionRepository;
import com.pernix.feedback360.data.QuestionTypeRepository;
import com.pernix.feedback360.data.RoleRepository;
import com.pernix.feedback360.data.SubtopicRepository;
import com.pernix.feedback360.data.SurveyRepository;
import com.pernix.feedback360.data.TopicRepository;
import com.pernix.feedback360.data.UserRepository;
import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.Question;
import com.pernix.feedback360.entities.QuestionType;
import com.pernix.feedback360.entities.Stage;
import com.pernix.feedback360.entities.Subtopic;
import com.pernix.feedback360.entities.Survey;
import com.pernix.feedback360.entities.Topic;
import com.pernix.feedback360.entities.Users;
import com.pernix.feedback360.entities.util.EvaluationStatus;
import com.pernix.feedback360.services.pojos.EvaluationRequest;
import com.pernix.feedback360.services.pojos.EvaluationResponse;
import com.pernix.feedback360.services.pojos.EvaluationSubmit;
import com.pernix.feedback360.services.pojos.EvaluatorRequest;
import com.pernix.feedback360.services.pojos.InvitationRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class EvaluationServicesWorkerTest {

    private static final String SURVEY_NAME_EN = "Survey name";
    private static final String SURVEY_NAME_ES = "Nombre del questionario";

    private static final String TOPIC_NAME_EN = "Topic name";
    private static final String TOPIC_NAME_ES = "Nombre del tema";

    private static final String SUBTOPIC_NAME_EN = "Subtopic name";
    private static final String SUBTOPIC_NAME_ES = "Nombre del subtema";

    private static final String QUESTION_NAME_EN = "Question name";
    private static final String QUESTION_NAME_ES = "Nombre de la pregunta";

    @Autowired
    private EvaluationServicesWorker evaluationWorker;
    @Autowired
    private UserRepository usersRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private SurveyRepository surveyRepository;
    @Autowired
    private EvaluationRepository evaluationRepository;

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private SubtopicRepository subtopicRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionTypeRepository questionTypeRepository;

    private Survey survey;
    private Topic topic;
    private Subtopic subtopic;
    private Question question;

    @Before
    public void prepareTestMethod() throws Exception {
        survey = createSurvey();
        topic = createTopic();
        subtopic = createSubtopic();
        question = createQuestion();
    }

    @After
    public void cleanTestMethod() throws Exception {
        evaluationRepository.deleteAll();
        surveyRepository.deleteAll();
    }

    private Survey createSurvey() {
        Survey survey = new Survey();
        survey.setNameEn(SURVEY_NAME_EN);
        survey.setNameEs(SURVEY_NAME_ES);
        surveyRepository.save(survey);
        return survey;
    }

    private Topic createTopic() {
        Topic topic = new Topic();
        topic.setNameEn(TOPIC_NAME_EN);
        topic.setNameEs(TOPIC_NAME_ES);
        topic.setSurvey(survey);
        topicRepository.save(topic);
        return topic;
    }

    private Subtopic createSubtopic() {
        Subtopic subtopic = new Subtopic();
        subtopic.setNameEn(SUBTOPIC_NAME_EN);
        subtopic.setNameEs(SUBTOPIC_NAME_ES);
        subtopic.setTopic(topic);
        subtopicRepository.save(subtopic);
        return subtopic;
    }

    private Question createQuestion() {
        QuestionType type = questionTypeRepository.findOne(2);
        Question question = new Question();
        question.setDescriptionEn(QUESTION_NAME_EN);
        question.setDescriptionEs(QUESTION_NAME_ES);
        question.setSubtopic(subtopic);
        question.setType(type);
        questionRepository.save(question);
        return question;
    }

    private LinkedList<Evaluator> createEvaluators(Evaluation evaluation) {
        InvitationRequest request = new InvitationRequest();
        request.setEvaluation(evaluation);
        EvaluatorRequest evaluatorRequestOne = new EvaluatorRequest();
        evaluatorRequestOne.setEmail("szamora@pernix-solutions.com");
        evaluatorRequestOne.setName("Saul Zamora");
        evaluatorRequestOne.setRole("Par");

        EvaluatorRequest evaluatorRequestTwo = new EvaluatorRequest();

        evaluatorRequestTwo.setEmail("alhernandez@pernixlabs.com");
        evaluatorRequestTwo.setName("Alejandro");
        evaluatorRequestTwo.setRole("Jefe");

        ArrayList<EvaluatorRequest> evaluators = new ArrayList<EvaluatorRequest>();
        evaluators.add(evaluatorRequestTwo);
        evaluators.add(evaluatorRequestTwo);

        request.setEvaluators(evaluators);

        return evaluationWorker.inviteEvaluators(request);
    }

    private LinkedList<Evaluation> createEvaluations() {
        EvaluationRequest request = new EvaluationRequest();
        new Survey();
        Stage stage = new Stage();
        stage.setEndDate(new Date());
        stage.setStartDate(new Date());
        stage.setNameEs("Periodo 1");
        stage.setNameEn("Stage 1");
        LinkedList<String> emails = new LinkedList<String>();
        emails.add("alejandro386hs@gmail.com");

        request.setSurvey(survey);
        request.setEmails(emails);
        request.setStage(stage);

        return evaluationWorker.createEvaluations(request);
    }

    @Ignore
    @Test
    public void adminInviteEvaluatorToStartSurvey() {

        Users user = new Users();
        user.setLogin("szamora@pernix-solutions.com");
        user.setName("Saul Zamora");
        user.setPassword("admin123");
        user.setRole(roleRepository.findOne(1));
        usersRepository.save(user);

        Users user2 = new Users();
        user2.setLogin("alhernandez@pernixlabs.com");
        user2.setName("Alejandro Hernandez");
        user2.setPassword("admin123");
        user2.setRole(roleRepository.findOne(1));
        usersRepository.save(user2);

        EvaluationRequest request = new EvaluationRequest();
        Stage stage = new Stage();
        stage.setEndDate(new Date());
        stage.setStartDate(new Date());
        stage.setNameEs("Periodo 1");
        stage.setNameEn("Stage 1");
        LinkedList<String> emails = new LinkedList<String>();

        emails.add(user.getLogin());
        emails.add(user2.getLogin());

        request.setSurvey(survey);
        request.setEmails(emails);
        request.setStage(stage);

        LinkedList<Evaluation> evaluations = evaluationWorker.createEvaluations(request);
        assertNotNull(evaluations);
        assertEquals(2, evaluations.size());

        Set<Evaluation> evaluationsForUser1 = evaluationRepository.findAllByUser(user);

        assertNotNull(evaluationsForUser1);
        assertEquals(1, evaluationsForUser1.size());

        Evaluation evaluation = (Evaluation) evaluationsForUser1.toArray()[0];
        assertNotNull(evaluation);
        assertEquals(stage.getStartDate(), evaluation.getStage().getStartDate());
        assertEquals(EvaluationStatus.STARTED, evaluation.getStatus());
    }

    @Ignore
    @Test
    public void inviteEvaluators() {

        EvaluationRequest evaluationRequest = new EvaluationRequest();
        Stage stage = new Stage();
        stage.setEndDate(new Date());
        stage.setStartDate(new Date());
        stage.setNameEs("Periodo 1");
        stage.setNameEn("Stage 1");
        LinkedList<String> emails = new LinkedList<String>();
        emails.add("szamora@pernix-solutions.com");
        evaluationRequest.setSurvey(survey);
        evaluationRequest.setEmails(emails);
        evaluationRequest.setStage(stage);
        evaluationRequest.setLanguage("es");

        evaluationWorker.createEvaluations(evaluationRequest);
        InvitationRequest request = new InvitationRequest();
        request.setLanguage("es");
        Evaluation evaluation = evaluationRepository.findAll().iterator().next();
        request.setEvaluation(evaluation);
        EvaluatorRequest evaluatorRequestOne = new EvaluatorRequest();
        evaluatorRequestOne.setEmail("szamora@pernix-solutions.com");
        evaluatorRequestOne.setName("Saul Zamora");
        evaluatorRequestOne.setRole("Par");

        EvaluatorRequest evaluatorRequestTwo = new EvaluatorRequest();

        evaluatorRequestTwo.setEmail("alejandro386hsgmail.com");
        evaluatorRequestTwo.setName("Alejandro");
        evaluatorRequestTwo.setRole("Jefe");

        ArrayList<EvaluatorRequest> evaluators = new ArrayList<EvaluatorRequest>();
        evaluators.add(evaluatorRequestOne);
        evaluators.add(evaluatorRequestTwo);

        request.setEvaluators(evaluators);

        evaluationWorker.inviteEvaluators(request);
    }

    @Ignore
    @Test
    public void sumbitEvaluation() {
        LinkedList<Evaluation> evaluations = createEvaluations();
        LinkedList<Evaluator> evaluators = createEvaluators(evaluations.getFirst());
        for (Evaluator evaluator : evaluators) {
            EvaluationSubmit submit = new EvaluationSubmit();
            submit.setEvaluator(evaluator);
            Answer answer = new Answer();
            answer.setResponse("1");
            answer.setEvaluation(evaluations.getFirst());
            answer.setQuestion(question);
            ArrayList<Answer> answers = new ArrayList<Answer>();
            answers.add(answer);
            submit.setAnswers(answers);
            evaluationWorker.submitEvaluation(submit, evaluator);
        }
    }

    
}
