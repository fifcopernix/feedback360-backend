package com.pernix.feedback360.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Subtopic {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String nameEs;

    @NotEmpty
    @Column(nullable = false)
    private String nameEn;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subtopic")
    private Set<Question> questions;

    @ManyToOne
    @JoinColumn(name = "FK_topicId", nullable = false)
    private Topic topic;

    public Subtopic() {
    }

    public Subtopic(Subtopic subtopic) {
        this.id = subtopic.getId();
        this.nameEs = subtopic.getNameEs();
        this.nameEn = subtopic.getNameEn();
        this.questions = subtopic.getQuestions();
        this.topic = subtopic.getTopic();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEs() {
        return nameEs;
    }

    public void setNameEs(String name) {
        this.nameEs = name;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEN) {
        this.nameEn = nameEN;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

}
