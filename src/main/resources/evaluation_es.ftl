  <!doctype html>
  <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <title></title>  
  </head>
  <body>
    <div style="width:100%!important;min-width:100%;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;text-align:left;line-height:19px;font-size:14px;background:#428bca;margin:0;padding:0" bgcolor="#428bca">
      <table bgcolor="#428bca" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;height:100%;width:100%;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;background:#428bca;margin:0;padding:0">
        <tbody>
          <tr align="left" style="vertical-align:top;text-align:left;padding:0">
            <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:0" valign="top">
              <center style="width:100%;min-width:580px">
                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0">
                  <tbody>
                    <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                      <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:0" valign="top">
                        <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:100%;display:block;padding:0">
                          <tbody>
                            <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                              <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:10px 0 0" valign="top">
                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;margin:0 auto;padding:0">
                                  <tbody>
                                    <tr>
                                      <td style="text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;padding-bottom:20px;padding-top:20px;"><img style="max-height: 150px;" alt="Pernix-Solutions" src="http://fifco360feedback-fe.herokuapp.com/app/images/fifco3.png"></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0">
                  <tbody>
                    <tr align="center" style="vertical-align:top;text-align:left;padding:0">
                      <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:0" valign="top">
                        <table bgcolor="#fff" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:100%;display:block;border-radius:5px;background:#fff;padding:0">
                          <tbody>
                            <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                              <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:20px 0" valign="top">
                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;margin:0 auto;padding:0">
                                  <tbody>
                                    <tr align="center" style="vertical-align:top;text-align:center;padding:0">
                                      <td align="center" style="word-break:break-word;vertical-align:top;text-align:center;color:rgb(34,34,34);font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0px;padding:0px 35px 10px;border-collapse:collapse!important;background:rgb(255,255,255)" valign="top">

<div style="text-align:left;color:#555;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.6;font-size:14px;margin:0 0 10px;padding:0 0 10px">
    <p>Estimado(a) ${employeeName},</p>
    <p>Reciba la m&#225;s cordial bienvenida al Sistema de Retroalimentaci&#243;n 360, dise&#241;ado para medir las percepciones de su l&#237;der, pares, colaboradores, clientes y otros, acerca de los comportamientos que usted demuestra en su contexto laboral en relaci&#243;n con el Modelo de Liderazgo de FIFCO.</p>
    <p>La primera fase del proceso de su Retroalimentaci&#243;n 360 consiste en los siguientes pasos:</p>
    <p>1. <b>USTED PROPONE SU LISTA DE EVALUADORES.</b> Usted como persona interesada en recibir retroalimentaci&#243;n, deber&#225; accesar al enlace que le proveemos al final de este mensaje, e indicar los nombres, los correos electr&#243;nicos y la relaci&#243;n laboral que mantienen con usted las personas que usted desea que sean sus Evaluadores en el proceso, procurando que las personas pertenezcan a las siguientes categor&#237;as:</p>
    <ul>
        <li>L&#237;der (Jefe, persona a la cual usted le reporta directamente)</li>
        <li>Pares/Clientes (Personas de su mismo nivel que trabajan con usted)</li>
        <li>Colaboradores (Personas que le reportan directamente a usted)</li>
        <li>Otros (Personas que no caben en las categor&#237;as anteriores, pero son relevantes en su trabajo)</li>
    </ul>
    <p>Es muy importante que estas personas tengan una relaci&#243;n de trabajo relevante y frecuente con usted desde hace m&#225;s de seis meses. Por favor indique como m&#237;nimo 3 personas por categor&#237;a, (excepto en el caso del L&#237;der), con el fin de garantizar la representatividad de la evaluaci&#243;n (Si no existe alguna de las Categor&#237;as para su caso, ign&#243;rela)</p>
    <p>Una vez que usted complete la lista de Evaluadores, usted deber&#225; hacer click en el bot&#243;n "Enviar lista" de esa p&#225;gina; eso generar&#225; una notificaci&#243;n autom&#225;tica.</p>
    <p>2. <b>USTED AVISA A SUS EVALUADORES.</b> Una vez recibida la validaci&#243;n, le recomendamos fuertemente que usted se asegure de hacerle saber a sus evaluadores que usted los ha incluido en su Retroalimentaci&#243;n 360, y que les llegar&#225; un e-mail de parte de FIFCO 360 invit&#225;ndoles a evaluarlo a usted.</p>
    <p>3. <b>FIFCO 360 LO INVITA A USTED Y A SUS EVALUADORES A INICIAR LA EVALUACI&#211;N 360:</b> nuestro Sistema les invitar&#225; a accesar y completar su Retroalimentaci&#243;n 360. El sistema le indicar&#225; el plazo m&#225;ximo para completar la encuesta.</p>
    <p>Para ingresar al sistema y crear su lista de Evaluadores por favor d&#233; clic en el siguiente enlace: https://fifco360feedback-fe.herokuapp.com/#/dashboard</p>
	    <p>Sus credenciales de ingreso al sistema son:</p>
	    <ul>
	    	<li>Usuario: <span style="color: blue">${login}</span></li>
	    	<li>Contrase&#241;a: <span style=" color: blue">${password}</span></li>
	    </ul>
    <p>Si requiere apoyo por favor comun&#237;quese con FIFCO 360 a la direcci&#243;n fifco360@gmail.com y con gusto le ayudaremos.</p>
    <p>Esperamos que esta experiencia de Retroalimentaci&#243;n 360 le agregue mucho valor.</p>
    <p>Cordialmente,</p>
    <p>FIFCO 360</p>
</div>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0">
                  <tbody>
                    <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                      <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0" valign="top">
                        <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:100%;display:block;padding:0">
                          <tbody style="display:block">
                            <tr align="left" style="vertical-align:top;text-align:left;padding:0;display:block">
                             <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:10px 0 0 0" valign="top">
                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:175px;margin:0 auto;padding:0">
                                  <tbody>
                                    <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                                        <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0 0 10px" valign="top"><!-- <a href="http://www.pernix-solutions.com" target="_blank"><img align="none" alt="Pernix logo" src="https://pernixnewsletter.files.wordpress.com/2015/10/logoo1.png" style="outline:0;text-decoration:none;width:auto;max-width:30px;float:none;clear:both;display:inline-block;margin:0" class="CToWUd"></a> --></td>
                                      <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;width:0;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0" valign="top">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                              <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:10px 10px 0" valign="top">
                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;width:210px;margin:0 auto;padding:0;text-align:center">
                                  <tbody style="display:inline-block">
                                    <tr align="left" style="vertical-align:top;text-align:left;padding:0;display:inline-block">
                                      <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#fff;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0 0 10px" valign="top"><br>
                                      <br>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                              <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:10px 0 0" valign="middle">
                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:right;width:175px!important;margin:0 auto;padding:0">
                                  <tbody style="display:inline-block">
                                    <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                                      <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;width:auto!important;margin:0;padding:0 0 10px" valign="top"><a href="#" style="color:#2ba6cb;text-decoration:none;display:block;width:25px;min-height:auto;text-align:right!important" target="_blank"><img align="none" alt="Pernix on Facebook" src="https://go.pardot.com/l/36622/2015-12-14/71t9yg/36622/93095/fb.png" style="outline:0;text-decoration:none;width:auto;max-width:100%;float:none;clear:both;display:inline-block;margin:0;border:none"></a></td>
                                      <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;width:auto!important;margin:0;padding:0 0 10px" valign="top"><a href="#" style="color:#2ba6cb;text-decoration:none;display:block;width:25px;min-height:auto;text-align:right!important" target="_blank"><img align="none" alt="Pernix on LinkedIn" src="https://go.pardot.com/l/36622/2016-07-18/8yhgn5/36622/109842/linkedin.png" style="outline:0;text-decoration:none;width:auto;max-width:100%;float:none;clear:both;display:inline-block;margin:0;border:none"></a></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </center>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </body>
  </html>