package com.pernix.feedback360.services.pojos;

public class QuestionResultResponse {

    private int id;
    private String statementEs;
    private String statementEn;
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatementEs() {
        return statementEs;
    }

    public void setStatementEs(String statementES) {
        this.statementEs = statementES;
    }

    public String getStatementEn() {
        return statementEn;
    }

    public void setStatementEn(String statementEN) {
        this.statementEn = statementEN;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
