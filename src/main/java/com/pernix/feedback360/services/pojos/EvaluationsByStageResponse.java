package com.pernix.feedback360.services.pojos;

import java.util.Date;

import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Stage;

public class EvaluationsByStageResponse {
    private int id;
    private String nameEs;
    private String nameEn;
    private Date startDate;
    private Date endDate;
    private Iterable<Evaluation> evaluations;

    public EvaluationsByStageResponse(Stage stage, Iterable<Evaluation> evaluations) {
        this.id = stage.getId();
        this.nameEn = stage.getNameEn();
        this.nameEs = stage.getNameEs();
        this.startDate = stage.getStartDate();
        this.endDate = stage.getEndDate();
        this.evaluations = evaluations;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEs() {
        return nameEs;
    }

    public void setNameEs(String nameEs) {
        this.nameEs = nameEs;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Iterable<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(Iterable<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }

}
