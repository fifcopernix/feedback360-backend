package com.pernix.feedback360.services;

import com.pernix.feedback360.data.EvaluationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.feedback360.data.SurveyRepository;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Survey;
import com.pernix.feedback360.services.pojos.SurveyResultResponse;
import com.pernix.feedback360.services.workers.SurveyServicesWorker;
import java.util.Set;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
public class SurveyServices {

    private final SurveyRepository surveyRepository;
    private final SurveyServicesWorker surveyServicesWorker;
    private final EvaluationRepository evaluationRepository;

    @Autowired
    public SurveyServices(SurveyRepository surveyRepository, SurveyServicesWorker surveyServicesWorker,
            EvaluationRepository evaluationRepository) {
        this.surveyRepository = surveyRepository;
        this.surveyServicesWorker = surveyServicesWorker;
        this.evaluationRepository = evaluationRepository;
    }

    @RequestMapping(value = "/surveys", method = RequestMethod.GET)
    public ResponseEntity<Iterable<SurveyResultResponse>> getAllSurveys() {
        Iterable<Survey> surveys = surveyRepository.findAll();
        if (surveys == null) {
            return new ResponseEntity<Iterable<SurveyResultResponse>>(HttpStatus.NO_CONTENT);
        }
        Iterable<SurveyResultResponse> results = surveyServicesWorker.getAll(surveys);
        return new ResponseEntity<Iterable<SurveyResultResponse>>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "/surveys/{id}", method = RequestMethod.GET)
    public ResponseEntity<SurveyResultResponse> get(@PathVariable(value = "id") int id) {
        Survey survey = surveyRepository.findOne(id);
        if (survey == null) {
            return new ResponseEntity<SurveyResultResponse>(HttpStatus.NOT_FOUND);
        }
        SurveyResultResponse response = surveyServicesWorker.get(survey);
        return new ResponseEntity<SurveyResultResponse>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/surveys", method = RequestMethod.POST)
    public ResponseEntity<Survey> create(@RequestBody Survey survey) {

        if (surveyRepository.exists(survey.getId())) {
            return new ResponseEntity<Survey>(HttpStatus.CONFLICT);
        }
        surveyRepository.save(survey);
        surveyRepository.findOne(survey.getId());
        return new ResponseEntity<Survey>(survey, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/surveys", method = RequestMethod.PUT)
    public ResponseEntity<Survey> update(@RequestBody Survey survey) {

        Survey current = surveyRepository.findOne(survey.getId());
        if (current == null) {
            return new ResponseEntity<Survey>(HttpStatus.NOT_FOUND);
        }
        survey.setId(current.getId());
        surveyRepository.save(survey);
        return new ResponseEntity<Survey>(survey, HttpStatus.OK);
    }

    @RequestMapping(value = "/surveys/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") int surveyId) {

        Survey current = surveyRepository.findOne(surveyId);
        if (current == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Set<Evaluation> evaluationsSurvey = evaluationRepository.findAllBySurvey(current);

        evaluationsSurvey.forEach((evaluationSurvey) -> {
            System.out.println("language " + evaluationSurvey.getLanguage());
            evaluationRepository.delete(evaluationSurvey);
        });

        surveyRepository.delete(current);
        return new ResponseEntity(HttpStatus.OK);
    }

}
