package com.pernix.feedback360.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.feedback360.data.QuestionTypeRepository;
import com.pernix.feedback360.entities.QuestionType;

@RestController
public class QuestionTypeServices {

    private final QuestionTypeRepository questionTypeRepository;

    @Autowired
    public QuestionTypeServices(QuestionTypeRepository questionTypeRepository) {
        this.questionTypeRepository = questionTypeRepository;
    }

    @RequestMapping(value = "/questionTypes", method = RequestMethod.GET)
    public ResponseEntity<Iterable<QuestionType>> getAll() {
        Iterable<QuestionType> questionTypes = questionTypeRepository.findAll();
        if (questionTypes == null) {
            return new ResponseEntity<Iterable<QuestionType>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<QuestionType>>(questionTypes, HttpStatus.OK);
    }

    @RequestMapping(value = "/questionTypes/{id}", method = RequestMethod.GET)
    public ResponseEntity<QuestionType> get(@PathVariable int id) {
        QuestionType questionType = questionTypeRepository.findOne(id);
        if (questionType == null) {
            return new ResponseEntity<QuestionType>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<QuestionType>(questionType, HttpStatus.OK);
    }

}
