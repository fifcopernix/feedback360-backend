package com.pernix.feedback360.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class EvaluatorRole {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "evaluatorRole")
    private Set<Evaluator> evaluators;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "evaluatorRole")
    private Set<Answer> answers;

    public EvaluatorRole() {

    }

    public EvaluatorRole(EvaluatorRole evaluatorRole) {
        this.id = evaluatorRole.getId();
        this.name = evaluatorRole.getName();
        this.evaluators = evaluatorRole.getEvaluators();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Evaluator> getEvaluators() {
        return evaluators;
    }

    public void setEvaluators(Set<Evaluator> evaluators) {
        this.evaluators = evaluators;
    }

}
