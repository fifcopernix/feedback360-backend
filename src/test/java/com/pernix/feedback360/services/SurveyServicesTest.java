
package com.pernix.feedback360.services;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.google.gson.Gson;
import com.pernix.feedback360.Application;
import com.pernix.feedback360.data.EvaluationRepository;
import com.pernix.feedback360.data.SurveyRepository;
import com.pernix.feedback360.entities.Survey;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class SurveyServicesTest extends BaseTest {

    private static final String PATH = "/surveys";
    private static final int NOT_FOUND_ID = 16357;
    private static final String SURVEY_NAME_EN = "Survey name";
    private static final String SURVEY_NAME_ES = "Nombre del questionario";

    @Autowired
    private SurveyRepository surveyRepository;
    @Autowired
    private EvaluationRepository evaluationRepository;

    private Survey survey;

    @Before
    public void prepareTestMethod() throws Exception {
        evaluationRepository.deleteAll();
        surveyRepository.deleteAll();
        survey = createSurvey("es");
    }

    @After
    public void cleanTestMethod() throws Exception {
        surveyRepository.deleteAll();
    }

    private Survey createSurvey(String language) {
        Survey survey = new Survey();
        survey.setNameEn(SURVEY_NAME_EN);
        survey.setNameEs(SURVEY_NAME_ES);
        surveyRepository.save(survey);
        return survey;
    }

    @Test
    public void getAll() throws Exception {
        createSurvey("es");
        ResultActions result = perform(get(PATH)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        result.andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void getById() throws Exception {
        String path = String.format("%s/%d", PATH, survey.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        result.andExpect(jsonPath("id", is(survey.getId()))).andExpect(jsonPath("nameEs", is(survey.getNameEs())));
    }

    @Test
    public void getByIdNotFound() throws Exception {
        String path = String.format("%s/%d/%s", PATH, NOT_FOUND_ID, "es");

        perform(get(path)).andExpect(status().isNotFound());
    }

    @Test
    public void create() throws Exception {
        Survey request = new Survey();
        request.setNameEn(SURVEY_NAME_EN);
        request.setNameEs(SURVEY_NAME_ES);

        ResultActions result = perform(post(PATH).content(new Gson().toJson(request))).andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("nameEs", is(SURVEY_NAME_ES)));
    }

    @Test
    public void createConflict() throws Exception {
        Survey request = survey;
        request.setNameEn(SURVEY_NAME_EN);
        request.setNameEs(SURVEY_NAME_ES);

        perform(post(PATH).content(new Gson().toJson(request))).andExpect(status().isConflict());
    }

    @Test
    public void update() throws Exception {
        final String updatedName = "Updated name";

        Survey request = survey;
        request.setNameEn(updatedName);
        request.setNameEs(SURVEY_NAME_ES);

        ResultActions result = perform(put(PATH).content(new Gson().toJson(request))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(survey.getId()))).andExpect(jsonPath("nameEn", is(updatedName)));
    }

}
