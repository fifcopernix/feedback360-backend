package com.pernix.feedback360.data;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.EvaluatorRole;
import com.pernix.feedback360.entities.Question;

public interface AnswerRepository extends CrudRepository<Answer, Integer> {
	
	Answer findById(int id);
	
    Set<Answer> findAllByEvaluation(Evaluation evaluation);

    Set<Answer> findAllByQuestionAndEvaluatorRole(Question question, EvaluatorRole evaluatorRole);

    Set<Answer> findAllByQuestionAndEvaluatorRoleIn(Question question, ArrayList<EvaluatorRole> evaluatorRoles);

    Set<Answer> findAllByQuestion(Question question);
    
    Set<Answer> findAllByEvaluatorAndEvaluation(Evaluator evaluator, Evaluation evaluation);
    
    Answer findAllByQuestionAndEvaluator(Question question, Evaluator evaluator);

    
}
