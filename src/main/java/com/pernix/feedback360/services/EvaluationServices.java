package com.pernix.feedback360.services;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.feedback360.data.EvaluationRepository;
import com.pernix.feedback360.data.EvaluatorRepository;
import com.pernix.feedback360.data.StageRepository;
import com.pernix.feedback360.data.UserRepository;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.Stage;
import com.pernix.feedback360.entities.Users;
import com.pernix.feedback360.entities.util.EvaluationStatus;
import com.pernix.feedback360.entities.util.EvaluatorStatus;
import com.pernix.feedback360.services.pojos.EvaluationRequest;
import com.pernix.feedback360.services.pojos.EvaluationResponse;
import com.pernix.feedback360.services.pojos.EvaluationSubmit;
import com.pernix.feedback360.services.pojos.EvaluationsByStageResponse;
import com.pernix.feedback360.services.pojos.InvitationRequest;
import com.pernix.feedback360.services.pojos.ResponseCsv;
import com.pernix.feedback360.services.workers.EvaluationServicesWorker;

@RestController
public class EvaluationServices {

    private final EvaluationRepository evaluationRepository;
    private final StageRepository stageRepository;
    private final EvaluatorRepository evaluatorRepository;
    private final UserRepository userRepository;
    private final EvaluationServicesWorker evaluationServicesWorker;

    @Autowired
    public EvaluationServices(EvaluationRepository evaluationRepository, StageRepository stageRepository,
            EvaluatorRepository evaluatorRepository, EvaluationServicesWorker evaluationServicesWorker,
            UserRepository userRepository) {
        this.evaluationRepository = evaluationRepository;
        this.stageRepository = stageRepository;
        this.evaluatorRepository = evaluatorRepository;
        this.evaluationServicesWorker = evaluationServicesWorker;
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/evaluations", method = RequestMethod.GET)
    public ResponseEntity<Iterable<EvaluationResponse>> getAll() {
        Iterable<Evaluation> allEvaluations = evaluationRepository.findAll();
        Iterable<EvaluationResponse> evaluations = evaluationServicesWorker.filterAddEvaluators(allEvaluations);
        if (evaluations == null) {
            return new ResponseEntity<Iterable<EvaluationResponse>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<EvaluationResponse>>(evaluations, HttpStatus.OK);
    }

    @RequestMapping(value = "/evaluations-by-stage", method = RequestMethod.GET)
    public ResponseEntity<Iterable<EvaluationsByStageResponse>> getByStage() {
        Iterable<Stage> stages = stageRepository.findAll();
        if (stages == null) {
            return new ResponseEntity<Iterable<EvaluationsByStageResponse>>(HttpStatus.NOT_FOUND);
        }
        Iterable<EvaluationsByStageResponse> response = evaluationServicesWorker.evaluationByStage(stages);
        if (response == null) {
            return new ResponseEntity<Iterable<EvaluationsByStageResponse>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<EvaluationsByStageResponse>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/evaluations-by-evaluator", method = RequestMethod.GET)
    public ResponseEntity<Iterable<EvaluationResponse>> getByEvaluator(@RequestParam("evaluatorId") int id) {
        Users user = userRepository.findById(id);
        if (user == null) {
            return new ResponseEntity<Iterable<EvaluationResponse>>(HttpStatus.NOT_FOUND);
        }
        Set<Evaluator> evaluators = evaluatorRepository.findAllByUser(user);

        if (evaluators == null) {
            return new ResponseEntity<Iterable<EvaluationResponse>>(HttpStatus.NOT_FOUND);
        }
        Set<Evaluation> allEvaluations = new HashSet<Evaluation>();
        for (Evaluator evaluator: evaluators) {
            allEvaluations.addAll(evaluationRepository.findAllByEvaluator(evaluator));
        }
        Iterable<EvaluationResponse> evaluations = evaluationServicesWorker.filterAddEvaluators(allEvaluations);
        if (evaluations == null) {
            return new ResponseEntity<Iterable<EvaluationResponse>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<EvaluationResponse>>(evaluations, HttpStatus.OK);
    }

    @RequestMapping(value = "/evaluations-by-user", method = RequestMethod.GET)
    public ResponseEntity<Iterable<EvaluationResponse>> getByUser(@RequestParam("userId") int id) {
        Users user = userRepository.findById(id);

        if (user == null) {
            return new ResponseEntity<Iterable<EvaluationResponse>>(HttpStatus.NOT_FOUND);
        }
        Iterable<EvaluationResponse> evaluations = evaluationServicesWorker
                .filterAddEvaluators(evaluationRepository.findAllByUser(user));
        if (evaluations == null || evaluationRepository.findAllByUser(user).isEmpty()) {
            return new ResponseEntity<Iterable<EvaluationResponse>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<EvaluationResponse>>(evaluations, HttpStatus.OK);
    }

    @RequestMapping(value = "/evaluations-completed", method = RequestMethod.GET)
    public ResponseEntity<Boolean> evaluationCompleted(@RequestParam("evaluatorId") int evaluatorId) {
        Evaluator evaluator = evaluatorRepository.findById(evaluatorId);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd"); 
        LocalDateTime now = LocalDateTime.now();

        if(dtf.format(now).compareTo(evaluator.getEvaluation().getStage().getEndDate().toString()) > 0) {
            return new ResponseEntity<Boolean>(true, HttpStatus.CONFLICT);
        }

        if (evaluator == null) {
            return new ResponseEntity<Boolean>(false, HttpStatus.NOT_FOUND);
        }
        else if (evaluator.getStatus().toString().equals(EvaluationStatus.COMPLETED.toString())) {
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        }
        else {
        	return new ResponseEntity<Boolean>(false,HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/evaluations", method = RequestMethod.POST)
    public ResponseEntity<Iterable<Evaluation>> create(@RequestBody EvaluationRequest evaluationRequest,
            HttpServletRequest request) {
        boolean existsEmails = false;
        for (String mail : evaluationRequest.getEmails()) {
            if (evaluationServicesWorker.checkDomain(mail)) {
                existsEmails = true;
            }
        }
        if (!existsEmails) {
            return new ResponseEntity<Iterable<Evaluation>>(HttpStatus.CONFLICT);
        }
        Iterable<Evaluation> evaluations = evaluationServicesWorker.createEvaluations(evaluationRequest);

        return new ResponseEntity<Iterable<Evaluation>>(evaluations, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/evaluations/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Evaluation> update(@PathVariable("id") int id, @RequestBody Evaluation evaluation) {
    	Evaluation current = evaluationRepository.findOne(id);
        if (current == null) {
            return new ResponseEntity<Evaluation>(HttpStatus.NOT_FOUND);
        }
        evaluation.setId(current.getId());
        evaluationRepository.save(evaluation);
        return new ResponseEntity<Evaluation>(evaluation, HttpStatus.OK);
    }

    @RequestMapping(value = "/evaluations-invite", method = RequestMethod.POST)
    public ResponseEntity<Iterable<Evaluator>> invite(@RequestBody InvitationRequest request) {
        Iterable<Evaluator> evaluators = evaluationServicesWorker.inviteEvaluators(request);
        return new ResponseEntity<Iterable<Evaluator>>(evaluators, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/evaluations-check-key", method = RequestMethod.GET)
    public ResponseEntity<Evaluator> verifyKey(@RequestParam("key") String key, HttpServletRequest request) {
        Evaluator evaluator = evaluatorRepository.findByUniqueLink(key);
        if (evaluator == null) {
            return new ResponseEntity<Evaluator>(HttpStatus.NOT_FOUND);
        }
        
        if (evaluator.getStatus().equals(EvaluatorStatus.COMPLETED) ||
        		evaluator.getEvaluation().getStatus().equals(EvaluationStatus.COMPLETED)) {
            return new ResponseEntity<Evaluator>(HttpStatus.CONFLICT);
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();

        if(dtf.format(now).compareTo(evaluator.getEvaluation().getStage().getEndDate().toString()) > 0) {
            return new ResponseEntity<Evaluator>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<Evaluator>(evaluator, HttpStatus.OK);
    }

    @RequestMapping(value = "/evaluations-submit", method = RequestMethod.POST)
    public ResponseEntity<Evaluator> submitEvaluation(@RequestBody EvaluationSubmit evaluationSubmit) {
        if (!evaluatorRepository.exists(evaluationSubmit.getEvaluator().getId())) {
            return new ResponseEntity<Evaluator>(HttpStatus.NOT_FOUND);
        }
        Evaluator evaluator = evaluatorRepository.findOne(evaluationSubmit.getEvaluator().getId());
        evaluator = evaluationServicesWorker.submitEvaluation(evaluationSubmit, evaluator);
        return new ResponseEntity<Evaluator>(evaluator, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/evaluations-draft", method = RequestMethod.POST)
    public ResponseEntity<Evaluator> draftEvaluation(@RequestBody EvaluationSubmit evaluationSubmit) {
        if (!evaluatorRepository.exists(evaluationSubmit.getEvaluator().getId())) {
            return new ResponseEntity<Evaluator>(HttpStatus.NOT_FOUND);
        }
        Evaluator evaluator = evaluatorRepository.findOne(evaluationSubmit.getEvaluator().getId());
        evaluator = evaluationServicesWorker.draftEvaluation(evaluationSubmit, evaluator);
        return new ResponseEntity<Evaluator>(evaluator, HttpStatus.OK);
    }

    @RequestMapping(value = "/generate-report", method = RequestMethod.GET)
    public ResponseEntity<Evaluation> generateReport(@RequestParam("id") int id, HttpServletResponse response)
            throws IOException {
        Evaluation current = evaluationRepository.findOne(id);
        if (current == null) {
            return new ResponseEntity<Evaluation>(HttpStatus.NOT_FOUND);
        }
        ResponseCsv dataResult = evaluationServicesWorker.generateCSV(current);

        response.setContentType("text-csv");
        response.setHeader("Content-Disposition", "attachment; filename=" + dataResult.getFilename());

        OutputStream writer = response.getOutputStream();

        writer.write(dataResult.getHeader().getBytes());
        writer.write(dataResult.getData().getBytes());
        writer.close();
        return new ResponseEntity<Evaluation>(HttpStatus.OK);

    }
}
