package com.pernix.feedback360.services.workers;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.pernix.feedback360.entities.Question;
import com.pernix.feedback360.entities.Subtopic;
import com.pernix.feedback360.entities.Survey;
import com.pernix.feedback360.entities.Topic;
import com.pernix.feedback360.services.pojos.QuestionResultResponse;
import com.pernix.feedback360.services.pojos.SubtopicResultResponse;
import com.pernix.feedback360.services.pojos.SurveyResultResponse;
import com.pernix.feedback360.services.pojos.TopicResultResponse;

@Service
public class SurveyServicesWorker {

    public SurveyServicesWorker() {
    }

    public Iterable<SurveyResultResponse> getAll(Iterable<Survey> surveys) {
        HashMap<Integer, SurveyResultResponse> results = new HashMap<Integer, SurveyResultResponse>();
        for (Survey survey : surveys) {
            SurveyResultResponse result = get(survey);
            results.put(survey.getId(), result);
        }
        return results.values();
    }

    public SurveyResultResponse get(Survey survey) {
        SurveyResultResponse result = new SurveyResultResponse();
        result.setId(survey.getId());
        result.setNameEs(survey.getNameEs());
        result.setNameEn(survey.getNameEn());

        Iterable<Topic> topics = survey.getTopics();
        for (Topic topic : topics) {
            TopicResultResponse topicResult = new TopicResultResponse();
            topicResult.setId(topic.getId());
            topicResult.setNameES(topic.getNameEs());
            topicResult.setNameEN(topic.getNameEn());

            Iterable<Subtopic> subtopics = topic.getSubtopics();
            for (Subtopic subtopic : subtopics) {
                SubtopicResultResponse subtopicResult = new SubtopicResultResponse();
                subtopicResult.setId(subtopic.getId());
                subtopicResult.setNameEs(subtopic.getNameEs());
                subtopicResult.setNameEn(subtopic.getNameEn());

                Iterable<Question> questions = subtopic.getQuestions();
                for (Question question : questions) {
                    QuestionResultResponse questionResult = new QuestionResultResponse();
                    questionResult.setId(question.getId());
                    questionResult.setStatementEs(question.getDescriptionEs());
                    questionResult.setStatementEn(question.getDescriptionEn());
                    questionResult.setType(question.getType().getName());

                    subtopicResult.getQuestions().add(questionResult);
                }
                topicResult.getSubtopics().add(subtopicResult);
            }
            result.getTopics().add(topicResult);
        }
        return result;
    }

}
