package com.pernix.feedback360.services.workers;

import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.apache.commons.text.CharacterPredicates.LETTERS;

import com.pernix.feedback360.data.AllowedDomainRepository;
import com.pernix.feedback360.data.AnswerRepository;
import com.pernix.feedback360.data.EvaluationRepository;
import com.pernix.feedback360.data.EvaluatorRepository;
import com.pernix.feedback360.data.EvaluatorRoleRepository;
import com.pernix.feedback360.data.RoleRepository;
import com.pernix.feedback360.data.StageRepository;
import com.pernix.feedback360.data.SurveyRepository;
import com.pernix.feedback360.data.UserRepository;
import com.pernix.feedback360.entities.AllowedDomain;
import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.EvaluatorRole;
import com.pernix.feedback360.entities.Role;
import com.pernix.feedback360.entities.Stage;
import com.pernix.feedback360.entities.Users;
import com.pernix.feedback360.entities.util.EvaluationStatus;
import com.pernix.feedback360.entities.util.EvaluatorStatus;
import com.pernix.feedback360.services.pojos.EvaluationRequest;
import com.pernix.feedback360.services.pojos.EvaluationResponse;
import com.pernix.feedback360.services.pojos.EvaluationSubmit;
import com.pernix.feedback360.services.pojos.EvaluationsByStageResponse;
import com.pernix.feedback360.services.pojos.EvaluatorRequest;
import com.pernix.feedback360.services.pojos.InvitationRequest;
import com.pernix.feedback360.services.pojos.ResponseCsv;
import com.pernix.feedback360.utils.CSVGenerator;
import com.pernix.feedback360.utils.MailUtil;
import com.pernix.feedback360.utils.UniqueLinkGenerator;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class EvaluationServicesWorker {

    private final EvaluationRepository evaluationRepository;
    private final EvaluatorRoleRepository evaluatorRoleRepository;
    private final UserRepository userRepository;
    private final StageRepository stageRepository;
    private final MailUtil mailUtil;
    private final EvaluatorRepository evaluatorRepository;
    private final AnswerRepository answerRepository;
    private final SurveyRepository surveyRepository;
    private final RoleRepository roleRepository;
    private final AllowedDomainRepository allowedDomainRepository;
    private final CSVGenerator csvGenerator;

    @Autowired
    public EvaluationServicesWorker(EvaluationRepository evaluationRepository,
            EvaluatorRoleRepository evaluatorRoleRepository,
            EvaluatorRepository evaluatorRepository,
            UserRepository userRepository, StageRepository stageRepository, MailUtil mailUtil,
            AnswerRepository answerRepository, SurveyRepository surveyRepository,
            RoleRepository roleRepository,
            AllowedDomainRepository allowedDomainRepository, CSVGenerator csvGenerator) {

        this.evaluationRepository = evaluationRepository;
        this.evaluatorRoleRepository = evaluatorRoleRepository;
        this.userRepository = userRepository;
        this.evaluatorRepository = evaluatorRepository;
        this.stageRepository = stageRepository;
        this.mailUtil = mailUtil;
        this.answerRepository = answerRepository;
        this.surveyRepository = surveyRepository;
        this.roleRepository = roleRepository;

        this.allowedDomainRepository = allowedDomainRepository;
        this.csvGenerator = csvGenerator;
    }

    public LinkedList<EvaluationsByStageResponse> evaluationByStage(Iterable<Stage> stages) {
        LinkedList<EvaluationsByStageResponse> request =
            new LinkedList<EvaluationsByStageResponse>();
        for (Stage stage : stages) {
            EvaluationsByStageResponse response = new EvaluationsByStageResponse(stage,
                    evaluationRepository.findAllByStage(stage));
            request.add(response);

        }
        return request;
    }

    public LinkedList<EvaluationResponse> filterAddEvaluators(Iterable<Evaluation> evaluations) {
        LinkedList<EvaluationResponse> request = new LinkedList<EvaluationResponse>();
        for (Evaluation evaluation : evaluations) {
            EvaluationResponse response = new EvaluationResponse();
            response.setId(evaluation.getId());
            response.setStage(evaluation.getStage());
            response.setSurvey(evaluation.getSurvey());
            response.setStatus(evaluation.getStatus());
            response.setUser(evaluation.getUser());
            response.setEvaluators(evaluatorRepository.findAllByEvaluation(evaluation));
            request.add(response);

        }
        return request;
    }

    public boolean checkDomain(String mail) {

        int domainIndex = mail.indexOf('@');
        String domain = mail.substring(domainIndex).toLowerCase();
        Iterable<AllowedDomain> allowedDomains = allowedDomainRepository.findAll();

        for (AllowedDomain allowed : allowedDomains) {
            if (domain.equals(allowed.getDomain())) {
                return true;
            }
        }
        return false;
    }

    public LinkedList<Evaluation> createEvaluations(EvaluationRequest request) {
        LinkedList<Evaluation> evaluations = new LinkedList<Evaluation>();
        Stage stage = request.getStage();
        String language = request.getLanguage();
        stageRepository.save(stage);

        for (String mail : request.getEmails()) {

            Evaluation evaluation = new Evaluation();
            evaluation.setStatus(EvaluationStatus.STARTED);
            evaluation.setStage(stage);
            evaluation.setSurvey(surveyRepository.findOne(request.getSurvey().getId()));
            Users user = userRepository.findByLogin(mail);

            if (user != null) {

                evaluation.setUser(user);

                evaluationRepository.save(evaluation);

                mailUtil.sendEvaluationNotification(evaluation, language);

                evaluations.add(evaluation);
            } else if (checkDomain(mail)) {
                RandomStringGenerator generator =
                    new RandomStringGenerator.Builder().withinRange('0', 'z')
                        .filteredBy(LETTERS, DIGITS).build();

                String password = generator.generate(6);
                user = new Users();
                user.setLogin(mail);
                user.setPassword(password);
                user.setName(mail);
                Role role = roleRepository.findOne(3);
                user.setRole(role);
                user.setEvaluations(null);
                user.setEvaluator(null);

                userRepository.save(user);
                evaluation.setUser(user);

                evaluationRepository.save(evaluation);

                mailUtil.sendEvaluationNotification(evaluation, language);

                evaluations.add(evaluation);

            }
            EvaluatorRequest evaluator = new EvaluatorRequest();
            evaluator.setName(user.getName());
            evaluator.setEmail(user.getLogin());
            evaluator.setRole("Auto");

            LinkedList<EvaluatorRequest> evaluatorRequestList = new LinkedList<EvaluatorRequest>();
            evaluatorRequestList.add(evaluator);

            InvitationRequest invitationRequest = new InvitationRequest();

            invitationRequest.setEvaluation(evaluation);
            invitationRequest.setEvaluators(evaluatorRequestList);

            inviteEvaluators(invitationRequest);
        }

        return evaluations;

    }

    public Evaluator submitEvaluation(EvaluationSubmit evaluationSubmit, Evaluator evaluator) {
        Iterable<Answer> answers = evaluationSubmit.getAnswers();
        for (Answer answer : answers) {
    		Answer answerRecord = answerRepository.findAllByQuestionAndEvaluator(answer.getQuestion(), evaluator);
    		if(answerRecord != null) {
    			answerRecord.setEvaluatorRole(evaluator.getEvaluatorRole());
    			answerRecord.setEvaluator(evaluator);
        		answerRecord.setResponse(answer.getResponse());
                answerRepository.save(answerRecord);
    		}
    		else {
    			answer.setEvaluatorRole(evaluator.getEvaluatorRole());
                answer.setEvaluator(evaluator);
                answerRepository.save(answer);
    		}
        }
        evaluator.setStatus(EvaluatorStatus.COMPLETED);
        evaluator = evaluatorRepository.save(evaluator);
        Evaluation evaluation = evaluationRepository.findOne(evaluator.getEvaluation().getId());
        Set<Evaluator> evaluators = evaluatorRepository.findAllByStatusAndEvaluation(
            EvaluatorStatus.COMPLETED,
            evaluator.getEvaluation());
        int evaluationsCompleted = evaluators.size();
        int evaluatorsTotal = evaluatorRepository.findAllByEvaluation(evaluation).size();
        if (evaluatorsTotal > 1) {
            if (evaluationsCompleted == evaluatorsTotal) {
                evaluation.setStatus(EvaluationStatus.COMPLETED);
                evaluationRepository.save(evaluation);
            }
        }
        mailUtil.sendEvaluatorCompleted(evaluation, evaluator);
        return evaluator;
    }
    
    public Evaluator draftEvaluation(EvaluationSubmit evaluationSubmit, Evaluator evaluator) {
        Iterable<Answer> answers = evaluationSubmit.getAnswers();
        for (Answer answer : answers) {
        	if(answer.getResponse() != "" && answer.getResponse() != null) {
        		Answer answerRecord = answerRepository.findAllByQuestionAndEvaluator(answer.getQuestion(), evaluator);
        		if(answerRecord != null) {
            		answerRecord.setEvaluatorRole(evaluator.getEvaluatorRole());
            		answerRecord.setEvaluator(evaluator);
            		answerRecord.setResponse(answer.getResponse());
                    answerRepository.save(answerRecord);
        		}
        		else {
        			answer.setEvaluatorRole(evaluator.getEvaluatorRole());
        			answer.setEvaluator(evaluator);
                    answerRepository.save(answer);
        		}

        	}
        }
        return evaluator;
    }

    public LinkedList<Evaluator> inviteEvaluators(InvitationRequest request) {
        LinkedList<Evaluator> evaluators = new LinkedList<Evaluator>();
        int id = request.getEvaluation().getId();
        Evaluation evaluation = evaluationRepository.findOne(id);
        evaluation.setStatus(EvaluationStatus.IN_PROGRESS);

        evaluationRepository.save(evaluation);

        for (EvaluatorRequest evaluatorRequest : request.getEvaluators()) {
            EvaluatorRole role = evaluatorRoleRepository.findByName(evaluatorRequest.getRole());
            Evaluator evaluator = new Evaluator();
            evaluator.setStatus(EvaluatorStatus.STARTED);
            evaluator.setEvaluation(evaluation);
            evaluator.setEvaluatorRole(role);
            evaluator.setUniqueLink(UniqueLinkGenerator.generateLinkKey(
                String.valueOf(evaluation.getId()),
                String.valueOf(evaluation.getStage().getId()),
                evaluatorRequest.getEmail()));
            Users user = userRepository.findByLogin(evaluatorRequest.getEmail());
            
            if (user != null) {
                evaluator.setUser(user);
                evaluator.setName(evaluatorRequest.getName());
            } else {
                evaluator.setName(evaluatorRequest.getEmail());
            } 
            evaluatorRepository.save(evaluator);

            if (evaluation.getUser() != evaluator.getUser()) {
                mailUtil.sendInvitationToEvaluation(evaluation, evaluator, evaluatorRequest.getEmail(), request.getLanguage());
            }

            evaluators.add(evaluator);
        }

        return evaluators;

    }

    public ResponseCsv generateCSV(Evaluation evaluation) {
        HashMap<String, ArrayList<Answer>> result = getEvaluationResults(evaluation);
        boolean isAutoEvaluationCompleted = isAutoEvaluation(evaluation);
        String headers = getEvaluationResultsHeaders(evaluation.getEvaluators().size(), isAutoEvaluationCompleted);
        String csvData = csvGenerator.GenerateCSV(result);
        LocalDateTime begin = LocalDateTime.ofInstant(evaluation.getStage().getStartDate().toInstant(),
                ZoneId.systemDefault());
        LocalDateTime end = LocalDateTime.ofInstant(evaluation.getStage().getEndDate().toInstant(),
                ZoneId.systemDefault());
        String beginDate = begin.format(DateTimeFormatter.ofPattern("yyyyMMdd", Locale.ENGLISH));
        String endDate = end.format(DateTimeFormatter.ofPattern("yyyyMMdd", Locale.ENGLISH));

        String fileName = evaluation.getUser().getName() + "_" + evaluation.getStage().getNameEs() + "_" + beginDate
                + "_" + endDate
                + ".csv";
        ResponseCsv response = new ResponseCsv();
        response.setData(csvData);
        response.setHeader(headers);
        response.setFilename(fileName);
        return response;
    }

    public HashMap<String, ArrayList<Answer>> getEvaluationResults(Evaluation evaluation) {
        Iterable<Evaluator> evaluators = evaluation.getEvaluators();
        HashMap<String, ArrayList<Answer>> result = new HashMap<String, ArrayList<Answer>>();
        for (Evaluator evaluator : evaluators) {
            Evaluation evaluatorEvaluation = evaluator.getEvaluation();
            Iterable<Answer> answers = answerRepository.findAllByEvaluation(evaluatorEvaluation);
            for (Answer answer : answers) {
                if (answer.getEvaluator().getId() == evaluator.getId()) {
                    String key = answer.getQuestion().getDescriptionEs();
                    if (result.containsKey(key)) {
                        result.get(key).add(answer);
                    } else {
                        ArrayList<Answer> answerReponses = new ArrayList<Answer>();
                        answerReponses.add(answer);
                        result.put(key, answerReponses);
                    }
                }
            }
        }
        return result;
    }

    public Boolean isAutoEvaluation(Evaluation evaluation) {
        Iterable<Evaluator> evaluators = evaluation.getEvaluators();
        boolean hasAutoEvaluation = false;
        for (Evaluator evaluator : evaluators) {
            if (evaluator.getEvaluatorRole().getId() == 5 && evaluator.getStatus().name() == "COMPLETED") {
                return hasAutoEvaluation = true;
            }
        }
        return hasAutoEvaluation;
    }

    public String getEvaluationResultsHeaders(int evaluatorsSize, boolean hasAutoEvaluation) {
        String headerLine = "";
        ArrayList<String> headers = new ArrayList<String>();
        headers.add("Pregunta");
        if (hasAutoEvaluation) {
            headers.add("Autoevaluación");
        }
        for (int i = 1; i < evaluatorsSize; i++) {
            headers.add("evaluador" + i);
        }
        headerLine = String.join(",", headers);
        return headerLine;
    }
}
