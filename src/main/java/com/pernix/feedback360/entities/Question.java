package com.pernix.feedback360.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String descriptionEs;

    @NotEmpty
    @Column(nullable = false)
    private String descriptionEn;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FK_questionTypeId", nullable = false)
    private QuestionType type;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "question")
    private Set<Answer> answers;
    @ManyToOne
    @JoinColumn(name = "FK_subtopicId", nullable = false)
    private Subtopic subtopic;

    public Question() {
    }

    public Question(Question question) {
        this.id = question.getId();
        this.descriptionEs = question.getDescriptionEs();
        this.descriptionEn = question.getDescriptionEn();
        this.type = question.getType();
        this.answers = question.getAnswers();
        this.subtopic = question.getSubtopic();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescriptionEs() {
        return descriptionEs;
    }

    public void setDescriptionEs(String description) {
        this.descriptionEs = description;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEN) {
        this.descriptionEn = descriptionEN;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    public Subtopic getSubtopic() {
        return subtopic;
    }

    public void setSubtopic(Subtopic subtopic) {
        this.subtopic = subtopic;
    }

}
