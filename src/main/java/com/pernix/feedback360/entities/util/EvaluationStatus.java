package com.pernix.feedback360.entities.util;

public enum EvaluationStatus {
    STARTED, INVITATIONS_SENT, IN_PROGRESS, COMPLETED
}
