package com.pernix.feedback360.services;

import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.feedback360.data.EvaluationRepository;
import com.pernix.feedback360.data.StageRepository;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Stage;
import com.pernix.feedback360.entities.util.EvaluationStatus;

@RestController
public class StageServices {

    private final StageRepository stageRepository;
    private final EvaluationRepository evaluationRepository;

    @Autowired
    public StageServices(StageRepository stageRepository, EvaluationRepository evaluationRepository) {
        this.stageRepository = stageRepository;
        this.evaluationRepository = evaluationRepository;
    }

    @RequestMapping(value = "/stages", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Stage>> getAll() {
        Iterable<Stage> stages = stageRepository.findAll();
        if (stages == null) {
            return new ResponseEntity<Iterable<Stage>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Stage>>(stages, HttpStatus.OK);
    }

    @RequestMapping(value = "/stages", method = RequestMethod.POST)
    public ResponseEntity<Stage> create(@RequestBody Stage stage) {
        if (stageRepository.exists(stage.getId())) {
            return new ResponseEntity<Stage>(HttpStatus.CONFLICT);
        }
        stageRepository.save(stage);
        Stage response = stageRepository.findOne(stage.getId());
        return new ResponseEntity<Stage>(response, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/stages/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Stage> update(@PathVariable("id") int id, @RequestBody Stage stage) {
        Stage current = stageRepository.findOne(id);
        
        if (current == null) {
            return new ResponseEntity<Stage>(HttpStatus.NOT_FOUND);
        }
        
        Date now = new Date();
        long today = now.getTime();
        long endDate = stage.getEndDate().getTime();
        
        stage.setId(current.getId());
        Set<Evaluation> evaluations = evaluationRepository.findAllByStage(stage);
        for(Evaluation evaluation : evaluations) {
        	if(today > endDate) {
        		evaluation.setStatus(EvaluationStatus.COMPLETED);
        	}
        	else if(today <= endDate) {
        		evaluation.setStatus(EvaluationStatus.IN_PROGRESS);
        	}      	
        	evaluationRepository.save(evaluation);
        }
        stage = stageRepository.save(stage);
        return new ResponseEntity<Stage>(current, HttpStatus.OK);
    }

}
