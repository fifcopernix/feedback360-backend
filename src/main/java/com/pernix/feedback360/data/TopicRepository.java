package com.pernix.feedback360.data;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.pernix.feedback360.entities.Survey;
import com.pernix.feedback360.entities.Topic;

public interface TopicRepository extends CrudRepository<Topic, Integer> {
    Set<Topic> findAllBySurvey(Survey survey);
}
