package com.pernix.feedback360.entities.util;

public enum EvaluatorStatus {
    STARTED, COMPLETED
}
