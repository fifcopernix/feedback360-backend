package com.pernix.feedback360.services.pojos;

import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Evaluator;

public class EvaluationSubmit {

    private Evaluator evaluator;
    private Iterable<Answer> answers;

    public Evaluator getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(Evaluator evaluator) {
        this.evaluator = evaluator;
    }

    public Iterable<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Iterable<Answer> answers) {
        this.answers = answers;
    }
}
