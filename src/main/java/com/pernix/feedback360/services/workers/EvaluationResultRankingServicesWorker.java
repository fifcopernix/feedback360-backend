package com.pernix.feedback360.services.workers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pernix.feedback360.data.AnswerRepository;
import com.pernix.feedback360.data.EvaluatorRepository;
import com.pernix.feedback360.data.EvaluatorRoleRepository;
import com.pernix.feedback360.data.QuestionRepository;
import com.pernix.feedback360.data.QuestionTypeRepository;
import com.pernix.feedback360.data.SubtopicRepository;
import com.pernix.feedback360.data.TopicRepository;
import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.EvaluatorRole;
import com.pernix.feedback360.entities.Question;
import com.pernix.feedback360.entities.QuestionType;
import com.pernix.feedback360.entities.Subtopic;
import com.pernix.feedback360.entities.Topic;
import com.pernix.feedback360.services.pojos.results.EvaluationResult;
import com.pernix.feedback360.services.pojos.results.QuestionResult;

@Service
public class EvaluationResultRankingServicesWorker {

    private final AnswerRepository answerRepository;
    private final TopicRepository topicRepository;
    private final SubtopicRepository subtopicRepository;
    private final QuestionRepository questionRepository;
    private final QuestionTypeRepository questionTypeRepository;

    @Autowired
    public EvaluationResultRankingServicesWorker(EvaluatorRepository evaluatorRepository,
            EvaluatorRoleRepository evaluatorRoleRepository, AnswerRepository answerRepository,
            TopicRepository topicRepository, SubtopicRepository subtopicRepository,
            QuestionRepository questionRepository, QuestionTypeRepository questionTypeRepository) {
        this.answerRepository = answerRepository;
        this.topicRepository = topicRepository;
        this.subtopicRepository = subtopicRepository;
        this.questionRepository = questionRepository;
        this.questionTypeRepository = questionTypeRepository;
    }

    public void createRankingResults(EvaluationResult results, Evaluation evaluation,
            ArrayList<EvaluatorRole> evaluatorRoles, String language) {

        ArrayList<QuestionResult> questionResults = new ArrayList<QuestionResult>();
        Set<Topic> topics = topicRepository.findAllBySurvey(evaluation.getSurvey());

        for (Topic topic : topics) {
            createTopicResults(evaluation, topic, questionResults, evaluatorRoles,language);
        }

        results.setWorstQuestions(createWorstQuestionsResults(results, questionResults));
        results.setBestQuestions(createBestQuestionsResults(results, questionResults));
    }

    private ArrayList<QuestionResult> createWorstQuestionsResults(EvaluationResult results,
            ArrayList<QuestionResult> questionResults) {
        questionResults.sort(new Comparator<QuestionResult>() {
            @Override
            public int compare(QuestionResult question1, QuestionResult question2) {
                if (question1.getGrade() < question2.getGrade()) {
                    return -1;
                } else if (question1.getGrade() == question2.getGrade()) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });

        int rankingSize = (questionResults.size() >= 10 ? 10 : questionResults.size());

        List<QuestionResult> worstQuestions = questionResults.subList(0, rankingSize);

        return new ArrayList<QuestionResult>(worstQuestions);
    }

    private ArrayList<QuestionResult> createBestQuestionsResults(EvaluationResult results,
            ArrayList<QuestionResult> questionResults) {
        questionResults.sort(new Comparator<QuestionResult>() {
            @Override
            public int compare(QuestionResult question1, QuestionResult question2) {
                if (question1.getGrade() > question2.getGrade()) {
                    return -1;
                } else if (question1.getGrade() == question2.getGrade()) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });

        int rankingSize = (questionResults.size() >= 10 ? 10 : questionResults.size());

        List<QuestionResult> bestQuestions = questionResults.subList(0, rankingSize);

        return new ArrayList<QuestionResult>(bestQuestions);
    }

    private void createTopicResults(Evaluation evaluation, Topic topic, ArrayList<QuestionResult> questionResults,
            ArrayList<EvaluatorRole> evaluatorRoles, String language) {

        Set<Subtopic> subtopics = subtopicRepository.findAllByTopic(topic);

        for (Subtopic subtopic : subtopics) {

            createSubtopicResults(evaluation, subtopic, questionResults, evaluatorRoles, language);

        }
    }

    private void createSubtopicResults(Evaluation evaluation, Subtopic subtopic, ArrayList<QuestionResult> questionResults,
        ArrayList<EvaluatorRole> evaluatorRoles, String language) {

        QuestionType closedQuestionType = questionTypeRepository.findByName("Closed");

        Set<Question> questions = questionRepository.findAllBySubtopic(subtopic);

        for (Question question : questions) {
            if (question.getType().getId() == closedQuestionType.getId()) {
                GradeAverage average = new GradeAverage();

                Set<Answer> answers = getAnswersFor(question, evaluatorRoles);

                for (Answer answer : answers) {
                    if (answer.getEvaluation().getId() == evaluation.getId()) {
                        Double response = Double.parseDouble(answer.getResponse());
                        Double responseValue;
                        try {
                            responseValue = response;

                        } catch (NumberFormatException e) {
                            responseValue = 0.0;
                        }

                        average.addAnswer(responseValue);
                    }
                }               
                QuestionResult questionResult = new QuestionResult();

                if (language.equals("en")) {
                	questionResult.setStatement(question.getDescriptionEn());
                }
                else {
                	questionResult.setStatement(question.getDescriptionEs());
                }
                
                questionResult.setGrade(average.getAnswersAverage());

                questionResults.add(questionResult);
            }
        }
    }

    private Set<Answer> getAnswersFor(Question question, ArrayList<EvaluatorRole> evaluatorRoles) {

        Set<Answer> answers = answerRepository.findAllByQuestionAndEvaluatorRoleIn(question, evaluatorRoles);
        return answers;
    }

    private class GradeAverage {
        private int answersCounter;
        private double answersSum;

        public GradeAverage() {
            answersCounter = 0;
            answersSum = 0;
        }

        public void addAnswer(double toAnswersSum) {
            answersCounter++;
            answersSum += toAnswersSum;
        }

        public double getAnswersAverage() {
            return answersCounter == 0 ? 0 : answersSum / answersCounter;
        }
    }

}
