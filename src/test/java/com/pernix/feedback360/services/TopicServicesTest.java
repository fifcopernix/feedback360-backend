package com.pernix.feedback360.services;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.google.gson.Gson;
import com.pernix.feedback360.Application;
import com.pernix.feedback360.entities.Topic;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class TopicServicesTest extends BaseTest {
    private static final String PATH = "/topics";
    private static final String PATH2 = "/topics-by-survey";

    private Topic topic;

    @Before
    public void prepareTestMethod() throws Exception {
        topicRepository.deleteAll();
        topic = createTopic();
    }

    @Test
    public void getAll() throws Exception {
        createTopic();
        createTopic();

        ResultActions result = perform(get(PATH)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(3))).andExpect(jsonPath("$[0].nameEs", is(topic.getNameEs())));
    }

    @Test
    public void getById() throws Exception {
        String path = String.format("%s/%d", PATH, topic.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(topic.getId()))).andExpect(jsonPath("nameEs", is(topic.getNameEs())));
    }

    @Test
    public void getBySurvey() throws Exception {
        createTopic();

        String path = String.format("%s?surveyId=%d", PATH2, survey.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].nameEs", is(topic.getNameEs())));
    }

    @Test
    public void create() throws Exception {
        Topic requestOne = new Topic();
        requestOne.setNameEs("Nombre del tema");
        requestOne.setNameEn("Topic Name");
        requestOne.setSurvey(survey);

        perform(post(PATH).content(new Gson().toJson(requestOne))).andExpect(status().isCreated());
    }

    @Test
    public void update() throws Exception {
        final String updatedNameEN = "Updated name";
        final String updatedNameES = "Nombre actualizado";
        Topic request = topic;
        topic.setSubtopics(null);
        request.setNameEn(updatedNameEN);
        request.setNameEs(updatedNameES);

        perform(put(PATH).content(new Gson().toJson(request))).andExpect(status().isOk());
    }

    @Test
    public void destroy() throws Exception {
        String path = String.format("%s/%d", PATH, topic.getId());

        perform(delete(path)).andExpect(status().isMethodNotAllowed());
    }
}
