package com.pernix.feedback360.services.pojos.results;

public class SubtopicComparatorResult {

    private String name;

    private double selfAssessmentGrade;

    private double evaluatorsAverageGrade;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSelfAssessmentGrade() {
        return selfAssessmentGrade;
    }

    public void setSelfAssessmentGrade(double selfAssessmentGrade) {
        this.selfAssessmentGrade = selfAssessmentGrade;
    }

    public double getEvaluatorsAverageGrade() {
        return evaluatorsAverageGrade;
    }

    public void setEvaluatorsAverageGrade(double evaluatorsAverageGrade) {
        this.evaluatorsAverageGrade = evaluatorsAverageGrade;
    }

}
