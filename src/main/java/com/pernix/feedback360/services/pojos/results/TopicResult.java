package com.pernix.feedback360.services.pojos.results;

import java.util.ArrayList;

public class TopicResult {

    private String name;

    private double grade;

    private ArrayList<SubtopicResult> subtopics;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public ArrayList<SubtopicResult> getSubtopics() {
        return subtopics;
    }

    public void setSubtopics(ArrayList<SubtopicResult> subtopics) {
        this.subtopics = subtopics;
    }

}
