package com.pernix.feedback360.data;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.EvaluatorRole;
import com.pernix.feedback360.entities.Users;
import com.pernix.feedback360.entities.util.EvaluatorStatus;

public interface EvaluatorRepository extends CrudRepository<Evaluator, Integer> {
    Evaluator findByUniqueLink(String uniqueLink);

    Set<Evaluator> findAllByStatusAndEvaluation(EvaluatorStatus status, Evaluation evaluation);

    Set<Evaluator> findAllByStatusAndEvaluationAndEvaluatorRole(EvaluatorStatus status, Evaluation evaluation,
            EvaluatorRole evaluatorRole);

    Set<Evaluator> findAllByEvaluation(Evaluation evaluation);

    Set<Evaluator> findAllByUser(Users user);

    Evaluator findByUser(Users user);
    
    @Query("from Evaluator where id=:evaluatorId")
    Evaluator findById(@Param("evaluatorId") int evaluatorId);
}
