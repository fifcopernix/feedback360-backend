package com.pernix.feedback360.data;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.pernix.feedback360.entities.Subtopic;
import com.pernix.feedback360.entities.Topic;

public interface SubtopicRepository extends CrudRepository<Subtopic, Integer> {
    Set<Subtopic> findAllByTopic(Topic topic);
}
