package com.pernix.feedback360.services;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.feedback360.data.EvaluationRepository;
import com.pernix.feedback360.data.UserRepository;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Users;
import com.pernix.feedback360.services.pojos.ResponseCsv;
import com.pernix.feedback360.services.pojos.results.EvaluationResult;
import com.pernix.feedback360.services.workers.EvaluationResultServicesWorker;

@RestController
public class EvaluationResultServices {

    private final EvaluationRepository evaluationRepository;
    private final UserRepository userRepository;
    private final EvaluationResultServicesWorker evaluationResultServicesWorker;

    @Autowired
    public EvaluationResultServices(EvaluationRepository evaluationRepository,
            EvaluationResultServicesWorker evaluationResultServicesWorker, UserRepository userRepository) {
        this.evaluationRepository = evaluationRepository;
        this.evaluationResultServicesWorker = evaluationResultServicesWorker;
        this.userRepository=userRepository;
    }

    @RequestMapping(value = "evaluation-results", method = RequestMethod.GET)
    public ResponseEntity<EvaluationResult> get(@RequestParam(value = "id") int id, @RequestParam("language") String language) {
        Evaluation evaluation = evaluationRepository.findOne(id);
        EvaluationResult results = evaluationResultServicesWorker.createResults(evaluation, language);

        return new ResponseEntity<EvaluationResult>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "user-evaluations-by-date", method = RequestMethod.GET)
    public ResponseEntity <Iterable <EvaluationResult>> getUserEvaluationsByDate(@RequestParam("userId") int userId, @RequestParam("initialDate") Date initialDate, @RequestParam("finalDate") Date finalDate,  HttpServletResponse response) throws IOException {
        Users user = userRepository.findById(userId);
        if (user == null) {
            return new ResponseEntity<Iterable<EvaluationResult>>(HttpStatus.NOT_FOUND);
        }
        ResponseCsv  dataResult = evaluationResultServicesWorker.GetUserEvaluationsByDate(user, initialDate, finalDate);
        response.setContentType("text-csv");
        response.setHeader("Content-Disposition", "attachment; filename=" + dataResult.getFilename());
        OutputStream writer = response.getOutputStream();
        writer.write(dataResult.getHeader().getBytes());
        writer.write(dataResult.getData().getBytes());
        writer.close();
        return new ResponseEntity<Iterable<EvaluationResult>>(HttpStatus.OK);
    }
}
