package com.pernix.feedback360.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.feedback360.data.SubtopicRepository;
import com.pernix.feedback360.data.TopicRepository;
import com.pernix.feedback360.entities.Subtopic;
import com.pernix.feedback360.entities.Topic;

@RestController
public class SubtopicServices {

    private final SubtopicRepository subtopicRepository;
    private final TopicRepository topicRepository;

    @Autowired
    public SubtopicServices(SubtopicRepository topicSubCategoryRepository, TopicRepository topicRepository) {
        this.subtopicRepository = topicSubCategoryRepository;
        this.topicRepository = topicRepository;
    }

    @RequestMapping(value = "/subtopics", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Subtopic>> getAll() {
        Iterable<Subtopic> subtopics = subtopicRepository.findAll();
        if (subtopics == null) {
            return new ResponseEntity<Iterable<Subtopic>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Subtopic>>(subtopics, HttpStatus.OK);
    }

    @RequestMapping(value = "/subtopics-by-topic", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Subtopic>> getAllByTopic(@RequestParam("topicId") int id) {
        Topic topic = topicRepository.findOne(id);
        if (topic == null) {
            return new ResponseEntity<Iterable<Subtopic>>(HttpStatus.NOT_FOUND);
        }
        Iterable<Subtopic> subtopics = subtopicRepository.findAll();
        if (subtopics == null) {
            return new ResponseEntity<Iterable<Subtopic>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Subtopic>>(subtopics, HttpStatus.OK);
    }

    @RequestMapping(value = "/subtopics/{id}", method = RequestMethod.GET)
    public ResponseEntity<Subtopic> get(@PathVariable("id") int id) {
        Subtopic subtopic = subtopicRepository.findOne(id);
        if (subtopic == null) {
            return new ResponseEntity<Subtopic>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Subtopic>(subtopic, HttpStatus.OK);
    }

    @RequestMapping(value = "/subtopics", method = RequestMethod.POST)
    public ResponseEntity<Subtopic> create(@RequestBody Subtopic subtopic) {

        if (subtopicRepository.exists(subtopic.getId())) {
            return new ResponseEntity<Subtopic>(HttpStatus.CONFLICT);
        }
        subtopicRepository.save(subtopic);
        subtopic.setTopic(topicRepository.findOne(subtopic.getTopic().getId()));
        return new ResponseEntity<Subtopic>(subtopic, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/subtopics", method = RequestMethod.PUT)
    public ResponseEntity<Subtopic> update(@RequestBody Subtopic subtopic) {

        Subtopic current = subtopicRepository.findOne(subtopic.getId());
        if (current == null) {
            return new ResponseEntity<Subtopic>(HttpStatus.NOT_FOUND);
        }
        subtopic.setId(current.getId());
        subtopicRepository.save(subtopic);
        subtopic.setTopic(topicRepository.findOne(subtopic.getTopic().getId()));
        return new ResponseEntity<Subtopic>(subtopic, HttpStatus.OK);
    }
}
