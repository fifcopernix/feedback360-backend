
-- Roles
insert into role(id, name) values (1,'ADMIN');
insert into role(id, name) values (2,'USER');
insert into role(id, name) values (3,'GUEST');

-- Usuarios
insert into users(id, name, login, password, fk_role_id) values (1,'Administrador','admin','adminsecret123', 1);
insert into users(id, name, login, password, fk_role_id) values (2,'Pedro Martinez','pedro','pedro', 2);
insert into users(id, name, login, password, fk_role_id) values (3,'Alejandro Hernandez','alejandro386hs@gmail.com','123', 2);

-- Tipos de pregunta
insert into question_type(id, name) values (1, 'Open');
insert into question_type(id, name) values (2, 'Closed');

-- Tipos de evaluadores
insert into public.evaluator_role(id, name) values (1, 'Jefe');
insert into public.evaluator_role(id, name) values (2, 'Par');
insert into public.evaluator_role(id, name) values (3, 'Colaborador');
insert into public.evaluator_role(id, name) values (4, 'Otro');
insert into public.evaluator_role(id, name) values (5, 'Auto');

insert into allowed_domain(id, domain) values (1, '@fifco.com');
insert into allowed_domain(id, domain) values (2, '@nabreweries.com');
insert into allowed_domain(id, domain) values (3, '@reservaconchal.com');
insert into allowed_domain(id, domain) values (4, '@pernixlabs.com');
insert into allowed_domain(id, domain) values (5, '@pernix-solutions.com');
insert into allowed_domain(id, domain) values (6, '@pernix.cr');
insert into allowed_domain(id, domain) values (7, '@gmail.com');

-- Surveys
insert into public.survey(id, name_en, name_es) values (1, '360 Feedback Survey','Cuestionario Retroalimentación 360');

-- Categorias
insert into topic(id,name_en,name_es,fk_survey_id) values (1,'Liberate potencial','Liberar el potencial',1);
insert into topic(id,name_en,name_es,fk_survey_id) values (2,'Meaningful relationships','Relaciones significativas',1);
insert into topic(id,name_en,name_es,fk_survey_id) values (3,'Explore and learn','Explora y aprende',1);
insert into topic(id,name_en,name_es,fk_survey_id) values (4,'Honor commitments','Honra compromisos',1);
insert into topic(id,name_en,name_es,fk_survey_id) values (5,'Improvement Opportunities','Oportunidades de mejora',1);

-- Sub Categorias
insert into subtopic(id,name_en,name_es,fk_topic_id) values (1,'Vision','Visión',1);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (2,'Identify talent','Identificar talento',1);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (3,'Develop talent','Desarrollar talento',1);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (4,'Flexible coach/mentor','Flexible coach/mentor',1);

insert into subtopic(id,name_en,name_es,fk_topic_id) values (5,'Connection','Conexión',2);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (6,'Co-create','Co-crear',2);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (7,'Collaboration','Colaboración',2);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (8,'Inspire','Inspirar',2);

insert into subtopic(id,name_en,name_es,fk_topic_id) values (9,'Experimentation','Experimentación',3);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (10,'Focus on client','Foco en el cliente',3);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (11,'Thinks simple','Piensa simple',3);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (12,'Challenge statuo quo','Desafía Status Quo',3);

insert into subtopic(id,name_en,name_es,fk_topic_id) values (13,'Project management','Administrar proyectos',4);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (14,'Manage changes','Administrar cambios',4);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (15,'Delegate','Delegar',4);
insert into subtopic(id,name_en,name_es,fk_topic_id) values (16,'Decision making','Tomar decisiones',4);

insert into subtopic(id,name_en,name_es,fk_topic_id) values (17,'Comments','Comentarios',5);

-- Preguntas
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Effectively communicates a vision for the future and approach includes improving the environment and society?','¿Desarrolla una visión mas allá de su área de acción  y su enfoque incluye la mejora del ambiente y la sociedad?',1,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Develops appropriate objectives and strategies to advance the organization`s vision and purpose?','¿Su visión estratégica le permite crear soluciones considerando los retos del futuro?',1,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Influences others to translate organization`s vision and purpose into action?','¿Comunica su visión y convence a otros de seguirlo?',1,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Identifies strengths in individuals and assigns tasks/projects accordingly?','¿Identifica y asigna proyectos o tareas según  las mayores fortalezas de cada persona?' ,2,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Recruits and selects qualified and effective team members?','¿Identifica con claridad el principal talento de cada persona a la hora de formar equipos?',2,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Recognizes the value of people with different talents and skills (appreciates diversity)?','¿Es capaz de conseguir la combinación perfecta de talentos para maximizar los resultados de un equipo?',2,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Encourages and supports staff with their personal development?','¿Facilita el desarrollo de las personas hasta su máximo potencial?',3,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Provides feedback and coaching that is specific and timely?','¿Organiza los talentos individuales para maximizar el valor del equipo?',3,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Conducts regular conversations with staff regarding personal development and career aspirations?','¿Sabe dar retroalimentación y escoger las acciones que desarrollan el potencial de las personas?',3,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Adapts leadership style to individual needs?','¿Utiliza muy bien sus habilidades de coaching (hacer preguntas que le ayuden a encontrar el sentido de las cosas) cuando se requiere?',4,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'When appropriate, uses questions and coaching tools to guide staff to own solutions?','¿Utiliza muy bien sus habilidades de consejero experto cuando se requiere?',4,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Actively requests feedback to improve personal leadership skills?','¿Se mueve de rol entre líder, coach (asesor que ayuda a encontrar sentido de las cosas) y consejero experto con flexibilidad según se requiera?',4,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Creates meaningful relationships based on trust and transparency?','¿Hace conexión profunda con los demás basado en la confianza?',5,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Rewards and recognizes people for good performance?','¿Su profunda conexión con los demás le permite ayudar a las personas a evolucionar, a crecer?',5,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Treats others fairly and ethically?','¿Su conexión profunda y su generosidad hacen que ayude a crecer a las personas integralmente, impactando su vida más allá del trabajo?',5,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Creates an environment of trust encourages collaboration among the team members?','¿Crea condiciones de confianza para que el pensamiento distinto de los miembros enriquezca los resultados del equipo?',6,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Shares important information and data with team members?','¿Cuando lidera equipos consigue mucha profundidad en el análisis y solución de los problemas por parte de todos los miembros del equipo?',6,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Empowers teams to propose solutions that are impactful and easy to implement?','¿Sus equipos proponen soluciones poderosas y fáciles de llevar a cabo?',6,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Builds appropriate alliances across organizational lines for the benefit of the company?','¿Consigue alianzas con terceros que son beneficiosas para la empresa?',7,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Fosters an environment that encourages collaboration and shared value?','¿Los resultados que logra con las alianzas externas son mucho mayores a los que lograría la organización por sí misma?',7,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Engages in difficult conversations with staff and colleagues to effectively address and resolve conflict?','¿Convence a nuestros aliados externos de los beneficios en el largo plazo?' ,7,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Leads by example in accordance to FIFCO USA values and purpose?','¿Es un ejemplo del modelo de liderazgo, de los valores y del propósito de la compañía?',8,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Creates an atmosphere that inspires others to achieve at a higher level?','¿Es capaz de inspirar cuando explica el valor que tienen los nuevos proyectos en el largo plazo?',8,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Learns and develops from own experiences and encourages others to do the same?','¿Promueve que la gente a su alrededor valore en cada experiencia los aprendizajes como ser humano/',8,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Is comfortable taking controllable risks?','¿En la experimentación toma riesgos grandes pero controlados?',9,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Encourages others to take risks in an effort to improve the business?','¿Tiene método para experimentar y lo pone en práctica?',9,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Willing to be the only champion for an idea or position?','¿Promueve la experimentación y que los aprendizajes se incorporen en los diseños?',9,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Asks questions & listens carefully to internal/external customers to determine their goals, needs and expectations?','¿Es un investigador permanente de sus cliente internos o externos, conoce sus gustos y necesidades profundamente?',10,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Proposals/ideas are highly valued and sought after amongst staff and customers?','¿Sus propuestas tienen un alto valor desde el punto de vista de sus clientes internos o externos?',10,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Willingly shares own expertise with others to drive their success?','¿Difunde en la organización sus aprendizajes sobre sus clientes (internos o externos) o consumidores para  enriquecer las propuestas de otros?',10,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Proposes logical solutions that are efficient and cost effective?','¿Promueve soluciones simples y de bajo costo?',11,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'All proposals/solutions consider all Triple Bottom Line aspects (economic, environmental, and social)?','¿Sus soluciones o propuestas consideran la triple utilidad, es decir, toman en cuenta los aspectos: económico, ambiental y social (colaboradores y comunidad)?',11,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Empowers and guides people to communicate their solutions in a simple and clear manner?','¿Promueve que sus grupos comuniquen muy sencillo y claro las soluciones a los problemas?',11,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Strives for continuous improvement and challenges the status quo?','¿Su liderazgo desafía constantemente la forma de hacer las cosas en la organización, no sólo en su área',12,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Encourage others to suggest new, innovative ideas and solutions?','¿Las ideas que lidera son muy novedosas?',12,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Provides staff with challenging assignments and opportunities for development?','¿Convence a otros con sus aprendizajes de mejorar permanentemente su entorno?',12,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Effectively manages priorities and completes tasks timely?','¿Tiene un manejo adecuado del tiempo?',13,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Pursues aggressive goals and works hard to achieve them?','¿Administra muy bien los recursos que tiene en cada proyecto o tarea?',13,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Clarifies expectations and holds people and teams accountable for getting results?','¿Organiza muy bien a las personas dentro de cada proyecto o tarea?',13,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Takes responsibility for implementing change in own functional area?','¿Cada uno de sus proyectos a cargo es ejecutado con una excelente administración del cambio (Dirige adecuadamente la implementación de los cambios necesarios en la empresa, considerando los impactos en las personas y facilitando el proceso)?',14,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Remains positive and productive when dealing with change?','¿Tiene un método claro para administrar el cambio y lo usa de forma consistente?',14,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Supports changes throughout organization and leads staff to understand and promote?','¿Promueve que los proyectos de otras áreas tengan un excelente manejo del cambio?',14,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Appropriately delegates tasks, authority and responsibility?','¿Capacita a las personas o grupos para que puedan llevar a cabo las tareas encomendadas?',15,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Delegates tasks for the purpose of development?','¿Lleva a cabo una efectiva delegación de tareas a las personas o equipos?',15,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Provides feedback about results from delegated tasks to people or teams?','¿Retroalimenta sobre los resultados de las tareas delegadas a las personas o equipos?',15,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Makes effective and timely decisions based on good data, good judgment and experience?','¿Analiza con profundidad y rapidez las variables para tomar una decisión?',16,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Demonstrates creativity and risk taking in decisions?','¿En sus decisiones toma riesgos controlados?',16,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'Understands the short term and long term implications of decisions?','¿Promueve que las decisiones tomen en cuenta el largo plazo?',16,2);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'What do you feel are this person’s greatest strengths/abilities in their current role?','¿Cuáles considera usted que son las principales capacidades y fortalezas que tiene esta persona?',17,1);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'What do you feel are this person’s greatest opportunities for improvement in their current role?','¿Cuáles considera usted que son las principales oportunidades de mejora que tiene esta persona?',17,1);
insert into question(id,description_en,description_es,fk_subtopic_id,fk_question_type_id) values(nextval('hibernate_sequence'),'What would you suggest to this person that would have the greatest positive impact on their ability to be a leader at FIFCO USA?','¿Qué sería lo más importante que podría sugerir a esta persona para mejorar su efectividad como líder?',17,1);
