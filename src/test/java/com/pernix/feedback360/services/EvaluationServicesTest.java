package com.pernix.feedback360.services;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pernix.feedback360.Application;
import com.pernix.feedback360.data.EvaluationRepository;
import com.pernix.feedback360.data.EvaluatorRepository;
import com.pernix.feedback360.data.EvaluatorRoleRepository;
import com.pernix.feedback360.data.StageRepository;
import com.pernix.feedback360.data.SurveyRepository;
import com.pernix.feedback360.data.UserRepository;
import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.EvaluatorRole;
import com.pernix.feedback360.entities.Stage;
import com.pernix.feedback360.entities.Users;
import com.pernix.feedback360.entities.util.EvaluationStatus;
import com.pernix.feedback360.entities.util.EvaluatorStatus;
import com.pernix.feedback360.services.pojos.EvaluationRequest;
import com.pernix.feedback360.services.pojos.EvaluationSubmit;
import com.pernix.feedback360.services.pojos.InvitationRequest;
import com.pernix.feedback360.services.workers.EvaluationServicesWorker;
import com.pernix.feedback360.utils.UniqueLinkGenerator;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class EvaluationServicesTest extends BaseTest {

    private static final String EV_BY_USER_PATH = "/evaluations-by-user";
    private static final String PATH = "/evaluations";
    private static final String EV_CHECK_KEY_PATH = "/evaluations-check-key";
    private static final String EV_BY_EVALUATOR_PATH = "/evaluations-by-evaluator";
    private static final String EV_SUBMIT = "/evaluations-submit";
    private static final String GSV_PATH = "/generate-report";
    private Users user;
    private Evaluation evaluation;
    private Stage stage;
    private Evaluator evaluator;

    @Autowired
    private EvaluationRepository evaluationRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StageRepository stageRepository;

    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private EvaluatorRepository evaluatorRepository;

    @Autowired
    EvaluatorRoleRepository evaluatorRoleRepository;

    @Autowired
    private EvaluationServicesWorker evaluationWorker;

    @Before
    public void prepareTestMethod() throws Exception {
        evaluationRepository.deleteAll();
        user = userRepository.findByLogin("alejandro386hs@gmail.com");
        survey = surveyRepository.findOne(1);
        stage = createStage();
        evaluation = createEvaluation();
        evaluator = createEvaluator();
    }

    private Evaluation createEvaluation() {
        Evaluation evaluation = new Evaluation();

        evaluation.setStatus(EvaluationStatus.STARTED);
        evaluation.setSurvey(survey);
        evaluation.setStage(stage);
        evaluation.setUser(user);
        evaluationRepository.save(evaluation);
        return evaluationRepository.findAll().iterator().next();
    }

    private Stage createStage() {
        Stage stage = new Stage();
        Date dateB = new Date(1501815600000L);
        Date dateE = new Date(1812356444440L);
        stage.setStartDate(dateB);
        stage.setEndDate(dateE);
        stage.setNameEn("periodo1");
        stage.setNameEs("stage1");
        stage.setEvaluations(null);
        stageRepository.save(stage);

        return stage;
    }
    private Evaluator createEvaluator() {
        EvaluatorRole role = evaluatorRoleRepository.findByName("Jefe");
        Evaluation eval = evaluationRepository.findAll().iterator().next();
        Users user = userRepository.findByLogin("alejandro386hs@gmail.com");
        String email = user.getLogin();
        Evaluator evaluator = new Evaluator();
        evaluator.setStatus(EvaluatorStatus.STARTED);
        evaluator.setEvaluation(eval);
        evaluator.setEvaluatorRole(role);
        evaluator.setUniqueLink(
                UniqueLinkGenerator.generateLinkKey(String.valueOf(eval.getId()), eval.getStage().getNameEs(), email));
        evaluator.setName("alejandro");
        evaluator.setUser(user);
        evaluatorRepository.save(evaluator);
        return evaluatorRepository.findAll().iterator().next();
    }

    @Test
    public void getAllByUser() throws Exception {
        String path = String.format("%s?userId=%d", EV_BY_USER_PATH, user.getId());

        perform(get(path)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    public void getAll() throws Exception {
        perform(get(PATH)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void getByStage() throws Exception {
        String path = String.format("%s?stageId=%d", PATH, stage.getId());
        perform(get(path)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    public void getByEvaluator() throws Exception {
        String path = String.format("%s?evaluatorId=%d", EV_BY_EVALUATOR_PATH, evaluator.getUser().getId());
        perform(get(path)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void getCheckKey() throws Exception {

        String path = String.format("%s?key=%s", EV_CHECK_KEY_PATH, evaluator.getUniqueLink());
        perform(get(path)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Ignore
    @Test
    public void create() throws Exception {

        EvaluationRequest request = new EvaluationRequest();

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);

        JsonObject surveyObject = new JsonObject();
        surveyObject.addProperty("id", 1);

        JsonObject stageObject = new JsonObject();
        stageObject.addProperty("nameEs", stage.getNameEs());
        stageObject.addProperty("nameEn", stage.getNameEn());
        stageObject.addProperty("startDate", (new Date()).getTime());
        stageObject.addProperty("endDate", (new Date()).getTime());

        JsonArray emailObject = new JsonArray();
        emailObject.add("alhernandez@pernixlabs.com");

        jsonRequest.getAsJsonObject().add("survey", surveyObject);
        jsonRequest.getAsJsonObject().add("stage", stageObject);
        jsonRequest.getAsJsonObject().add("emails", emailObject);

        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isCreated())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Ignore
    @Test
    public void update() throws Exception {
        String path = String.format("%s/%d", PATH, evaluation.getId());

        Evaluation evaluationRequest = evaluation;
        evaluationRequest.setStatus(EvaluationStatus.IN_PROGRESS);
        evaluationRequest.setStage(null);
        evaluationRequest.setSurvey(null);
        evaluationRequest.setUser(null);
        evaluationRequest.setEvaluators(null);
        evaluationRequest.setAnswers(null);

        JsonElement jsonRequest = (new Gson()).toJsonTree(evaluationRequest);

        JsonObject jsonStage = new JsonObject();
        jsonStage.addProperty("id", 1);

        JsonObject jsonUser = new JsonObject();
        jsonUser.addProperty("id", 3);

        jsonRequest.getAsJsonObject().add("stage", jsonStage);
        jsonRequest.getAsJsonObject().add("user", jsonUser);

        perform(put(path).content(jsonRequest.toString())).andExpect(status().isOk())
        .andExpect(jsonPath("status", is(EvaluationStatus.IN_PROGRESS.name())));
    }

    @Ignore
    @Test
    public void inviteEvaluators() throws Exception {

        InvitationRequest request = new InvitationRequest();

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);

        evaluation = evaluationRepository.findAll().iterator().next();
        JsonObject evaluationObject = new JsonObject();
        evaluationObject.addProperty("id", evaluation.getId());

        JsonObject evaluatorObject = new JsonObject();
        evaluatorObject.addProperty("name", "alejandro 1");
        evaluatorObject.addProperty("role", "Jefe");
        evaluatorObject.addProperty("email", "alhernandez@pernixlabs.com");
        JsonArray emailObject = new JsonArray();
        emailObject.add(evaluatorObject);

        jsonRequest.getAsJsonObject().add("evaluation", evaluationObject);
        jsonRequest.getAsJsonObject().add("evaluators", emailObject);

        perform(post("/evaluations-invite").content(jsonRequest.toString())).andExpect(status().isCreated());
    }
    
    @Ignore
    @Test
    public void submitEvaluation() throws Exception {
        EvaluationSubmit evaluationSubmit = new EvaluationSubmit();
        JsonElement jsonRequest = (new Gson()).toJsonTree(evaluationSubmit);

        JsonObject evaluatorUserObject = new JsonObject();
        evaluatorUserObject.addProperty("id", evaluator.getUser().getId());

        JsonObject evaluatorRoleObject = new JsonObject();
        evaluatorRoleObject.addProperty("id", evaluator.getEvaluatorRole().getId());

        JsonObject evaluationObject = new JsonObject();
        evaluationObject.addProperty("id", evaluation.getId());

        JsonObject evaluatorObject = new JsonObject();
        evaluatorObject.addProperty("id", evaluator.getId());
        evaluatorObject.addProperty("name", evaluator.getName());
        evaluatorObject.add("user", evaluatorUserObject);
        evaluatorObject.add("evaluation", evaluationObject);
        evaluatorObject.add("evaluatorRole", evaluatorRoleObject);

        JsonObject questionOneObject = new JsonObject();
        questionOneObject.addProperty("id", questionOne.getId());

        JsonObject questionTwoObject = new JsonObject();
        questionTwoObject.addProperty("id", questionTwo.getId());

        JsonObject answerOneObject = new JsonObject();
        answerOneObject.addProperty("response", "1");
        answerOneObject.add("evaluation", evaluationObject.getAsJsonObject());
        answerOneObject.add("question", questionOneObject.getAsJsonObject());

        JsonObject answerTwoObject = new JsonObject();
        answerTwoObject.addProperty("response", "1");
        answerTwoObject.add("evaluation", evaluationObject.getAsJsonObject());
        answerTwoObject.add("question", questionTwoObject.getAsJsonObject());

        JsonArray answersObject = new JsonArray();
        answersObject.add(answerOneObject);
        answersObject.add(answerTwoObject);

        jsonRequest.getAsJsonObject().add("evaluator", evaluatorObject);
        jsonRequest.getAsJsonObject().add("answers", answersObject);

        perform(post(EV_SUBMIT).content(jsonRequest.toString())).andExpect(status().isOk());
    }
    
    @Ignore
    @Test
    public void generateCSV() throws Exception {
        EvaluationSubmit evaluationSubmit = new EvaluationSubmit();
        evaluationSubmit.setEvaluator(evaluator);
        Answer answer = new Answer();
        answer.setEvaluatorRole(evaluator.getEvaluatorRole());
        answer.setEvaluation(evaluation);
        answer.setQuestion(questionOne);
        answer.setResponse("test response");
        LinkedList<Answer> answers = new LinkedList<Answer>();
        answers.add(answer);
        Iterable<Answer> iterable = answers;

        evaluationSubmit.setAnswers(iterable);
        evaluationWorker.submitEvaluation(evaluationSubmit, evaluator);

        String path = String.format("%s/?id=%d", GSV_PATH, evaluation.getId());
        perform(get(path)).andExpect(status().isOk());
    }
}
