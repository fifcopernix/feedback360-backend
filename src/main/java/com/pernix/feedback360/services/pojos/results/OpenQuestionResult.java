package com.pernix.feedback360.services.pojos.results;

import java.util.ArrayList;

public class OpenQuestionResult {

    private String statement;
    private String selfAssesmentResponse;
    private ArrayList<String> leaderResponses;
    private ArrayList<String> peerResponses;
    private ArrayList<String> collaboratorResponses;

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getSelfAssesmentResponse() {
        return selfAssesmentResponse;
    }

    public void setSelfAssesmentResponse(String selfAssesmentResponse) {
        this.selfAssesmentResponse = selfAssesmentResponse;
    }

    public ArrayList<String> getLeaderResponses() {
        return leaderResponses;
    }

    public void setLeaderResponses(ArrayList<String> leaderResponses) {
        this.leaderResponses = leaderResponses;
    }

    public ArrayList<String> getPeerResponses() {
        return peerResponses;
    }

    public void setPeerResponses(ArrayList<String> peerResponses) {
        this.peerResponses = peerResponses;
    }

    public ArrayList<String> getCollaboratorResponses() {
        return collaboratorResponses;
    }

    public void setCollaboratorResponses(ArrayList<String> collaboratorResponses) {
        this.collaboratorResponses = collaboratorResponses;
    }
}
