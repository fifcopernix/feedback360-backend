package com.pernix.feedback360.data;

import org.springframework.data.repository.CrudRepository;

import com.pernix.feedback360.entities.QuestionType;

public interface QuestionTypeRepository extends CrudRepository<QuestionType, Integer> {
    QuestionType findByName(String name);
}
