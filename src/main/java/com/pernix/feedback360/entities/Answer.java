package com.pernix.feedback360.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String response;

    @ManyToOne
    @JoinColumn(name = "FK_evaluatorRoleId", nullable = false)
    private EvaluatorRole evaluatorRole;

    @ManyToOne
    @JoinColumn(name = "FK_evaluatorId", nullable = false)
    private Evaluator evaluator;

    @ManyToOne
    @JoinColumn(name = "FK_questionId", nullable = false)
    private Question question;

    @ManyToOne
    @JoinColumn(name = "FK_evaluationId", nullable = false)
    private Evaluation evaluation;

    public Answer() {
    }

    public Answer(Answer answer) {
        this.id = answer.getId();
        this.response = answer.getResponse();
        this.question = answer.getQuestion();
        this.evaluation = answer.getEvaluation();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }

    public EvaluatorRole getEvaluatorRole() {
        return evaluatorRole;
    }

    public void setEvaluatorRole(EvaluatorRole evaluatorRole) {
        this.evaluatorRole = evaluatorRole;
    }
    
    public Evaluator getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(Evaluator evaluator) {
        this.evaluator = evaluator;
    }
}
