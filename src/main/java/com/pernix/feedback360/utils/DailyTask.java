package com.pernix.feedback360.utils;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.pernix.feedback360.data.EvaluationRepository;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.util.EvaluationStatus;
import com.pernix.feedback360.entities.util.EvaluatorStatus;

@Component
public class DailyTask {

	private final EvaluationRepository evaluationRepository;
	private final MailUtil mailUtil;

	@Autowired
	public DailyTask(EvaluationRepository evaluationRepository, MailUtil mailUtil) {
		this.evaluationRepository = evaluationRepository;
		this.mailUtil = mailUtil;
	}

	Date clearDate(Date request) {
		Date res = request;
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(request);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		res = calendar.getTime();

		return res;

	}

	@Transactional
	@Scheduled(cron = "0 0 23 * * ?")
	public void closeEvaluations() {
		Date now = new Date();
		long today = now.getTime();
		Iterable<Evaluation> evaluations = evaluationRepository.findAllByStatus(EvaluationStatus.IN_PROGRESS);
		for(Evaluation evaluation : evaluations) {
			long endDate = evaluation.getStage().getEndDate().getTime();
			if (today > endDate) {
				evaluation.setStatus(EvaluationStatus.COMPLETED);
				this.evaluationRepository.save(evaluation);
			}
		}
		
	}

	@Transactional
	@Scheduled(cron = "0 0 23 1/5 * ?")
	public void sendRemindersEvaluations() {
		Iterable<Evaluation> evaluations = evaluationRepository.findAllByStatus(EvaluationStatus.IN_PROGRESS);
		Date now = new Date();
		for (Evaluation evaluation : evaluations) {
			long startDate = clearDate(evaluation.getStage().getStartDate()).getTime();
			LocalDate satrtDateLocal = Instant.ofEpochMilli(startDate).atZone(ZoneId.systemDefault()).toLocalDate();// getDateFromTimestamp(startDate);
			LocalDate endDateLocal = Instant.ofEpochMilli(now.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
			Period diff = Period.between(satrtDateLocal, endDateLocal);
			if (diff.getDays() >= 3) {
				Set<Evaluator> evaluators = evaluation.getEvaluators();
				for (Evaluator evaluator : evaluators) {
					if (evaluator.getStatus() != EvaluatorStatus.COMPLETED) {
						String login = evaluator.getUser() != null ? evaluator.getUser().getLogin()
								: evaluator.getName();
						mailUtil.sendInvitationToEvaluation(evaluation, evaluator, login, "es");
					}
				}
			}
		}
	}
}
