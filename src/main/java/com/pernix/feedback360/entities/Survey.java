package com.pernix.feedback360.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Survey {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String nameEs;

    @NotEmpty
    @Column(nullable = false)
    private String nameEn;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "survey")
    private Set<Topic> topics;

    public Survey() {

    }

    public Survey(Survey survey) {
        this.id = survey.getId();
        this.nameEs = survey.getNameEs();
        this.nameEn = survey.getNameEn();
        this.topics = survey.getTopics();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEs() {
        return nameEs;
    }

    public void setNameEs(String name) {
        this.nameEs = name;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEN) {
        this.nameEn = nameEN;
    }

    public Set<Topic> getTopics() {
        return topics;
    }

    public void setTopics(Set<Topic> topics) {
        this.topics = topics;
    }

}
