package com.pernix.feedback360.services.workers;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pernix.feedback360.data.AnswerRepository;
import com.pernix.feedback360.data.EvaluatorRepository;
import com.pernix.feedback360.data.EvaluatorRoleRepository;
import com.pernix.feedback360.data.QuestionRepository;
import com.pernix.feedback360.data.QuestionTypeRepository;
import com.pernix.feedback360.data.SubtopicRepository;
import com.pernix.feedback360.data.TopicRepository;
import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Question;
import com.pernix.feedback360.entities.QuestionType;
import com.pernix.feedback360.entities.Subtopic;
import com.pernix.feedback360.entities.Topic;
import com.pernix.feedback360.services.pojos.results.EvaluationResult;
import com.pernix.feedback360.services.pojos.results.OpenQuestionResult;

@Service
public class EvaluationResultOpenQuestionsServicesWorker {

    private final AnswerRepository answerRepository;
    private final TopicRepository topicRepository;
    private final SubtopicRepository subtopicRepository;
    private final QuestionRepository questionRepository;
    private final QuestionTypeRepository questionTypeRepository;

    @Autowired
    public EvaluationResultOpenQuestionsServicesWorker(EvaluatorRepository evaluatorRepository,
            EvaluatorRoleRepository evaluatorRoleRepository, AnswerRepository answerRepository,
            TopicRepository topicRepository, SubtopicRepository subtopicRepository,
            QuestionRepository questionRepository, QuestionTypeRepository questionTypeRepository) {
        this.answerRepository = answerRepository;
        this.topicRepository = topicRepository;
        this.subtopicRepository = subtopicRepository;
        this.questionRepository = questionRepository;
        this.questionTypeRepository = questionTypeRepository;
    }

    public void createOpenQuestionsResults(EvaluationResult results, Evaluation evaluation, String language) {

        ArrayList<OpenQuestionResult> questionResults = new ArrayList<OpenQuestionResult>();
        Set<Topic> topics = topicRepository.findAllBySurvey(evaluation.getSurvey());

        for (Topic topic : topics) {
            createTopicResults(evaluation, topic, questionResults, language);
        }

        results.setOpenQuestions(questionResults);
    }

    private void createTopicResults(Evaluation evaluation, Topic topic, ArrayList<OpenQuestionResult> questionResults, String language) {

        Set<Subtopic> subtopics = subtopicRepository.findAllByTopic(topic);

        for (Subtopic subtopic : subtopics) {

            createSubtopicResults(evaluation, subtopic, questionResults, language);

        }
    }

    private void createSubtopicResults(Evaluation evaluation, Subtopic subtopic, ArrayList<OpenQuestionResult> questionResults, String language) {

        QuestionType openQuestionType = questionTypeRepository.findByName("Open");

        Set<Question> questions = questionRepository.findAllBySubtopic(subtopic);

        for (Question question : questions) {
            if (question.getType().getId() == openQuestionType.getId()) {
                OpenQuestionResult questionResult = new OpenQuestionResult();
                if("en".equals(language)) {
                	questionResult.setStatement(question.getDescriptionEn());
                } else {
                	questionResult.setStatement(question.getDescriptionEs());
                }         
                questionResult.setSelfAssesmentResponse("");
                questionResult.setLeaderResponses(new ArrayList<String>());
                questionResult.setPeerResponses(new ArrayList<String>());
                questionResult.setCollaboratorResponses(new ArrayList<String>());

                Set<Answer> answers = answerRepository.findAllByQuestion(question);

                for (Answer answer : answers) {
                    if (answer.getEvaluation().getId() == evaluation.getId()) {
                        String response = answer.getResponse();
                        switch (answer.getEvaluatorRole().getName()) {
                        case "Auto":
                            questionResult.setSelfAssesmentResponse(response);
                            break;
                        case "Jefe":
                            questionResult.getLeaderResponses().add(response);
                            break;
                        case "Par":
                            questionResult.getPeerResponses().add(response);
                            break;
                        case "Colaborador":
                            questionResult.getCollaboratorResponses().add(response);
                            break;
                        default:
                            break;
                        }
                    }
                }

                questionResults.add(questionResult);
            }
        }
    }
}
