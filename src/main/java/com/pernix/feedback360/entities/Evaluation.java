package com.pernix.feedback360.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pernix.feedback360.entities.util.EvaluationStatus;

@Entity
public class Evaluation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(nullable = false)
    private EvaluationStatus status;

    @ManyToOne
    @JoinColumn(name = "FK_surveyId")
    private Survey survey;

    @ManyToOne
    @JoinColumn(name = "FK_userId")
    private Users user;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "evaluation")
    private Set<Evaluator> evaluators;

    @ManyToOne
    @JoinColumn(name = "FK_stageId", nullable = false)
    private Stage stage;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "evaluation")
    private Set<Answer> answers;

    private String language;

    public Evaluation() {
    }

    public Evaluation(Evaluation evaluation) {
        id = evaluation.getId();
        user = evaluation.getUser();
        status = evaluation.getStatus();
        survey = evaluation.getSurvey();
        evaluators = evaluation.getEvaluators();
        stage = evaluation.getStage();
        answers = evaluation.getAnswers();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EvaluationStatus getStatus() {
        return status;
    }

    public void setStatus(EvaluationStatus status) {
        this.status = status;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Set<Evaluator> getEvaluators() {
        return evaluators;
    }

    public void setEvaluators(Set<Evaluator> evaluator) {
        evaluators = evaluator;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

}
