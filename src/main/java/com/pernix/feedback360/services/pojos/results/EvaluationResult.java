package com.pernix.feedback360.services.pojos.results;

import java.util.ArrayList;

public class EvaluationResult {

    private int countSelfAssessmentEvaluators;

    private int countLeaderEvaluators;

    private int countCollaboratorEvaluators;

    private int countPeerEvaluators;

    private int countOverallEvaluators;

    private ArrayList<TopicResult> selfAssessmentResults;

    private ArrayList<TopicResult> leaderEvaluatorsResults;

    private ArrayList<TopicResult> collaboratorEvaluatorsResults;

    private ArrayList<TopicResult> peerEvaluatorsResults;

    private ArrayList<TopicResult> evaluatorsResults;

    private ArrayList<TopicComparatorResult> overallResults;

    private ArrayList<QuestionResult> bestQuestions;

    private ArrayList<QuestionResult> worstQuestions;

    private ArrayList<OpenQuestionResult> openQuestions;

    public int getCountSelfAssessmentEvaluators() {
        return countSelfAssessmentEvaluators;
    }

    public void setCountSelfAssessmentEvaluators(int countSelfAssessmentEvaluators) {
        this.countSelfAssessmentEvaluators = countSelfAssessmentEvaluators;
    }

    public int getCountLeaderEvaluators() {
        return countLeaderEvaluators;
    }

    public void setCountLeaderEvaluators(int countLeaderEvaluators) {
        this.countLeaderEvaluators = countLeaderEvaluators;
    }

    public int getCountCollaboratorEvaluators() {
        return countCollaboratorEvaluators;
    }

    public void setCountCollaboratorEvaluators(int countCollaboratorEvaluators) {
        this.countCollaboratorEvaluators = countCollaboratorEvaluators;
    }

    public int getCountPeerEvaluators() {
        return countPeerEvaluators;
    }

    public void setCountPeerEvaluators(int countPeerEvaluators) {
        this.countPeerEvaluators = countPeerEvaluators;
    }

    public int getCountOverallEvaluators() {
        return countOverallEvaluators;
    }

    public void setCountOverallEvaluators(int countOverallEvaluators) {
        this.countOverallEvaluators = countOverallEvaluators;
    }

    public ArrayList<TopicResult> getSelfAssessmentResults() {
        return selfAssessmentResults;
    }

    public void setSelfAssessmentResults(ArrayList<TopicResult> selfAssessmentResults) {
        this.selfAssessmentResults = selfAssessmentResults;
    }

    public ArrayList<TopicResult> getLeaderEvaluatorsResults() {
        return leaderEvaluatorsResults;
    }

    public void setLeaderEvaluatorsResults(ArrayList<TopicResult> leaderEvaluatorsResults) {
        this.leaderEvaluatorsResults = leaderEvaluatorsResults;
    }

    public ArrayList<TopicResult> getCollaboratorEvaluatorsResults() {
        return collaboratorEvaluatorsResults;
    }

    public void setCollaboratorEvaluatorsResults(ArrayList<TopicResult> collaboratorEvaluatorsResults) {
        this.collaboratorEvaluatorsResults = collaboratorEvaluatorsResults;
    }

    public ArrayList<TopicResult> getPeerEvaluatorsResults() {
        return peerEvaluatorsResults;
    }

    public void setPeerEvaluatorsResults(ArrayList<TopicResult> peerEvaluatorsResults) {
        this.peerEvaluatorsResults = peerEvaluatorsResults;
    }

    public ArrayList<TopicResult> getEvaluatorsResults() {
        return evaluatorsResults;
    }

    public void setEvaluatorsResults(ArrayList<TopicResult> evaluatorsResults) {
        this.evaluatorsResults = evaluatorsResults;
    }

    public ArrayList<TopicComparatorResult> getOverallResults() {
        return overallResults;
    }

    public void setOverallResults(ArrayList<TopicComparatorResult> overallResults) {
        this.overallResults = overallResults;
    }

    public ArrayList<QuestionResult> getBestQuestions() {
        return bestQuestions;
    }

    public void setBestQuestions(ArrayList<QuestionResult> bestQuestions) {
        this.bestQuestions = bestQuestions;
    }

    public ArrayList<QuestionResult> getWorstQuestions() {
        return worstQuestions;
    }

    public void setWorstQuestions(ArrayList<QuestionResult> worstQuestions) {
        this.worstQuestions = worstQuestions;
    }

    public ArrayList<OpenQuestionResult> getOpenQuestions() {
        return openQuestions;
    }

    public void setOpenQuestions(ArrayList<OpenQuestionResult> openQuestions) {
        this.openQuestions = openQuestions;
    }
}
