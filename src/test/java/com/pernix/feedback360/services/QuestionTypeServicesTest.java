
package com.pernix.feedback360.services;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import com.google.gson.Gson;
import com.pernix.feedback360.Application;
import com.pernix.feedback360.data.QuestionTypeRepository;
import com.pernix.feedback360.entities.QuestionType;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class QuestionTypeServicesTest extends BaseTest {

    private static final String PATH = "/questionTypes";

    @Autowired
    protected QuestionTypeRepository questionTypeRepository;

    private QuestionType questionType;

    @Before
    public void prepareTestMethod() throws Exception {
        questionType = questionTypeRepository.findOne(1);
    }

    @Test
    public void getAll() throws Exception {

        ResultActions result = perform(get(PATH)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].name", is("Open")))
                .andExpect(jsonPath("$[1].name", is("Closed")));
    }

    @Test
    public void getById() throws Exception {
        String path = String.format("%s/%d", PATH, questionType.getId());

        ResultActions result = perform(get(path)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        result.andExpect(jsonPath("id", is(questionType.getId())))
                .andExpect(jsonPath("name", is(questionType.getName())));
    }

    @Test
    public void create() throws Exception {
        QuestionType request = new QuestionType();
        request.setName("Question Type Name");

        perform(post(PATH).content(new Gson().toJson(request))).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void update() throws Exception {
        String path = String.format("%s/%d", PATH, questionType.getId());
        final String updatedName = "Updated name";

        QuestionType request = questionType;
        questionType.setQuestions(null);
        request.setName(updatedName);

        perform(put(path).content(new Gson().toJson(request))).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void destroy() throws Exception {
        String path = String.format("%s/%d", PATH, questionType.getId());

        perform(delete(path)).andExpect(status().isMethodNotAllowed());
    }
}
