    <!doctype html>
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
      <title></title>  
    </head>
    <body>
      <div style="width:100%!important;min-width:100%;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;text-align:left;line-height:19px;font-size:14px;background:#428bca;margin:0;padding:0" bgcolor="#428bca">
        <table bgcolor="#428bca" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;height:100%;width:100%;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;background:#428bca;margin:0;padding:0">
          <tbody>
            <tr align="left" style="vertical-align:top;text-align:left;padding:0">
              <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:0" valign="top">
                <center style="width:100%;min-width:580px">
                  <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0">
                    <tbody>
                      <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                        <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:0" valign="top">
                          <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:100%;display:block;padding:0">
                            <tbody>
                              <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                                <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:10px 0 0" valign="top">
                                  <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;margin:0 auto;padding:0">
                                    <tbody>
                                      <tr>
                                        <td style="text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;padding-bottom:20px;padding-top:20px;"><img style="max-height: 150px;" alt="Pernix-Solutions" src="http://fifco360feedback-fe.herokuapp.com/app/images/fifco3.png"></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0">
                    <tbody>
                      <tr align="center" style="vertical-align:top;text-align:left;padding:0">
                        <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:0" valign="top">
                          <table bgcolor="#fff" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:100%;display:block;border-radius:5px;background:#fff;padding:0">
                            <tbody>
                              <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                                <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:20px 0" valign="top">
                                  <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;margin:0 auto;padding:0">
                                    <tbody>
                                      <tr align="center" style="vertical-align:top;text-align:center;padding:0">
                                        <td align="center" style="word-break:break-word;vertical-align:top;text-align:center;color:rgb(34,34,34);font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0px;padding:0px 35px 10px;border-collapse:collapse!important;background:rgb(255,255,255)" valign="top">

  <div style="text-align:left;color:#555;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.6;font-size:14px;margin:0 0 10px;padding:0 0 10px">
      <p>Dear ${employeeName},</p>
      <p>I invite you to participate in my 360-degree Feedback Evaluation, which is applied to provide 
    FIFCO with inputs about my overall performance.</p>
      <p>This is a competency evaluation, and it is called 360 degrees because different people participate.
       They all work with me and can evaluate how I behave, and thus, will help me know and provide
       my leadership skills.</p>
  <p> This evaluation is generated through a system developed by our supplier Pernix S.A. and it
   guarantees total confidentially. Each answer provided is processed and the results are presented
   as group averaged</p>
  <p>I would like to request you to be one of my evaluators, which implies that:</p>
      <ul>
          <li>You enter this link to perform the survey (The functionality is not optimal in Internet Explorer, the recommended navigator is Chrome): https://fifco360feedback-fe.herokuapp.com/#/evaluations?key=${key}</li>
          <li>You will have time until ${deadline} to fill out the survey</li>
          <li>Completing the survey will take approximately 20 minutes</li>
      </ul>
      <p>Please let me know ASAP if you can not perform this evaluation.</p>
      <p>Here are some tips to fill out the Evaluation:</p>
      <ul>
          <li>Conduct the evaluation based on what you have personally observed about my behavior,</li>
          <li>Conduct the evaluation as objectively as possible, using the scale to reflect strengths and areas for improvement.</li>
          <li>In the case of open questions, please express in the most open and constructive way your recommendations for my improvement and development.</li>
      </ul>
      <p>I thank you in advance for your valuable feedback.</p>
      <p>Best regards,</p>
      <p>${user}</p>

  </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0">
                    <tbody>
                      <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                        <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0" valign="top">
                          <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:100%;display:block;padding:0">
                            <tbody style="display:block">
                              <tr align="left" style="vertical-align:top;text-align:left;padding:0;display:block">
                               <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:10px 0 0 0" valign="top">
                                  <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:175px;margin:0 auto;padding:0">
                                    <tbody>
                                      <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                                          <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0 0 10px" valign="top"><!-- <a href="http://www.pernix-solutions.com" target="_blank"><img align="none" alt="Pernix logo" src="https://pernixnewsletter.files.wordpress.com/2015/10/logoo1.png" style="outline:0;text-decoration:none;width:auto;max-width:30px;float:none;clear:both;display:inline-block;margin:0" class="CToWUd"></a> --></td>
                                        <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;width:0;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0" valign="top">&nbsp;</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:10px 10px 0" valign="top">
                                  <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;width:210px;margin:0 auto;padding:0;text-align:center">
                                    <tbody style="display:inline-block">
                                      <tr align="left" style="vertical-align:top;text-align:left;padding:0;display:inline-block">
                                        <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#fff;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0 0 10px" valign="top"><br>
                                        <br>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                                <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:10px 0 0" valign="middle">
                                  <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:right;width:175px!important;margin:0 auto;padding:0">
                                    <tbody style="display:inline-block">
                                      <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                                        <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;width:auto!important;margin:0;padding:0 0 10px" valign="top"><a href="#" style="color:#2ba6cb;text-decoration:none;display:block;width:25px;min-height:auto;text-align:right!important" target="_blank"><img align="none" alt="Pernix on Facebook" src="https://go.pardot.com/l/36622/2015-12-14/71t9yg/36622/93095/fb.png" style="outline:0;text-decoration:none;width:auto;max-width:100%;float:none;clear:both;display:inline-block;margin:0;border:none"></a></td>
                                        <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;width:auto!important;margin:0;padding:0 0 10px" valign="top"><a href="#" style="color:#2ba6cb;text-decoration:none;display:block;width:25px;min-height:auto;text-align:right!important" target="_blank"><img align="none" alt="Pernix on LinkedIn" src="https://go.pardot.com/l/36622/2016-07-18/8yhgn5/36622/109842/linkedin.png" style="outline:0;text-decoration:none;width:auto;max-width:100%;float:none;clear:both;display:inline-block;margin:0;border:none"></a></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </body>
    </html>