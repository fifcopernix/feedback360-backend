
package com.pernix.feedback360.services;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pernix.feedback360.Application;
import com.pernix.feedback360.data.QuestionRepository;
import com.pernix.feedback360.data.QuestionTypeRepository;
import com.pernix.feedback360.entities.Question;
import com.pernix.feedback360.entities.QuestionType;
import com.pernix.feedback360.entities.Subtopic;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class QuestionServicesTest extends BaseTest {

    private static String NAME_EN = "how r u?";
    private static String NAME_ES = "como estas?";
    private static String PATH = "/questions";
    private static String PATH2 = "/questions-by-subtopic";

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionTypeRepository questionTypeRepository;

    private QuestionType questionType;
    private Subtopic subtopic;
    private Question questionOne;
    private Question questionTwo;

    @Before
    public void prepareTestMethod() throws Exception {
        questionType = questionTypeRepository.findOne(1);
        topic = createTopic();
        subtopic = createSubtopic();
        questionOne = createQuestion();
        questionTwo = createQuestion();

    }

    @Test
    public void create() throws Exception {

        Question request = new Question();
        request.setDescriptionEn(NAME_EN);
        request.setDescriptionEs(NAME_ES);

        JsonElement jsonRequest = (new Gson()).toJsonTree(request);

        JsonObject surveyObject = new JsonObject();
        surveyObject.addProperty("id", survey.getId());

        JsonObject questionTypeObject = new JsonObject();
        questionTypeObject.addProperty("id", questionType.getId());

        JsonObject subtopicObject = new JsonObject();
        subtopicObject.addProperty("id", subtopic.getId());

        jsonRequest.getAsJsonObject().add("type", questionTypeObject);
        jsonRequest.getAsJsonObject().add("survey", surveyObject);
        jsonRequest.getAsJsonObject().add("subtopic", subtopicObject);

        perform(post(PATH).content(jsonRequest.toString())).andExpect(status().isCreated());
    }

    @Test
    public void getAll() throws Exception {
        perform(get(PATH)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void getById() throws Exception {
        Question question = createQuestion();
        String path = String.format("%s/%d", PATH, question.getId());

        perform(get(path)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void getBySubtopic() throws Exception {
        String path = String.format("%s?subtopicId=%d", PATH2, subtopic.getId());
        perform(get(path)).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void update() throws Exception {
        Question request = questionOne;

        request.setSubtopic(null);
        request.setType(null);

        final String updatedNameEN = "test update";
        final String updatedNameES = "prueba update";
        request.setDescriptionEs(updatedNameES);
        request.setDescriptionEn(updatedNameEN);
        JsonElement jsonRequest = (new Gson()).toJsonTree(request);

        JsonObject surveyObject = new JsonObject();
        surveyObject.addProperty("id", survey.getId());

        JsonObject questionTypeObject = new JsonObject();
        questionTypeObject.addProperty("id", questionType.getId());

        JsonObject subtopicObject = new JsonObject();
        subtopicObject.addProperty("id", subtopic.getId());

        jsonRequest.getAsJsonObject().add("type", questionTypeObject);
        jsonRequest.getAsJsonObject().add("survey", surveyObject);
        jsonRequest.getAsJsonObject().add("subtopic", subtopicObject);

        perform(put(PATH).content(jsonRequest.toString())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void destroy() throws Exception {
        Question question = questionTwo;
        String path = String.format("%s/%d", PATH, question.getId());
        perform(delete(path)).andExpect(status().isOk());
    }
}
