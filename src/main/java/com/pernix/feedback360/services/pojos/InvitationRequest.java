package com.pernix.feedback360.services.pojos;

import com.pernix.feedback360.entities.Evaluation;

public class InvitationRequest {

    private Evaluation evaluation;

    private Iterable<EvaluatorRequest> evaluators;

    private String language;

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }

    public Iterable<EvaluatorRequest> getEvaluators() {
        return evaluators;
    }

    public void setEvaluators(Iterable<EvaluatorRequest> evaluators) {
        this.evaluators = evaluators;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

}
