package com.pernix.feedback360.utils;

import java.util.Base64;

import com.pernix.feedback360.data.EvaluatorRepository;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;

public class UniqueLinkGenerator {
    EvaluatorRepository evaluatorRepository;
    public static String generateLinkKey(String evaluationId, String evaluationStageId, String userId) {
        String response = "";
        String joined = String.join("---", evaluationId, userId, evaluationStageId);
        byte[] encoded = Base64.getEncoder().withoutPadding().encode(joined.getBytes());
        response = new String(encoded);
        return response;
    }

    public static boolean checkLinkKey(String key, Evaluation evaluation, Evaluator evaluator) {
        boolean isEquals = false;

        byte[] decoded = Base64.getDecoder().decode(key);
        String decodedString = new String(decoded);
        String[] splitted = decodedString.split("---");
        if (evaluator.getUser() != null) {
            isEquals = String.valueOf(evaluation.getId()).equals(splitted[0])
                    && String.valueOf(evaluator.getUser().getLogin()).equals(splitted[1])
                    && String.valueOf(evaluation.getStage().getId()).equals(splitted[2]);
        } else {
            isEquals = String.valueOf(evaluation.getId()).equals(splitted[0])
                    && String.valueOf(evaluator.getName()).equals(splitted[1])
                    && String.valueOf(evaluation.getStage().getId()).equals(splitted[2]);
        }

        return isEquals;
    }
}
