package com.pernix.feedback360.data;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.pernix.feedback360.entities.Question;
import com.pernix.feedback360.entities.Subtopic;

public interface QuestionRepository extends CrudRepository<Question, Integer> {
    Set<Question> findAllBySubtopic(Subtopic subtopic);

    @Query("from Question where descriptionEs=:descriptionEs")
    Question findByDescriptionEs(@Param("descriptionEs") String descriptionEs);
}
