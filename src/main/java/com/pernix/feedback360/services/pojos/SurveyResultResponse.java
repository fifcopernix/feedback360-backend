package com.pernix.feedback360.services.pojos;

import java.util.ArrayList;

public class SurveyResultResponse {

    private int id;
    private String nameEs;
    private String nameEn;
    private ArrayList<TopicResultResponse> topics;

    public SurveyResultResponse() {
        topics = new ArrayList<TopicResultResponse>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEs() {
        return nameEs;
    }

    public void setNameEs(String name) {
        this.nameEs = name;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEN) {
        this.nameEn = nameEN;
    }

    public ArrayList<TopicResultResponse> getTopics() {
        return topics;
    }

    public void setTopics(ArrayList<TopicResultResponse> topics) {
        this.topics = topics;
    }

}
