package com.pernix.feedback360.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String nameEs;

    @NotEmpty
    @Column(nullable = false)
    private String nameEn;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "topic")
    private Set<Subtopic> subtopics;

    @ManyToOne
    @JoinColumn(name = "FK_surveyId", nullable = false)
    private Survey survey;

    public Topic() {

    }

    public Topic(Topic topic) {
        this.id = topic.getId();
        this.nameEs = topic.getNameEs();
        this.nameEn = topic.getNameEn();
        this.subtopics = topic.getSubtopics();
        this.survey = topic.getSurvey();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEs() {
        return nameEs;
    }

    public void setNameEs(String name) {
        this.nameEs = name;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEN) {
        this.nameEn = nameEN;
    }

    public Set<Subtopic> getSubtopics() {
        return subtopics;
    }

    public void setSubtopics(Set<Subtopic> subtopics) {
        this.subtopics = subtopics;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }
}
