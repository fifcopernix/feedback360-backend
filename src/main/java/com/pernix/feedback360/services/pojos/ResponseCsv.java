package com.pernix.feedback360.services.pojos;

public class ResponseCsv {
    private String header;
    private String data;
    private String filename;

    public ResponseCsv() {

    }

    public ResponseCsv(String header, String data, String filename) {
        this.header = header;
        this.data = data;
        this.filename = filename;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

}
