package com.pernix.feedback360.services.workers;

import com.pernix.feedback360.data.AnswerRepository;
import com.pernix.feedback360.data.EvaluationRepository;
import com.pernix.feedback360.data.EvaluatorRepository;
import com.pernix.feedback360.data.EvaluatorRoleRepository;
import com.pernix.feedback360.data.QuestionRepository;
import com.pernix.feedback360.data.QuestionTypeRepository;
import com.pernix.feedback360.data.SubtopicRepository;
import com.pernix.feedback360.data.TopicRepository;
import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.EvaluatorRole;
import com.pernix.feedback360.entities.Question;
import com.pernix.feedback360.entities.QuestionType;
import com.pernix.feedback360.entities.Subtopic;
import com.pernix.feedback360.entities.Topic;
import com.pernix.feedback360.entities.Users;
import com.pernix.feedback360.entities.util.EvaluationStatus;
import com.pernix.feedback360.entities.util.EvaluatorStatus;
import com.pernix.feedback360.services.pojos.EvaluationResponse;
import com.pernix.feedback360.services.pojos.ResponseCsv;
import com.pernix.feedback360.services.pojos.results.EvaluationResult;
import com.pernix.feedback360.services.pojos.results.SubtopicComparatorResult;
import com.pernix.feedback360.services.pojos.results.SubtopicResult;
import com.pernix.feedback360.services.pojos.results.TopicComparatorResult;
import com.pernix.feedback360.services.pojos.results.TopicResult;
import com.pernix.feedback360.utils.CSVGenerator;

import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class EvaluationResultServicesWorker {

    private final EvaluatorRepository evaluatorRepository;
    private final EvaluatorRoleRepository evaluatorRoleRepository;
    private final AnswerRepository answerRepository;
    private final TopicRepository topicRepository;
    private final SubtopicRepository subtopicRepository;
    private final QuestionRepository questionRepository;
    private final QuestionTypeRepository questionTypeRepository;
    private final EvaluationRepository evaluationRepository;
    private final CSVGenerator csvGenerator;

    private final EvaluationResultRankingServicesWorker rankingResults;
    private final EvaluationResultOpenQuestionsServicesWorker openQuestionsResults;

    @Autowired
    public EvaluationResultServicesWorker(EvaluatorRepository evaluatorRepository,
            EvaluatorRoleRepository evaluatorRoleRepository, AnswerRepository answerRepository,
            TopicRepository topicRepository, SubtopicRepository subtopicRepository,
            QuestionRepository questionRepository, QuestionTypeRepository questionTypeRepository,
            EvaluationResultRankingServicesWorker rankingResults,
            EvaluationResultOpenQuestionsServicesWorker openQuestionsResults,
            EvaluationRepository evaluationRepository, CSVGenerator csvGenerator) {
        this.evaluatorRepository = evaluatorRepository;
        this.evaluatorRoleRepository = evaluatorRoleRepository;
        this.answerRepository = answerRepository;
        this.topicRepository = topicRepository;
        this.subtopicRepository = subtopicRepository;
        this.questionRepository = questionRepository;
        this.questionTypeRepository = questionTypeRepository;
        this.rankingResults = rankingResults;
        this.openQuestionsResults = openQuestionsResults;
        this.evaluationRepository = evaluationRepository;
        this.csvGenerator = csvGenerator;
    }

    public EvaluationResult createResults(Evaluation evaluation, String language) {

        EvaluatorRole selfEvaluatorRole = evaluatorRoleRepository.findByName("Auto");
        EvaluatorRole leaderEvaluatorRole = evaluatorRoleRepository.findByName("Jefe");
        EvaluatorRole peerEvaluatorRole = evaluatorRoleRepository.findByName("Par");
        EvaluatorRole collaboratorEvaluatorRole = evaluatorRoleRepository.findByName("Colaborador");

        ArrayList<EvaluatorRole> evaluatorRoles = new ArrayList<EvaluatorRole>();
        evaluatorRoles.add(leaderEvaluatorRole);
        evaluatorRoles.add(peerEvaluatorRole);
        evaluatorRoles.add(collaboratorEvaluatorRole);

        Set<Evaluator> overallEvaluators = evaluatorRepository.findAllByStatusAndEvaluation(EvaluatorStatus.COMPLETED,
                evaluation);

        EvaluationResult results = new EvaluationResult();
        results.setCountOverallEvaluators(overallEvaluators.size());
        results.setCountSelfAssessmentEvaluators(countTotalEvaluators(evaluation, selfEvaluatorRole));
        results.setCountLeaderEvaluators(countTotalEvaluators(evaluation, leaderEvaluatorRole));
        results.setCountPeerEvaluators(countTotalEvaluators(evaluation, peerEvaluatorRole));
        results.setCountCollaboratorEvaluators(countTotalEvaluators(evaluation, collaboratorEvaluatorRole));

        results.setSelfAssessmentResults(createResultsForEvaluators(evaluation, selfEvaluatorRole, null, language));
        results.setLeaderEvaluatorsResults(createResultsForEvaluators(evaluation, leaderEvaluatorRole, null, language));
        results.setPeerEvaluatorsResults(createResultsForEvaluators(evaluation, peerEvaluatorRole, null, language));
        results.setCollaboratorEvaluatorsResults(
                createResultsForEvaluators(evaluation, collaboratorEvaluatorRole, null, language));
        results.setEvaluatorsResults(createResultsForEvaluators(evaluation, null, evaluatorRoles, language));

        results.setOverallResults(
                createComparatorResults(results.getSelfAssessmentResults(), results.getEvaluatorsResults()));

        rankingResults.createRankingResults(results, evaluation, evaluatorRoles, language);

        openQuestionsResults.createOpenQuestionsResults(results, evaluation, language);

        return results;
    }

    public ResponseCsv GetUserEvaluationsByDate(Users user, Date initialDate, Date finalDate) {
        Set<Evaluation> evaluations = evaluationRepository.findAllByUser(user);
        Set<Evaluation> evaluationsFilteredByRangeDate = new HashSet<Evaluation>();
        for (Evaluation evaluation: evaluations) {
            if ((evaluation.getStage().getEndDate().before(finalDate) || evaluation.getStage().getEndDate() == finalDate)
                && evaluation.getStatus().name() == EvaluationStatus.COMPLETED.name() && (evaluation.getStage().getEndDate().after(initialDate)
                || evaluation.getStage().getEndDate() == initialDate)) {
                evaluationsFilteredByRangeDate.add(evaluation);
            }
        }
        ResponseCsv dataResult = generateCSV(evaluationsFilteredByRangeDate, initialDate, finalDate,user);
        return dataResult;
    }

    public ResponseCsv generateCSV(Set<Evaluation> evaluations, Date initialDate, Date finalDate,Users user) {
        int evaluators = 0;
        for (Evaluation evaluation: evaluations) {
            Set<Evaluator> evaluatorsByEvaluation = evaluatorRepository.findAllByEvaluation(evaluation);
            for (Evaluator evaluator: evaluatorsByEvaluation) {
                if (evaluator.getEvaluation().getId() == evaluation.getId()) {
                    evaluators = evaluators + 1;
                }
            }
        }
        
        String headers = getEvaluationResultsHeaders(evaluators);
        ResponseCsv response = new ResponseCsv();
        LocalDateTime begin = LocalDateTime.ofInstant(initialDate.toInstant(), ZoneId.systemDefault());
        LocalDateTime end = LocalDateTime.ofInstant(finalDate.toInstant(), ZoneId.systemDefault());
        String beginDate = begin.format(DateTimeFormatter.ofPattern("yyyyMMdd", Locale.ENGLISH));
        String endDate = end.format(DateTimeFormatter.ofPattern("yyyyMMdd", Locale.ENGLISH));
        Set<Answer> answers = new HashSet<Answer>();
        for (Evaluation evaluation: evaluations) {
            answers.addAll(answerRepository.findAllByEvaluation(evaluation));
        }
        HashMap<String, ArrayList<Answer>> result =  getEvaluationResults(answers);
        String csvData = csvGenerator.GenerateCSV(result);
        response.setData(csvData);
        String fileName = user.getName() + "_" + beginDate + "_" + endDate + ".csv";
        response.setHeader(headers);
        response.setFilename(fileName);
        return response;
    }

    public Answer answerForEvaluator(Set<Answer> answers, Evaluator evaluator) {
        for (Answer answer : answers) {
            if (answer.getEvaluator().getId() == evaluator.getId()) {
                return answer;
            }
        }
        return null;
    }

    public Set<Evaluator> getEvaluators(Set<Answer> answers) {
        Set<Evaluator> evaluators = new HashSet<Evaluator>();
        for (Answer answer : answers) {
            evaluators.addAll(evaluatorRepository.findAllByEvaluation(answer.getEvaluation()));
        }
        return evaluators;
    }

    public Set<Question> getQuestions(Set<Answer> answers) {
        Set<Question> questions = new HashSet<Question>();
        for (Answer answer : answers) {
            questions.add(answer.getQuestion());
        }
        return questions;
    }

    public HashMap<String, ArrayList<Answer>> getEvaluatorsAnswers(HashMap<String, ArrayList<Answer>> result, Set<Evaluator> evaluators, Set<Answer> answers, String questionDescriptionEs) {
        for (Evaluator evaluator : evaluators) {
            Answer answer = answerForEvaluator(answers, evaluator);
            if (result.containsKey(questionDescriptionEs)) {
            	if(answer!=null) {
            		result.get(questionDescriptionEs).add(answer);
            	}
                
            } else {
                ArrayList<Answer> answerReponses = new ArrayList<Answer>();
                if(answer!=null) {
                    answerReponses.add(answer);
                    result.put(questionDescriptionEs, answerReponses);
                }
            }
        }
        return result;
    }

    public HashMap<String, ArrayList<Answer>> getEvaluationResults(Set<Answer> answers) {
        Set<Question> questions = getQuestions(answers);
        HashMap<String, ArrayList<Answer>> result = new HashMap<String, ArrayList<Answer>>();
        Set<Evaluator> evaluators = getEvaluators(answers);
        for (Question question : questions) {
            Set<Answer> questionAnswers = question.getAnswers();
            String questionDescription =  question.getDescriptionEs();
            result = getEvaluatorsAnswers(result, evaluators, questionAnswers, questionDescription);
        }
        return result;
    }

    public String getEvaluationResultsHeaders(int evaluatorsSize) {
        String headerLine = "";
        ArrayList<String> headers = new ArrayList<String>();
        headers.add("Pregunta");
        for (int i = 0; i < evaluatorsSize; i++) {
            headers.add("evaluador" + i);
        }
        headerLine = String.join(",", headers);
        return headerLine;
    }

    private int countTotalEvaluators(Evaluation evaluation, EvaluatorRole evaluatorRole) {
        Set<Evaluator> evaluators = evaluatorRepository.findAllByStatusAndEvaluationAndEvaluatorRole(EvaluatorStatus.COMPLETED, evaluation, evaluatorRole);
        return evaluators.size();
    }

    private ArrayList<TopicResult> createResultsForEvaluators(Evaluation evaluation, EvaluatorRole evaluatorRole,
            ArrayList<EvaluatorRole> evaluatorRoles, String language) {
        ArrayList<TopicResult> topicResults = new ArrayList<TopicResult>();
        Set<Topic> topics = topicRepository.findAllBySurvey(evaluation.getSurvey());

        for (Topic topic : topics) {
            TopicResult topicResult = new TopicResult();
            GradeAverage topicGradeAverage = new GradeAverage();
            ArrayList<SubtopicResult> subtopicResults = createSubtopicResults(evaluation, topic, topicGradeAverage, evaluatorRole,
                    evaluatorRoles, language);

            // Filter out topics with no closed questions
            if (topicGradeAverage.getQuestionsCounter() > 0) {
            	if("en".equals(language)) {
            		topicResult.setName(topic.getNameEn());
            	} else {
            		topicResult.setName(topic.getNameEs());
            	}             
                topicResult.setGrade(topicGradeAverage.getAnswersAverage());
                topicResult.setSubtopics(subtopicResults);

                topicResults.add(topicResult);
            }
        }

        return topicResults;
    }

    private ArrayList<SubtopicResult> createSubtopicResults(Evaluation evaluation, Topic topic, GradeAverage topicAverage,
            EvaluatorRole evaluatorRole, ArrayList<EvaluatorRole> evaluatorRoles, String language) {
        ArrayList<SubtopicResult> subtopicResults = new ArrayList<SubtopicResult>();
        Set<Subtopic> subtopics = subtopicRepository.findAllByTopic(topic);

        for (Subtopic subtopic : subtopics) {
            SubtopicResult subtopicResult = new SubtopicResult();

            GradeAverage subtopicAverage = getSubtopicAverage(evaluation, subtopic, evaluatorRole, evaluatorRoles);

            topicAverage.add(subtopicAverage.getQuestionsCounter(), subtopicAverage.getAnswersCounter(),
                    subtopicAverage.getAnswersSum());
            if("en".equals(language)) {
                subtopicResult.setName(subtopic.getNameEn());
            } else {
                subtopicResult.setName(subtopic.getNameEs());
            }
            subtopicResult.setGrade(subtopicAverage.getAnswersAverage());
            subtopicResults.add(subtopicResult);
        }
        return subtopicResults;
    }

    private GradeAverage getSubtopicAverage(Evaluation evaluation, Subtopic subtopic, EvaluatorRole evaluatorRole,
            ArrayList<EvaluatorRole> evaluatorRoles) {

        QuestionType closedQuestionType = questionTypeRepository.findByName("Closed");

        Set<Question> questions = questionRepository.findAllBySubtopic(subtopic);

        GradeAverage average = new GradeAverage();

        for (Question question : questions) {
            if (question.getType().getId() == closedQuestionType.getId()) {

                Set<Answer> answers = null;

                answers = getAnswersFor(question, evaluatorRole, evaluatorRoles);

                for (Answer answer : answers) {
                    if (answer.getEvaluation().getId() == evaluation.getId()) {
                        Double response = Double.parseDouble(answer.getResponse());
                        Double responseValue;
                        try {
                            responseValue = response;

                        } catch (NumberFormatException e) {
                            responseValue = 0.0;
                        }

                        average.addAnswer(responseValue);
                    }
                }

                average.addQuestion();
            }
        }
        return average;
    }

    private Set<Answer> getAnswersFor(Question question, EvaluatorRole evaluatorRole,
            ArrayList<EvaluatorRole> evaluatorRoles) {

        Set<Answer> answers;
        if (evaluatorRoles != null) {
            // Multiple roles
            answers = answerRepository.findAllByQuestionAndEvaluatorRoleIn(question, evaluatorRoles);
        } else if (evaluatorRole != null) {
            // One role
            answers = answerRepository.findAllByQuestionAndEvaluatorRole(question, evaluatorRole);
        } else {
            // Overall results
            answers = answerRepository.findAllByQuestion(question);
        }
        return answers;
    }

    private ArrayList<TopicComparatorResult> createComparatorResults(ArrayList<TopicResult> selfAssessment,
            ArrayList<TopicResult> evaluators) {
        ArrayList<TopicComparatorResult> results = new ArrayList<TopicComparatorResult>();

        for (TopicResult selfAssessmentTopic : selfAssessment) {
            for (TopicResult evaluatorsTopic : evaluators) {
                if (selfAssessmentTopic.getName().equals(evaluatorsTopic.getName())) {

                    TopicComparatorResult topic = createTopicComparatorResult(selfAssessmentTopic, evaluatorsTopic);
                    results.add(topic);
                    break;
                }
            }
        }
        return results;
    }

    private TopicComparatorResult createTopicComparatorResult(TopicResult selfAssessmentTopic,
            TopicResult evaluatorsTopic) {

        TopicComparatorResult topic = new TopicComparatorResult();
        ArrayList<SubtopicComparatorResult> subtopics = new ArrayList<SubtopicComparatorResult>();

        for (SubtopicResult selfAssessmentSubtopic : selfAssessmentTopic.getSubtopics()) {
            for (SubtopicResult evaluatorsSubtopic : evaluatorsTopic.getSubtopics()) {
                if (selfAssessmentSubtopic.getName().equals(evaluatorsSubtopic.getName())) {
                    SubtopicComparatorResult subtopic = new SubtopicComparatorResult();

                    subtopic.setName(selfAssessmentSubtopic.getName());
                    subtopic.setSelfAssessmentGrade(selfAssessmentSubtopic.getGrade());
                    subtopic.setEvaluatorsAverageGrade(evaluatorsSubtopic.getGrade());

                    subtopics.add(subtopic);
                    break;
                }
            }
        }

        topic.setName(selfAssessmentTopic.getName());
        topic.setSelfAssessmentGrade(selfAssessmentTopic.getGrade());
        topic.setEvaluatorsAverageGrade(evaluatorsTopic.getGrade());
        topic.setSubtopics(subtopics);
        return topic;
    }

    private class GradeAverage {
        private int questionsCounter;
        private int answersCounter;
        private double answersSum;

        public GradeAverage() {
            questionsCounter = 0;
            answersCounter = 0;
            answersSum = 0;
        }

        public void add(int toQuestionsCounter, int toAnswersCounter, double toAnswersSum) {
            questionsCounter += toQuestionsCounter;
            answersCounter += toAnswersCounter;
            answersSum += toAnswersSum;
        }

        public void addQuestion() {
            this.questionsCounter++;
        }

        public void addAnswer(double toAnswersSum) {
            answersCounter++;
            answersSum += toAnswersSum;
        }

        public int getQuestionsCounter() {
            return questionsCounter;
        }

        public int getAnswersCounter() {
            return answersCounter;
        }

        public double getAnswersSum() {
            return answersSum;
        }

        public double getAnswersAverage() {
            return answersCounter == 0 ? 0 : answersSum / answersCounter;
        }
    }

}
