package com.pernix.feedback360.data;

import org.springframework.data.repository.CrudRepository;

import com.pernix.feedback360.entities.Survey;

public interface SurveyRepository extends CrudRepository<Survey, Integer> {

}
