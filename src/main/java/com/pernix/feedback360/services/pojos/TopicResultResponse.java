package com.pernix.feedback360.services.pojos;

import java.util.ArrayList;

public class TopicResultResponse {

    private int id;
    private String nameES;
    private String nameEN;
    private ArrayList<SubtopicResultResponse> subtopics;

    public TopicResultResponse() {
        subtopics = new ArrayList<SubtopicResultResponse>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameES() {
        return nameES;
    }

    public void setNameES(String name) {
        this.nameES = name;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public ArrayList<SubtopicResultResponse> getSubtopics() {
        return subtopics;
    }

    public void setSubtopics(ArrayList<SubtopicResultResponse> subtopics) {
        this.subtopics = subtopics;
    }

}
