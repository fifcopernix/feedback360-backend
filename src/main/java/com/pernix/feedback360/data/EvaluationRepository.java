package com.pernix.feedback360.data;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.Stage;
import com.pernix.feedback360.entities.Survey;
import com.pernix.feedback360.entities.Users;
import com.pernix.feedback360.entities.util.EvaluationStatus;

public interface EvaluationRepository extends CrudRepository<Evaluation, Integer> {

    Set<Evaluation> findAllByStage(Stage stage);

    @Query("from Evaluation a where ?1 member a.evaluators")
    Set<Evaluation> findAllByEvaluator(Evaluator evaluator);

    Set<Evaluation> findAllByUser(Users user);

    Set<Evaluation> findAllByStatus(EvaluationStatus status);

    Set<Evaluation> findAllBySurvey(Survey survey);

}
