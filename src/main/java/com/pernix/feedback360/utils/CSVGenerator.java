package com.pernix.feedback360.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.pernix.feedback360.data.QuestionRepository;
import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Question;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class CSVGenerator {

    private final QuestionRepository questionRepository;

    @Autowired
    public CSVGenerator(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public String GenerateCSV(HashMap<String, ArrayList<Answer>> result) {
        StringBuilder builder = new StringBuilder();
        StringBuilder builderOpen = new StringBuilder();
        Iterator<Entry<String, ArrayList<Answer>>> iterator = result.entrySet().iterator();

        while (iterator.hasNext()) {
            Entry entry = iterator.next();
            ArrayList<String> valuesToJoin = new ArrayList<>();
            Question question = questionRepository.findByDescriptionEs((String) entry.getKey());
            if (question.getType().getId() == 2) {
                String csvLine = addQuestionAndAnswer(entry, valuesToJoin, question);
                builder.append(System.getProperty("line.separator"));
                builder.append(csvLine);
            } else {
                String csvLine = addQuestionAndAnswer(entry, valuesToJoin, question);
                builderOpen.append(System.getProperty("line.separator"));
                builderOpen.append(csvLine);
            }
        }
        builder.append(builderOpen);
        return builder.toString();
    }

    public String addQuestionAndAnswer(Entry entry, ArrayList<String> valuesToJoin, Question question) {
        String description = (String) entry.getKey();
        description =  description.replaceAll(",", "");
        valuesToJoin.add(0, description);
        Iterable<Answer> responses = sortResponseByRole((Iterable<Answer>) entry.getValue());
        for (Answer response : responses) {
            valuesToJoin.add(response.getResponse());
        }
        String csvLine = String.join(",", valuesToJoin);
        return csvLine;
    }

    public ArrayList<Answer> sortResponseByRole(Iterable<Answer> responses) {
        ArrayList<Answer> sortedResponses = new ArrayList<Answer>();
        for (Answer response : responses) {
            if (response.getEvaluator().getEvaluatorRole().getId() == 5) {
                sortedResponses.add(0, response);
            } else {
                sortedResponses.add(response);
            }
        }
        return sortedResponses;
    }

}
