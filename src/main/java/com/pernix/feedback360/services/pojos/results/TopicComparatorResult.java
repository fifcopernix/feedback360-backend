package com.pernix.feedback360.services.pojos.results;

import java.util.ArrayList;

public class TopicComparatorResult {

    private String name;

    private double selfAssessmentGrade;

    private double evaluatorsAverageGrade;

    private ArrayList<SubtopicComparatorResult> subtopics;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSelfAssessmentGrade() {
        return selfAssessmentGrade;
    }

    public void setSelfAssessmentGrade(double selfAssessmentGrade) {
        this.selfAssessmentGrade = selfAssessmentGrade;
    }

    public double getEvaluatorsAverageGrade() {
        return evaluatorsAverageGrade;
    }

    public void setEvaluatorsAverageGrade(double evaluatorsAverageGrade) {
        this.evaluatorsAverageGrade = evaluatorsAverageGrade;
    }

    public ArrayList<SubtopicComparatorResult> getSubtopics() {
        return subtopics;
    }

    public void setSubtopics(ArrayList<SubtopicComparatorResult> subtopics) {
        this.subtopics = subtopics;
    }

}
