package com.pernix.feedback360.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.feedback360.data.SurveyRepository;
import com.pernix.feedback360.data.TopicRepository;
import com.pernix.feedback360.entities.Survey;
import com.pernix.feedback360.entities.Topic;

@RestController
public class TopicServices {

    private final TopicRepository topicRepository;
    private final SurveyRepository surveyRepository;

    @Autowired
    public TopicServices(TopicRepository topicRepository, SurveyRepository surveyRepository) {
        this.topicRepository = topicRepository;
        this.surveyRepository = surveyRepository;
    }

    @RequestMapping(value = "/topics", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Topic>> getAll() {
        Iterable<Topic> topics = topicRepository.findAll();
        if (topics == null) {
            return new ResponseEntity<Iterable<Topic>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Topic>>(topics, HttpStatus.OK);
    }

    @RequestMapping(value = "/topics/{id}", method = RequestMethod.GET)
    public ResponseEntity<Topic> get(@PathVariable int id) {
        Topic topic = topicRepository.findOne(id);
        if (topic == null) {
            return new ResponseEntity<Topic>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Topic>(topic, HttpStatus.OK);
    }

    @RequestMapping(value = "/topics-by-survey", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Topic>> getAllBySurvey(@RequestParam("surveyId") int id) {
        Survey survey = surveyRepository.findOne(id);
        if (survey == null) {
            return new ResponseEntity<Iterable<Topic>>(HttpStatus.NOT_FOUND);
        }
        Iterable<Topic> topics = topicRepository.findAllBySurvey(survey);
        if (topics == null) {
            return new ResponseEntity<Iterable<Topic>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Topic>>(topics, HttpStatus.OK);
    }

    @RequestMapping(value = "/topics", method = RequestMethod.POST)
    public ResponseEntity<Topic> create(@RequestBody Topic topic) {

        if (topicRepository.exists(topic.getId())) {
            return new ResponseEntity<Topic>(HttpStatus.CONFLICT);
        }
        topicRepository.save(topic);
        topic.setSurvey(surveyRepository.findOne(topic.getSurvey().getId()));
        return new ResponseEntity<Topic>(topic, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/topics", method = RequestMethod.PUT)
    public ResponseEntity<Topic> update(@RequestBody Topic topic) {
        Topic current = topicRepository.findOne(topic.getId());
        if (current == null) {
            return new ResponseEntity<Topic>(HttpStatus.NOT_FOUND);
        }
        topic.setId(current.getId());
        topicRepository.save(topic);
        topic.setSurvey(surveyRepository.findOne(topic.getSurvey().getId()));
        return new ResponseEntity<Topic>(topic, HttpStatus.OK);
    }
}
