package com.pernix.feedback360.services;

import com.pernix.feedback360.data.UserRepository;
import com.pernix.feedback360.entities.Users;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserServices {

    private final UserRepository userRepository;

    @Autowired
    public UserServices(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Users>> getAll() {
        Iterable<Users> users = userRepository.findAll();
        if (users == null) {
            return new ResponseEntity<Iterable<Users>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Users>>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/user-by-login", method = RequestMethod.GET)
    public ResponseEntity<Users> get(@RequestParam("login") String login) throws IOException {
        String decodedLogin = java.net.URLDecoder.decode(login, "UTF-8");
        Users user = userRepository.findByLogin(decodedLogin);
        if (user == null) {
            return new ResponseEntity<Users>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Users>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<Users> create(@RequestBody Users user) {
        if (userRepository.exists(user.getId())) {
            return new ResponseEntity<Users>(HttpStatus.CONFLICT);
        }
        userRepository.save(user);
        return new ResponseEntity<Users>(user, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") int id) {
        Users user = userRepository.findOne(id);
        if (user == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        userRepository.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/set-office365-credentials", method = RequestMethod.PUT)
    public void setOffice365Credentials(@RequestBody Users user) {
        Users oldUser = userRepository.findById(user.getId());
        oldUser.setOffice365Email(user.getOffice365Email());
        oldUser.setOffice365Id(user.getOffice365Id());
        oldUser.setOffice365TokenAuth(user.getOffice365TokenAuth());
        userRepository.save(oldUser);
    }

    @RequestMapping(value = "/office365Login")
    public ResponseEntity<Users> office365Login(@RequestBody Users user) {
        Users dbUser = userRepository.findByOffice365IDAndOffice365Email(user.getOffice365Id(), user.getOffice365Email());
        if (dbUser == null) {
            return new ResponseEntity<Users>(HttpStatus.NOT_FOUND);
        } else {
            if (!user.getOffice365TokenAuth().equals(dbUser.getOffice365TokenAuth())) {
                dbUser.setOffice365TokenAuth(user.getOffice365TokenAuth());
                userRepository.save(dbUser);
            }
            return new ResponseEntity<Users>(dbUser, HttpStatus.OK);
        }
    }
}
