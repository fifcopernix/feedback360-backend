package com.pernix.feedback360.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String name;

    @NotEmpty
    @Column(unique = true, nullable = false)
    private String login;

    @JsonIgnore
    @NotEmpty
    @Column(nullable = false)
    private String password;

    @Column(nullable=true, length = 2048)
    private String office365TokenAuth;

    @Column(nullable=true)
    private String office365Email;

    @Column(nullable=true)
    private String office365Id;

	@NotNull
    @ManyToOne
    @JoinColumn(name = "FK_roleId", nullable = false)
    private Role role;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Evaluation> evaluations;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Evaluator> evaluators;

    public Users() {
    }

    public Users(Users user) {
        this.id = user.getId();
        this.name = user.getName();
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.role = user.getRole();
        this.evaluations = user.getEvaluations();
        this.evaluators = user.getEvaluators();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(Set<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }

    public Set<Evaluator> getEvaluators() {
        return evaluators;
    }

    public void setEvaluator(Set<Evaluator> evaluators) {
        this.evaluators = evaluators;
    }

    public String getOffice365TokenAuth() {
		return office365TokenAuth;
	}

	public void setOffice365TokenAuth(String office365TokenAuth) {
		this.office365TokenAuth = office365TokenAuth;
	}

	public String getOffice365Email() {
		return office365Email;
	}

	public void setOffice365Email(String office365Email) {
		this.office365Email = office365Email;
	}

	public String getOffice365Id() {
		return office365Id;
	}

	public void setOffice365Id(String office365Id) {
		this.office365Id = office365Id;
	}
}

