package com.pernix.feedback360.utils;

import java.io.File;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XmlToXhtmlTransformer {

    public void transform() {
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(new File("transform.xslt"));
        Transformer transformer;
        try {
            transformer = factory.newTransformer(xslt);

            Source text = new StreamSource(new File("input.xml"));
            transformer.transform(text, new StreamResult(new File("output.xhtml")));
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

}
