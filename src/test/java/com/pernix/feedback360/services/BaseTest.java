package com.pernix.feedback360.services;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import com.pernix.feedback360.AppConfig;
import com.pernix.feedback360.data.QuestionRepository;
import com.pernix.feedback360.data.QuestionTypeRepository;
import com.pernix.feedback360.data.RoleRepository;
import com.pernix.feedback360.data.SubtopicRepository;
import com.pernix.feedback360.data.SurveyRepository;
import com.pernix.feedback360.data.TopicRepository;
import com.pernix.feedback360.data.UserRepository;
import com.pernix.feedback360.entities.Question;
import com.pernix.feedback360.entities.QuestionType;
import com.pernix.feedback360.entities.Role;
import com.pernix.feedback360.entities.Subtopic;
import com.pernix.feedback360.entities.Survey;
import com.pernix.feedback360.entities.Topic;
import com.pernix.feedback360.entities.Users;

public class BaseTest {
    private static final String OAUTH_TOKEN_URL = "/oauth/token";
    private static final String GRANT_TYPE = "grant_type";
    private static final String AUTHORIZED_USERNAME = "testUser";
    private static final String AUTHORIZED_PASSWORD = "testUserPassword";
    private static final String PASSWORD = "password";
    private static final String USERNAME = "username";
    protected static final String SURVEY_NAME_ES = "Nombre de la questionario+";
    protected static final String SURVEY_NAME_EN = "Survey name";
    protected static final String TOPIC_NAME_EN = "new topic";
    protected static final String TOPIC_NAME_ES = "nuevo tema";
    protected static final String SUBTOPIC_NAME_EN = "Survey name";
    protected static final String SUBTOPIC_NAME_ES = "Nombre del questionario";
    protected static final String QUESTION_NAME_EN = "Question name";
    protected static final String QUESTION_NAME_ES = "Nombre de la pregunta";

    @Autowired
    WebApplicationContext context;
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    protected AppConfig appConfig;

    @Autowired
    protected SurveyRepository surveyRepository;
    @Autowired
    protected TopicRepository topicRepository;
    @Autowired
    protected SubtopicRepository subtopicRepository;

    @Autowired
    protected QuestionTypeRepository questionTypeRepository;

    @Autowired
    protected QuestionRepository questionRepository;

    protected Users user;
    protected Survey survey;
    protected Topic topic;
    protected Subtopic subtopic;
    protected Question questionOne;
    protected Question questionTwo;

    private String userAccessToken;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;
    private MockMvc mvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.webAppContextSetup(context).addFilter(springSecurityFilterChain).build();

        if (user == null) {
            user = userRepository.findByLogin(AUTHORIZED_USERNAME);
        }
        if (user == null) {
            Role role = roleRepository.findOne(1);
            user = new Users();
            user.setLogin(AUTHORIZED_USERNAME);
            user.setName("Test User");
            user.setPassword(AUTHORIZED_PASSWORD);
            user.setRole(role);
            userRepository.save(user);
        }

        if (userAccessToken == null) {
            userAccessToken = getAccessToken(AUTHORIZED_USERNAME, AUTHORIZED_PASSWORD);
        }
        survey = createSurvey("es");
        topic = createTopic();
        subtopic = createSubtopic();
        questionOne = createQuestion();
        questionTwo = createQuestion();
    }

    private String getAccessToken(String username, String password) throws Exception {
        String authorization = "Basic " + new String(Base64Utils.encode(
                (appConfig.getFeedback360OauthClientId() + ":" + appConfig.getFeedback360OauthSecret()).getBytes()));
        String content = mvc
                .perform(post(OAUTH_TOKEN_URL).header("Authorization", authorization)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED).param(USERNAME, username)
                        .param(PASSWORD, password).param(GRANT_TYPE, PASSWORD))
                .andReturn().getResponse().getContentAsString();
        return content.substring(17, 53);
    }

    protected ResultActions perform(MockHttpServletRequestBuilder request) throws Exception {
        return perform(request.contentType(MediaType.APPLICATION_JSON), false);
    }

    protected ResultActions performAuthorized(MockHttpServletRequestBuilder request) throws Exception {
        return perform(request.contentType(MediaType.APPLICATION_JSON), true);
    }

    private ResultActions perform(MockHttpServletRequestBuilder request, boolean performAuthorized) throws Exception {
        if (performAuthorized) {
            request = request.header("Authorization", "Bearer " + userAccessToken);
        }
        return mvc.perform(request.contentType(MediaType.APPLICATION_JSON));
    }

    private Survey createSurvey(String language) {
        Survey survey = new Survey();
        survey.setNameEs(SURVEY_NAME_ES);
        survey.setNameEn(SURVEY_NAME_EN);

        surveyRepository.save(survey);
        return survey;
    }

    protected Topic createTopic() {
        Topic topic = new Topic();
        topic.setNameEn(TOPIC_NAME_EN);
        topic.setNameEs(TOPIC_NAME_ES);
        topic.setSurvey(survey);
        topicRepository.save(topic);
        return topic;
    }

    protected Subtopic createSubtopic() {
        Subtopic subtopic = new Subtopic();
        subtopic.setNameEn(SUBTOPIC_NAME_EN);
        subtopic.setNameEs(SUBTOPIC_NAME_ES);
        subtopic.setTopic(topic);
        subtopicRepository.save(subtopic);
        return subtopic;
    }

    protected Question createQuestion() {
        QuestionType type = questionTypeRepository.findOne(2);
        Question question = new Question();
        question.setDescriptionEn(QUESTION_NAME_EN);
        question.setDescriptionEs(QUESTION_NAME_ES);
        question.setSubtopic(subtopic);
        question.setType(type);
        questionRepository.save(question);
        return question;
    }
}
