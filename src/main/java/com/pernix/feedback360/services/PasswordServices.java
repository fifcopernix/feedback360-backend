package com.pernix.feedback360.services;

import com.pernix.feedback360.data.UserRepository;
import com.pernix.feedback360.entities.Users;
import com.pernix.feedback360.services.pojos.PasswordRequest;
import com.pernix.feedback360.utils.MailUtil;
import com.pernix.feedback360.utils.RandomGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PasswordServices {

    private final UserRepository userRepository;
    private final MailUtil mailUtil;

    @Autowired
    public PasswordServices(UserRepository userRepository, MailUtil mailUtil) {
        this.userRepository = userRepository;
        this.mailUtil = mailUtil;
    }

    @RequestMapping(value = "/password", method = RequestMethod.PUT)
    public ResponseEntity<Void> editPassword(@RequestBody PasswordRequest passwordData) {
        Users user = userRepository.findById(passwordData.getUserId());
        if (user == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else if (user.getPassword().equals(passwordData.getOldPassword())) {
            user.setPassword(passwordData.getNewPassword());
            userRepository.save(user);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } else {
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(value = "/password/recover/{userLogin:.+}/{language}",
        method = RequestMethod.GET)
    public ResponseEntity<Void> recoverPasswordGeneration(
                                            @PathVariable("userLogin") String userLogin,
                                            @PathVariable("language") String language) {
        Users user = userRepository.findByLogin(userLogin);
        if (user == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            String randomString = RandomGenerator.randomString(6);
            user.setPassword(randomString);
            userRepository.save(user);
            mailUtil.sendResetPasswordEmail(user, language);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
    }

}
