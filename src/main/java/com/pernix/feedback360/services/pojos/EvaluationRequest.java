package com.pernix.feedback360.services.pojos;

import com.pernix.feedback360.entities.Stage;
import com.pernix.feedback360.entities.Survey;

public class EvaluationRequest {

    private Iterable<String> emails;
    private Survey survey;
    private Stage stage;
    private String language;

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public Iterable<String> getEmails() {
        return emails;
    }

    public void setEmails(Iterable<String> emails) {
        this.emails = emails;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
