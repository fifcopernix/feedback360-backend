package com.pernix.feedback360.services.pojos;

import java.util.Set;

import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.Stage;
import com.pernix.feedback360.entities.Survey;
import com.pernix.feedback360.entities.Users;
import com.pernix.feedback360.entities.util.EvaluationStatus;

public class EvaluationResponse {
    private int id;
    private Survey survey;
    private Set<Evaluator> evaluators;
    private Set<Answer> answers;
    private Stage stage;
    private Users user;
    private EvaluationStatus status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public Stage getStage() {
        return stage;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Set<Evaluator> getEvaluators() {
        return evaluators;
    }

    public void setEvaluators(Set<Evaluator> evaluators) {
        this.evaluators = evaluators;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    public EvaluationStatus getStatus() {
        return status;
    }

    public void setStatus(EvaluationStatus status) {
        this.status = status;
    }

}
