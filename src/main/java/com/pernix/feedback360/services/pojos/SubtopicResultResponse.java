package com.pernix.feedback360.services.pojos;

import java.util.ArrayList;

public class SubtopicResultResponse {

    private int id;
    private String nameEs;
    private String nameEn;

    private ArrayList<QuestionResultResponse> questions;

    public SubtopicResultResponse() {
        questions = new ArrayList<QuestionResultResponse>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEs() {
        return nameEs;
    }

    public void setNameEs(String nameES) {
        this.nameEs = nameES;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEN) {
        this.nameEn = nameEN;
    }

    public ArrayList<QuestionResultResponse> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<QuestionResultResponse> questions) {
        this.questions = questions;
    }

}
