package com.pernix.feedback360;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class AppConfig {

    @Autowired
    Environment env;

    public String getFeedback360OauthResouceId() {
        return env.getProperty("feedback360.oauth2.resource-id");
    }

    public String getFeedback360OauthClientId() {
        return env.getProperty("feedback360.oauth2.client-id");
    }

    public String getFeedback360OauthSecret() {
        return env.getProperty("feedback360.oauth2.secret");
    }

    public String getFeedBack360mailHost() {
        return env.getProperty("feedback360.mail.host");
    }

    public String getFeedBack360mailPort() {
        return env.getProperty("feedback360.mail.port");
    }

    public String getFeedBack360mailUser() {
        return env.getProperty("feedback360.mail.user");
    }

    public String getFeedBack360mailPassword() {
        return env.getProperty("feedback360.mail.password");
    }
}
