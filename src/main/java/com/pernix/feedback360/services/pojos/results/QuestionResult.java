package com.pernix.feedback360.services.pojos.results;

public class QuestionResult {

    private String statement;
    private double grade;

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

}
