package com.pernix.feedback360.utils;

import com.pernix.feedback360.AppConfig;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;
import com.pernix.feedback360.entities.Users;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MailUtil {
    static final Logger logger = Logger.getLogger(MailUtil.class);

    @Autowired
    private AppConfig appConfig;

    public MailUtil() {
    }

    public void sendEvaluationNotification(Evaluation evaluation, String language) {

        Configuration cfg = new Configuration();
        Template template;
        Writer out = new StringWriter();
        String source = language.equals("es") ? "src/main/resources/evaluation_es.ftl" : "src/main/resources/evaluation_en.ftl";
        try {
            template = cfg.getTemplate(source);
            Map<String, String> rootMap = new HashMap<String, String>();

            rootMap.put("login", evaluation.getUser().getLogin());
            rootMap.put("password", evaluation.getUser().getPassword());
            rootMap.put("employeeName", evaluation.getUser().getName());
            rootMap.put("body", "asdfadsf");
            rootMap.put("from", "asdfasdfasd.");
            template.process(rootMap, out);
            sendMailWithHtml("fifco360@gmail.com", evaluation.getUser().getLogin(),
                    "Indicaciones para dar inicio a su Evaluación 360 de Liderazgo",
                    out.toString());
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (TemplateException e) {
            logger.error(e.getMessage());
        }
    }

    public void sendInvitationToEvaluation(Evaluation evaluation, Evaluator evaluator, String evaluatorMail, String language) {
        Configuration cfg = new Configuration();
        Template template;
        Writer out = new StringWriter();
        DateFormat dt = DateFormat.getDateInstance(DateFormat.SHORT, new Locale("es", "ES"));
        String date = dt.format(evaluation.getStage().getEndDate());
        String source = language.equals("es") ? "src/main/resources/invitation_es.ftl" : "src/main/resources/invitation_en.ftl"; 
        try {
            template = cfg.getTemplate(source);
            Map<String, String> rootMap = new HashMap<String, String>();
            rootMap.put("employeeName", evaluator.getName());
            rootMap.put("deadline", date);
            rootMap.put("key", evaluator.getUniqueLink());
            rootMap.put("user", evaluation.getUser().getName());
            template.process(rootMap, out);
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (TemplateException e) {
            logger.error(e.getMessage());
        }
        sendMailWithHtml("fifco360@gmail.com", evaluatorMail,
                "Solicitud de retroalimentación de " + evaluation.getUser().getName(),
                out.toString());
    }

    public void sendMail(String from, String to, String subject, String mailBody) {
        logger.info("**************Sending email.....");
        Session session = Session.getInstance(setProperties(), setAuthentication());
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setText(mailBody);
            Transport.send(message);
            logger.info("SENT");
        } catch (MessagingException ex) {
            logger.error(ex.getMessage());
        }
    }

    public void sendMailWithHtml(String from, String to, String subject, String mailBody) {
        Session session = Session.getDefaultInstance(setProperties(), setAuthentication());
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setContent(mailBody, "text/html");
            Transport.send(message);
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
    }

    public void sendEvaluatorCompleted(Evaluation evaluation, Evaluator evaluator) {
    	Configuration cfg = new Configuration();
        Template template;
        Writer out = new StringWriter();
        try {
        	if(evaluator.getUser() == null || evaluator.getUser().getId() != evaluation.getUser().getId()) {
                String templateBaseSrc = "src/main/resources/";
                String templateSrc = "evaluatorCompleted_es.ftl";
                template = cfg.getTemplate(templateBaseSrc + templateSrc);
                Map<String, String> rootMap = new HashMap<String, String>();
                
                rootMap.put("employeeName", evaluation.getUser().getName());
                rootMap.put("evalautorName", evaluator.getName());
                template.process(rootMap, out);
                sendMailWithHtml("fifco360@gmail.com", evaluation.getUser().getLogin(),
                        "Answers Sent By Evaluator", out.toString());
        	}
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (TemplateException e) {
            logger.error(e.getMessage());
        }
    }

    private Authenticator setAuthentication() {
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(appConfig.getFeedBack360mailUser(),
                        appConfig.getFeedBack360mailPassword());
            }
        };
        return auth;
    }

    private Properties setProperties() {
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.host", appConfig.getFeedBack360mailHost());
        properties.setProperty("mail.smtp.port", appConfig.getFeedBack360mailPort());
        return properties;
    }

    public void sendResetPasswordEmail(Users user, String language) {

        Configuration cfg = new Configuration();
        Template template;
        Writer out = new StringWriter();
        try {
            String templateBaseSrc = "src/main/resources/";
            String templateSrc = language.equals("es") ? "passwordReset_es.ftl"
                : "passwordReset_en.ftl";
            template = cfg.getTemplate(templateBaseSrc + templateSrc);
            Map<String, String> rootMap = new HashMap<String, String>();

            rootMap.put("employeeName", user.getName());
            rootMap.put("password", user.getPassword());
            template.process(rootMap, out);
            sendMailWithHtml("fifco360@gmail.com", user.getLogin(),
                    "Password Reset", out.toString());
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (TemplateException e) {
            logger.error(e.getMessage());
        }
    }
}
