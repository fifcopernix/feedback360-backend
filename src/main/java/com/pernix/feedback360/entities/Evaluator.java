package com.pernix.feedback360.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pernix.feedback360.entities.util.EvaluatorStatus;

@Entity
public class Evaluator {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @NotEmpty
    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "FK_userId", nullable = true)
    private Users user;

    @ManyToOne
    @JoinColumn(name = "FK_evaluationId", nullable = false)
    private Evaluation evaluation;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "evaluatorRole")
    private Set<Evaluator> evaluators;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(nullable = false)
    private EvaluatorStatus status;

    @NotEmpty
    @Column(nullable = false)
    private String uniqueLink;

    @ManyToOne
    @JoinColumn(name = "FK_evaluatorRoleId", nullable = false)
    private EvaluatorRole evaluatorRole;

    public Evaluator() {
    }

    public Evaluator(Evaluator invitation) {
        this.id = invitation.getId();
        this.name = invitation.getName();
        this.evaluation = invitation.getEvaluation();
        this.user = invitation.getUser();
        this.status = invitation.getStatus();
        this.evaluatorRole = invitation.getEvaluatorRole();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Evaluation getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }

    public EvaluatorStatus getStatus() {
        return status;
    }

    public void setStatus(EvaluatorStatus status) {
        this.status = status;
    }

    public String getUniqueLink() {
        return uniqueLink;
    }

    public void setUniqueLink(String uniqueLink) {
        this.uniqueLink = uniqueLink;
    }

    public EvaluatorRole getEvaluatorRole() {
        return evaluatorRole;
    }

    public void setEvaluatorRole(EvaluatorRole evaluatorRole) {
        this.evaluatorRole = evaluatorRole;
    }
}
