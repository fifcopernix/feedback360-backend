package com.pernix.feedback360.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pernix.feedback360.data.AnswerRepository;
import com.pernix.feedback360.data.EvaluationRepository;
import com.pernix.feedback360.data.EvaluatorRepository;
import com.pernix.feedback360.entities.Answer;
import com.pernix.feedback360.entities.Evaluation;
import com.pernix.feedback360.entities.Evaluator;

@RestController
public class AnswerServices {

    private final AnswerRepository answerRepository;
    private final EvaluationRepository evaluationRepository;
    private final EvaluatorRepository evaluatorRepository;

    @Autowired
    public AnswerServices(AnswerRepository answerRepository, EvaluationRepository evaluationRepository, EvaluatorRepository evaluatorRepository) {
        this.answerRepository = answerRepository;
        this.evaluationRepository = evaluationRepository;
        this.evaluatorRepository = evaluatorRepository;
    }

    @RequestMapping(value = "/answers", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Answer>> getAll() {
        Iterable<Answer> answers = answerRepository.findAll();
        if (answers == null) {
            return new ResponseEntity<Iterable<Answer>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Answer>>(answers, HttpStatus.OK);
    }

    @RequestMapping(value = "/answers-by-evaluation", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Answer>> getByEvaluation(@RequestParam("evaluationId") int id) {
        Evaluation evaluation = evaluationRepository.findOne(id);
        if (evaluation == null) {
            return new ResponseEntity<Iterable<Answer>>(HttpStatus.NOT_FOUND);
        }
        Iterable<Answer> answers = answerRepository.findAllByEvaluation(evaluation);
        if (answers == null) {
            return new ResponseEntity<Iterable<Answer>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Answer>>(answers, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/answers-by-evaluator-and-evaluation", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Answer>> getByEvaluatorAndEvaluation(@RequestParam("evaluatorId") int evaluatorId, @RequestParam("evaluationId") int evaluationId) {
        Evaluation evaluation = evaluationRepository.findOne(evaluationId);
        Evaluator evaluator = evaluatorRepository.findOne(evaluatorId);
        if (evaluation == null || evaluator == null) {
            return new ResponseEntity<Iterable<Answer>>(HttpStatus.NOT_FOUND);
        }
        Iterable<Answer> answers = answerRepository.findAllByEvaluatorAndEvaluation(evaluator, evaluation);
        if (answers == null) {
            return new ResponseEntity<Iterable<Answer>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Iterable<Answer>>(answers, HttpStatus.OK);
    }
}
