  <!doctype html>
  <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <title></title>  
  </head>
  <body>
    <div style="width:100%!important;min-width:100%;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;text-align:left;line-height:19px;font-size:14px;background:#428bca;margin:0;padding:0" bgcolor="#428bca">
      <table bgcolor="#428bca" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;height:100%;width:100%;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;background:#428bca;margin:0;padding:0">
        <tbody>
          <tr align="left" style="vertical-align:top;text-align:left;padding:0">
            <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:0" valign="top">
              <center style="width:100%;min-width:580px">
                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0">
                  <tbody>
                    <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                      <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:0" valign="top">
                        <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:100%;display:block;padding:0">
                          <tbody>
                            <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                              <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:10px 0 0" valign="top">
                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;margin:0 auto;padding:0">
                                  <tbody>
                                    <tr>
                                      <td style="text-align:center;vertical-align:top;font-size:0px;padding:20px 0px;padding-bottom:20px;padding-top:20px;"><img style="max-height: 150px;" alt="Pernix-Solutions" src="http://fifco360feedback-fe.herokuapp.com/app/images/fifco3.png"></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0">
                  <tbody>
                    <tr align="center" style="vertical-align:top;text-align:left;padding:0">
                      <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:0" valign="top">
                        <table bgcolor="#fff" style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:100%;display:block;border-radius:5px;background:#fff;padding:0">
                          <tbody>
                            <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                              <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#222;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0;padding:20px 0" valign="top">
                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:580px;margin:0 auto;padding:0">
                                  <tbody>
                                    <tr align="center" style="vertical-align:top;text-align:center;padding:0">
                                      <td align="center" style="word-break:break-word;vertical-align:top;text-align:center;color:rgb(34,34,34);font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:14px;margin:0px;padding:0px 35px 10px;border-collapse:collapse!important;background:rgb(255,255,255)" valign="top">

<div style="text-align:left;color:#555;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.6;font-size:14px;margin:0 0 10px;padding:0 0 10px">
    <p>Dear ${employeeName},</p>
    <p>Welcome to the 360 Feedback System, designed to measure the perceptions of your leader,peers,collaborators,clients and others,about the behaviors that you demonstrate in your work context in relation to the FIFCO Leadership Model.</p>
    <p>The first phase of the 360 Feedback process consists of the following steps:</p>
    <p>1. <b>YOU PROPOSE YOUR LIST OF EVALUATORS.</b> You will be evaluated and should be able to access the link that we provide at the end of this message, and indicate the names, emails and employment relationship maintained with the person you wish to be your Evaluators in the process. Make sure these people belong to the following categories:</p>
    <ul>
        <li>Leader (Boss, person to whom you report directly)</li>
        <li>Pairs / Clients (People of the same level who work with you - peers))</li>
        <li>Collaborators (People who report directly to you)</li>
        <li>Others (People who do not fit in the previous categories, but for you, they are relevant in their work)</li>
    </ul>
    <p>It is very important that these people have a relevant and frequent working relationship with you for more than six months. Please indicate at least 3 people on each category, except for the case of the Leader, in order to guarantee the representation of the evaluation (if it does not exist any of the Categories for your case, just ignore it)</p>
    <p>Once you complete the list of evaluators, click on the "Send List" button on that page. That will generate an automatic notification.</p>
    <p>2. <b>YOU NOTIFY YOUR EVALUATORS.</b> Once validation is received, we strongly recommend that you make sure you let your evaluators know that you have included them in your 360 Feedback, they will receive an e-mail from FIFCO 360 inviting them to evaluate you.</p>
    <p>3. <b>FIFCO 360 INVITES YOU AND YOUR EVALUATOR:</b> our System will invite you to access and complete your 360 Feedback. The system will indicate the maximum amount of time to complete the survey.</p>
    <p>To enter the system and create your list of Evaluators please click on the following link: https://fifco360feedback-fe.herokuapp.com/#/dashboard</p>
	    <p>Your login credentials to the system are:</p>
	    <ul>
	    	<li>User: <span style="color: blue">${login}</span></li>
	    	<li>Password: <span style=" color: blue">${password}</span></li>
	    </ul>
    <p>If you require support please contact FIFCO 360 at fifco360@gmail.com and we will gladly help you.</p>
    <p>We hope that this 360 Feedback experience adds a lot of value to you and helps you improve professionally.</p>
    <p>Cordially,</p>
    <p>FIFCO 360</p>
</div>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:inherit;width:580px;margin:0 auto;padding:0">
                  <tbody>
                    <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                      <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0" valign="top">
                        <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:100%;display:block;padding:0">
                          <tbody style="display:block">
                            <tr align="left" style="vertical-align:top;text-align:left;padding:0;display:block">
                             <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:10px 0 0 0" valign="top">
                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:left;width:175px;margin:0 auto;padding:0">
                                  <tbody>
                                    <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                                        <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0 0 10px" valign="top"><!-- <a href="http://www.pernix-solutions.com" target="_blank"><img align="none" alt="Pernix logo" src="https://pernixnewsletter.files.wordpress.com/2015/10/logoo1.png" style="outline:0;text-decoration:none;width:auto;max-width:30px;float:none;clear:both;display:inline-block;margin:0" class="CToWUd"></a> --></td>
                                      <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;width:0;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0" valign="top">&nbsp;</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                              <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:left;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:10px 10px 0" valign="top">
                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;width:210px;margin:0 auto;padding:0;text-align:center">
                                  <tbody style="display:inline-block">
                                    <tr align="left" style="vertical-align:top;text-align:left;padding:0;display:inline-block">
                                      <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#fff;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:0 0 10px" valign="top"><br>
                                      <br>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                              <td align="left" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;margin:0;padding:10px 0 0" valign="middle">
                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;text-align:right;width:175px!important;margin:0 auto;padding:0">
                                  <tbody style="display:inline-block">
                                    <tr align="left" style="vertical-align:top;text-align:left;padding:0">
                                      <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;width:auto!important;margin:0;padding:0 0 10px" valign="top"><a href="#" style="color:#2ba6cb;text-decoration:none;display:block;width:25px;min-height:auto;text-align:right!important" target="_blank"><img align="none" alt="Pernix on Facebook" src="https://go.pardot.com/l/36622/2015-12-14/71t9yg/36622/93095/fb.png" style="outline:0;text-decoration:none;width:auto;max-width:100%;float:none;clear:both;display:inline-block;margin:0;border:none"></a></td>
                                      <td align="center" style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;text-align:center;color:#9276ce;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:19px;font-size:12px;width:auto!important;margin:0;padding:0 0 10px" valign="top"><a href="#" style="color:#2ba6cb;text-decoration:none;display:block;width:25px;min-height:auto;text-align:right!important" target="_blank"><img align="none" alt="Pernix on LinkedIn" src="https://go.pardot.com/l/36622/2016-07-18/8yhgn5/36622/109842/linkedin.png" style="outline:0;text-decoration:none;width:auto;max-width:100%;float:none;clear:both;display:inline-block;margin:0;border:none"></a></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </center>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </body>
  </html>