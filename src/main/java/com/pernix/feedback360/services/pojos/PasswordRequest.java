package com.pernix.feedback360.services.pojos;

public class PasswordRequest {
    private String newPassword;
    private String oldPassword;
    private int userId;

    public PasswordRequest() {

    }

    public PasswordRequest(String newPassword, String oldPassword, int userId) {
        this.newPassword = newPassword;
        this.oldPassword = oldPassword;
        this.userId = userId;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public int getUserId() {
        return userId;
    }

}
